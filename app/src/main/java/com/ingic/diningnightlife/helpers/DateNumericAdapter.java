/*
 * Created by Ingic on 11/10/17 5:23 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 11/10/17 5:23 PM
 */

package com.ingic.diningnightlife.helpers;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.wheel.adapters.NumericWheelAdapter;

/**
 * Created by syedatafseer on 11/10/2017.
 */

public class DateNumericAdapter extends NumericWheelAdapter {
    // Index of current item
    int currentItem;
    // Index of item to be highlighted
    int currentValue;
    Context context;

    /**
     * Constructor
     */
    public DateNumericAdapter(Context context, int minValue, int maxValue, int current) {
        super(context, minValue, maxValue);
        this.context =context;
        this.currentValue = current;
        setTextSize(16);
    }

    @Override
    protected void configureTextView(TextView view) {
        super.configureTextView(view);
        if (currentItem == currentValue) {
            view.setTextColor(context.getResources().getColor(R.color.login_blue_button));
        }
        view.setTypeface(Typeface.SANS_SERIF);
    }

    @Override
    public View getItem(int index, View cachedView, ViewGroup parent) {
        currentItem = index;

        return super.getItem(index, cachedView, parent);
    }
}
