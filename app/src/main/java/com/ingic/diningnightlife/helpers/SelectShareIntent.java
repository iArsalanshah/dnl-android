package com.ingic.diningnightlife.helpers;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.ShareDialog;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.ingic.diningnightlife.activities.DockActivity;
import com.ingic.diningnightlife.global.AppConstants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class SelectShareIntent {
    private static final String[] items = {"Facebook", "Twitter", "WhatsApp", "Email"};

    public static void selectIntent(final DockActivity context, String title, final String msgTitle, final String msg) {
        final ActionSheetDialog dialog = new ActionSheetDialog(context, items, null);
        dialog.title(title)
                .titleTextSize_SP(14.5f)
                .cancelText(context.getString(android.R.string.cancel))
                .show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                dialog.dismiss();
                switch (position) {
                    case 0:
                        facebookShare(context, msgTitle, msg);
                        break;
                    case 1:
                        twitterShare(context, msg);
                        break;
                    case 2:
                        whatsAppShare(context, msg);
                        break;
                    case 3:
                        emailShare(context, msg);
                        break;
                }
            }
        });
    }

    private static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf("TAG: ", "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

    //Share on Twitter
    private static void twitterShare(Context context, String msg) {
        // Create intent using ACTION_VIEW and a normal Twitter url:
        String tweetUrl = String.format("https://twitter.com/intent/tweet?text=%s&url=%s",
                urlEncode(msg),
                urlEncode(AppConstants.WEBSITE_LINK));
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));

        // Narrow down to official Twitter app, if available:
        List<ResolveInfo> matches = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                intent.setPackage(info.activityInfo.packageName);
            }
        }
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(context, "No Application Found to share on twitter", Toast.LENGTH_SHORT).show();
        }
    }

    //Share on Facebook
    private static void facebookShare(Activity activity, String title, String msg) {
        // Create an object
        ShareOpenGraphObject object = new ShareOpenGraphObject.Builder()
                .putString("og:type", "restaurant.restaurant")
                .putString("og:title", title.toUpperCase())
                .putString("og:description", msg)
                .putString("og:url", AppConstants.WEBSITE_LINK)
                .build();
        // Create an action
        ShareOpenGraphAction action = new ShareOpenGraphAction.Builder()
                .setActionType("restaurant.visited")
                .putObject("restaurant", object)
                .build();
        // Create the content
        ShareOpenGraphContent content = new ShareOpenGraphContent.Builder()
                .setPreviewPropertyName("restaurant")
                .setAction(action)
                .build();
        ShareDialog.show(activity, content);
    }

    //Email Share
    private static void emailShare(Context context, String msg) {
        String msgWithLink = msg + "\n" + AppConstants.WEBSITE_LINK + "\n";
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "", null));
//        emailIntent.putExtra(Intent.EXTRA_SUBJECT, Constant.SHARE_SUBJECT);
        emailIntent.putExtra(Intent.EXTRA_TEXT, msgWithLink);
        context.startActivity(Intent.createChooser(emailIntent, ""));
    }

    private static void whatsAppShare(Context context, String msg) {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, msg + "\n" + AppConstants.WEBSITE_LINK + "\n");
        try {
            context.startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            UIHelper.showShortToastInCenter(context, "Whatsapp have not been installed.");
        }
    }

    //SMS Share
//    private static void messageShare(Context context, String msg) {
//        String msgWithLink = msg + "\n\n" + Constant.SHARE_LINK;
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) //At least KitKat
//        {
//            String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(context); //Need to change the build to API 19
//
//            Intent sendIntent = new Intent(Intent.ACTION_SEND);
//            sendIntent.setType("text/plain");
//            sendIntent.putExtra(Intent.EXTRA_TEXT, msgWithLink);
//
//            if (defaultSmsPackageName != null)//Can be null in case that there is no default, then the user would be able to choose any app that support this intent.
//            {
//                sendIntent.setPackage(defaultSmsPackageName);
//            }
//            context.startActivity(sendIntent);
//
//        } else //For early versions, do what worked for you before.
//        {
//            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
//            sendIntent.setData(Uri.parse("sms:"));
//            sendIntent.putExtra("sms_body", msgWithLink);
//            context.startActivity(sendIntent);
//        }
//    }
}
