package com.ingic.diningnightlife.helpers;

import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;

import com.ingic.diningnightlife.R;

import static android.content.Context.CONNECTIVITY_SERVICE;


public class InternetHelper {

    public static boolean CheckInternetConectivityandShowToast(AppCompatActivity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            return true;
        } else {
            UIHelper.showLongToastInCenter(activity, activity.getString(R.string.no_internet_connection));
            return false;
        }
    }

    public static boolean CheckInternetConectivity(AppCompatActivity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}