package com.ingic.diningnightlife.helpers;

import android.content.Context;
import android.util.Log;

import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.retrofit.WebService;
import com.ingic.diningnightlife.retrofit.WebServiceFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TokenUpdater {
    private static final TokenUpdater tokenUpdater = new TokenUpdater();
    private WebService webservice;

    private TokenUpdater() {
    }

    public static TokenUpdater getInstance() {
        return tokenUpdater;
    }

    @RestAPI
    public void UpdateToken(Context context, final int userid, String DeviceType, String Token) {
        if (Token.isEmpty()) {
            Log.e("Token Updater", "Token is Empty");
        }
        webservice = WebServiceFactory.getWebServiceInstanceWithDefaultInterceptor(context,
                WebServiceConstants.SERVICE_BASE_URL);
        Call<ResponseWrapper> call = webservice.updateDeviceToken(userid, DeviceType, Token);
        call.enqueue(new Callback<ResponseWrapper>() {
            @Override
            public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                if (response.body() != null) {
                    Log.i("UPDATE TOKEN", response.body().getResponse());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                Log.e("UPDATE TOKEN", t.toString());
            }
        });
    }
}
