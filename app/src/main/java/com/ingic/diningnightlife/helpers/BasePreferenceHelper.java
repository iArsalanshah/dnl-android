package com.ingic.diningnightlife.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.ingic.diningnightlife.entities.LoginResult;


public class BasePreferenceHelper extends PreferenceHelper {
    private static final String KEY_USER = "userDataKey";
    private static final String KEY_LOGIN_STATUS = "islogin";
    private static final String FILENAME = "preferences";
    private static final String KEY_CONFIRM_AGE = "isConfirmAge";
    private static final String KEY_LAST_ENTERED_EMAIL = "last_entered_email";
    private static final String KEY_SHOW_CHANGE_CITY = "show_change_city";
    private Context context;


    public BasePreferenceHelper(Context c) {
        this.context = c;
    }

    public SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(FILENAME, Activity.MODE_PRIVATE);
    }

    public void setLoginStatus(boolean isLogin) {
        putBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS, isLogin);
    }

    public boolean isLogin() {
        return getBooleanPreference(context, FILENAME, KEY_LOGIN_STATUS);
    }

    public LoginResult getUser() {
        return new Gson().fromJson(getStringPreference(context, FILENAME, KEY_USER), LoginResult.class);
    }

    public void putUser(LoginResult user) {
        putStringPreference(context, FILENAME, KEY_USER, new Gson().toJson(user));
    }

    public void setIsUserSignUp(boolean value){
        putBooleanPreference(context, FILENAME, KEY_SHOW_CHANGE_CITY, value);
    }

    public boolean getIsUserSignUp(){
        return getBooleanPreference(context, FILENAME, KEY_SHOW_CHANGE_CITY);
    }

    public String getLastEnteredEmail() {
        return getStringPreference(context, FILENAME, KEY_LAST_ENTERED_EMAIL);
    }

    public void setLastEnteredEmail(String text) {
        putStringPreference(context, FILENAME, KEY_LAST_ENTERED_EMAIL, text);
    }

    public void clearData(){
        getSharedPreferences().edit().remove(KEY_LOGIN_STATUS).apply();
        getSharedPreferences().edit().remove(KEY_USER).apply();
        getSharedPreferences().edit().remove(KEY_CONFIRM_AGE).apply();
        getSharedPreferences().edit().remove(KEY_SHOW_CHANGE_CITY).apply();
    }
}
