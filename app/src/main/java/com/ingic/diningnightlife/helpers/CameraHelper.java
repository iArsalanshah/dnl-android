package com.ingic.diningnightlife.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.widget.ImageView;

import com.ingic.diningnightlife.BuildConfig;
import com.ingic.diningnightlife.fragments.ProfileFragment;
import com.ingic.diningnightlife.fragments.WriteReviewFragment;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

public class CameraHelper {

    public static final int WIDTH = 500;
    public static final int HEIGHT = 500;
    private static final int CAMERA_REQUEST = 1001;
    private static final int GALLERY_REQUEST = 1002;
    private static final int VIDEO_REQUEST = 1003;
    public static String mCurrentPhotoPath;
    private static Intent pictureActionIntent;
    private static String image_path_string = "";
    private static Uri uriImage;
    //	@Nullable
    private static File picture = null;
    //	@Nullable
    private static File video = null;

    public static void uploadFromGallery(Fragment fragment) {

        pictureActionIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        pictureActionIntent.setType("image/*");
        pictureActionIntent.putExtra("return-data", false);
        fragment.startActivityForResult(pictureActionIntent, GALLERY_REQUEST);
    }

    public static void uploadVideo(Fragment fragment) {

        pictureActionIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        pictureActionIntent.setType("video/*");
        pictureActionIntent.putExtra("return-data", false);
        fragment.startActivityForResult(pictureActionIntent, VIDEO_REQUEST);

    }

    public static void uploadFromGalleryAndVideo(Fragment fragment) {

        pictureActionIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        pictureActionIntent.setType("video/*, images/*");
        pictureActionIntent.putExtra("return-data", false);
        fragment.startActivityForResult(pictureActionIntent, GALLERY_REQUEST);

    }

    // public static void uploadMultipleFromGallery(Fragment fragment) {
    //
    // pictureActionIntent = new Intent(Intent.ACTION_PICK,
    // MediaStore.Images.Media.INTERNAL_CONTENT_URI);
    // pictureActionIntent.setType("image/*");
    // pictureActionIntent.putExtra("return-data", false);
    // pictureActionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
    // fragment.startActivityForResult(pictureActionIntent, GALLERY_REQUEST);
    //
    // }

    public static void uploadFromCamera(Fragment fragment, Context context) {

        pictureActionIntent = new Intent(
                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            uriImage = FileProvider.getUriForFile(context,
                    BuildConfig.APPLICATION_ID + ".provider",
                    createImageFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Funcs.addImageParamsExtra(pictureActionIntent, uriImage);
        fragment.startActivityForResult(pictureActionIntent, CAMERA_REQUEST);
    }

    private static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Dining&Nightlife");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }


    public static File retrievePicture(int requestCode, int resultCode,
                                       Intent data, Activity activity) {

        try {
            Uri selectedImageUri = data.getData();
            image_path_string = Funcs.getPath(selectedImageUri, activity);
            Bitmap bmp = BitmapFactory.decodeFile(image_path_string,
                    getOptions());
            bmp = BitmapHelper.getImageOrientation(image_path_string, bmp);
            if (bmp != null) {
                bmp = BitmapHelper.scaleCenterCrop(bmp, HEIGHT, WIDTH);
            }

            image_path_string = Funcs.replace(activity, bmp, image_path_string);
            bmp.recycle();
            bmp = null;
            picture = new File(image_path_string);

            // Drawable d = activity.getResources().getDrawable(
            // R.drawable.img_profile_photo);

            // imgPhoto.getLayoutParams().width = d.getIntrinsicWidth();
            // imgPhoto.getLayoutParams().height = d.getIntrinsicHeight();

            // ImageLoader.getInstance().displayImage(
            // "file:///" + image_path_string, imgPhoto);

            // llUploadPhoto.setVisibility(View.GONE);

            return picture;
        } catch (Exception e) {
            if (activity != null)
                Funcs.showShortToast("Unable to get image. Please try again..",
                        activity);
            return null;
        }

    }

    public static File retrieveAndDisplayPictureOfDrawableSize(int requestCode,
                                                               int resultCode, Intent data, Activity activity, Drawable d,
                                                               ImageView imgPhoto) {
        switch (requestCode) {

            case GALLERY_REQUEST:
                try {
                    Uri selectedImageUri = data.getData();
                    image_path_string = Funcs.getPath(selectedImageUri, activity);
                    // Bitmap bmp = BitmapFactory.decodeFile(image_path_string,
                    // getOptions());
                    // bmp = BitmapHelper.getImageOrientation(image_path_string,
                    // bmp);
                    //
                    // image_path_string = Funcs.replace(activity, bmp,
                    // image_path_string) ;
                    // bmp.recycle();
                    // bmp = null;
                    picture = new File(image_path_string);

                    imgPhoto.getLayoutParams().width = d.getIntrinsicWidth();
                    imgPhoto.getLayoutParams().height = d.getIntrinsicHeight();

                    ImageLoader.getInstance().displayImage(
                            "file:///" + image_path_string, imgPhoto);

                    return picture;

                } catch (Exception e) {
                    if (activity != null)
                        Funcs.showShortToast(
                                "Unable to get image. Please try again..", activity);
                    return null;
                }

            case CAMERA_REQUEST:
                try {
                    File pictureFile = null;
                    image_path_string = uriImage.getPath();
                    Bitmap bmp = BitmapFactory.decodeFile(image_path_string,
                            getOptions());
                    bmp = BitmapHelper.getImageOrientation(image_path_string, bmp);
                    if (bmp != null) {
                        bmp = BitmapHelper.scaleCenterCrop(bmp, HEIGHT, WIDTH);
                    }
                    image_path_string = Funcs.replace(activity, bmp,
                            image_path_string);
                    bmp.recycle();
                    bmp = null;
                    pictureFile = new File(image_path_string);

                    // d = activity.getResources().getDrawable(
                    // R.drawable.img_btn_signup_addphoto);

                    imgPhoto.getLayoutParams().width = d.getIntrinsicWidth();
                    imgPhoto.getLayoutParams().height = d.getIntrinsicHeight();

                    ImageLoader.getInstance().displayImage(
                            "file:///" + image_path_string, imgPhoto);
                    return pictureFile;
                } catch (Exception e) {
                    e.printStackTrace();
                    Funcs.showShortToast("Unable to get image. Please try again..",
                            activity);
                    return null;
                }

            default:
                return null;
        }
    }

    public static File retrieveAndDisplayPicture(int requestCode,
                                                 int resultCode, Intent data, Activity activity,
                                                 ImageView imgPhoto, String imageUri) {
        if (resultCode == RESULT_OK)
            switch (requestCode) {
                case GALLERY_REQUEST:
                    try {
                        Uri selectedImageUri = data.getData();
                        image_path_string = Funcs.getPath(selectedImageUri, activity);
                        // Bitmap bmp = BitmapFactory.decodeFile(image_path_string,
                        // getOptions());
                        // bmp = BitmapHelper.getImageOrientation(image_path_string,
                        // bmp);
                        //
                        // image_path_string = Funcs.replace(activity, bmp,
                        // image_path_string) ;
                        // bmp.recycle();
                        // bmp = null;
                        picture = new File(image_path_string);

                        ImageLoader.getInstance().displayImage(
                                "file:///" + image_path_string, imgPhoto);

                        return picture;
                    } catch (Exception e) {
                        if (activity != null)
                            Funcs.showShortToast(
                                    "Unable to get image. Please try again..", activity);
                        return null;
                    }

                case CAMERA_REQUEST:
                    File pictureFile;
                    Uri uri = Uri.parse(imageUri);
                    imageUri = uri.getPath();
                    Bitmap bmp = BitmapFactory.decodeFile(imageUri,
                            getOptions());
                    bmp = BitmapHelper.getImageOrientation(imageUri, bmp);
                    if (bmp != null) {
                        bmp = BitmapHelper.scaleCenterCrop(bmp, HEIGHT, WIDTH);
                    }
                    imageUri = Funcs.replace(activity, bmp,
                            imageUri);
                    bmp.recycle();
                    pictureFile = new File(imageUri);

                    ImageLoader.getInstance().displayImage(
                            "file:///" + imageUri, imgPhoto);
                    return pictureFile;
//                    File file = new File(imageUri);
//                    ImageLoaderHelper.loadImage("file://" + imageUri, imgPhoto);
//                    // ScanFile so it will be appeared on Gallery
//                    MediaScannerConnection.scanFile(activity,
//                            new String[]{imageUri}, null,
//                            new MediaScannerConnection.OnScanCompletedListener() {
//                                public void onScanCompleted(String path, Uri uri) {
//                                }
//                            });
//                    return file;
                default:
                    return null;

            }
        return null;
    }

    public static File retrieveVideo(int requestCode, int resultCode,
                                     Intent data, Activity activity) {

        try {
            Uri selectedImageUri = data.getData();
            image_path_string = Funcs.getPathForVideo(selectedImageUri, activity);

            video = new File(image_path_string);

            return video;
        } catch (Exception e) {
            if (activity != null)
                Funcs.showShortToast("Unable to get video. Please try again..",
                        activity);
            return null;
        }

    }

    private static BitmapFactory.Options getOptions() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        return options;
    }

    public static void uploadPhotoDialog(final ProfileFragment fragment,
                                         final Context context) {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(context);
        myAlertDialog.setTitle("Upload Photo");
        myAlertDialog.setMessage("How do you want to set your photo?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        uploadFromGallery(fragment);
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        fragment.uploadFromCamera();
                    }
                });
        myAlertDialog.show();
    }

    public static void uploadPhotoDialogForWriteReview(final WriteReviewFragment fragment,
                                                       final Context context) {
        AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(context);
        myAlertDialog.setTitle("Upload Photo");
        myAlertDialog.setMessage("How do you want to set your photo?");

        myAlertDialog.setPositiveButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        uploadFromGallery(fragment);
                    }
                });

        myAlertDialog.setNegativeButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        fragment.uploadFromCamera();
                    }
                });
        myAlertDialog.show();
    }

    public static void uploadPhotoAndVideoDialog(final Fragment fragment,
                                                 Activity activity) {

        AlertDialog.Builder alert = new AlertDialog.Builder(activity);
        alert.setTitle("Upload Media");
        alert.setMessage("Choose Media Type");

        alert.setPositiveButton("Photo",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        uploadFromGallery(fragment);
                    }
                });

        alert.setNegativeButton("Video",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        uploadVideo(fragment);
                    }
                });

        alert.show();

    }

}
