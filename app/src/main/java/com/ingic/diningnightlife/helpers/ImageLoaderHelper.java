package com.ingic.diningnightlife.helpers;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import com.ingic.diningnightlife.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;

/**
 * Created by developer on 8/19/17.
 */

public class ImageLoaderHelper {

    public static void loadImage(String url, ImageView img) {
        if (img == null) return;
        ImageLoader.getInstance()
                .displayImage(url, img);
    }


    public static void loadImageWithPicasso(Context context, String url, ImageView imageView, boolean isHomeBanner) {
        if (imageView == null || TextUtils.isEmpty(url)) return;
        if (isHomeBanner)
            Picasso.with(context)
                    .load(url)
                    .error(R.drawable.placeholder_banner_home)
                    .noPlaceholder()
                    .into(imageView);
        else {
            Picasso.with(context)
                    .load(url)
                    .error(R.drawable.placeholder_banner_other)
                    .noPlaceholder()
                    .into(imageView);
        }
    }
}
