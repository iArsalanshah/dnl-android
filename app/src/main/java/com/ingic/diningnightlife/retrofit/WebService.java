package com.ingic.diningnightlife.retrofit;


import com.ingic.diningnightlife.entities.AdvanceSearchDataResult;
import com.ingic.diningnightlife.entities.AllStore;
import com.ingic.diningnightlife.entities.BannerResult;
import com.ingic.diningnightlife.entities.DetailPageResult;
import com.ingic.diningnightlife.entities.FavoritesListResult;
import com.ingic.diningnightlife.entities.HomeResult;
import com.ingic.diningnightlife.entities.LoginResult;
import com.ingic.diningnightlife.entities.MagazineResult;
import com.ingic.diningnightlife.entities.NewsResult;
import com.ingic.diningnightlife.entities.NotificationsResult;
import com.ingic.diningnightlife.entities.PayFortData;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.ReviewResult;
import com.ingic.diningnightlife.entities.SavingResult;
import com.ingic.diningnightlife.entities.SharedVoucherList;
import com.ingic.diningnightlife.entities.SubscribeResult;
import com.ingic.diningnightlife.entities.SubscriptionResult;
import com.ingic.diningnightlife.entities.SuggestedStore;
import com.ingic.diningnightlife.entities.TnCResult;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface WebService {
    @FormUrlEncoded
    @POST("login")
    Call<ResponseWrapper<LoginResult>> login(
            @Field("email") String email,
            @Field("verification_code") String verification_code,
            @Field("device_type") String device_type,
            @Field("device_token") String device_token,
            @Field("email_update") int email_update
    );

    @FormUrlEncoded
    @POST("register")
    Call<ResponseWrapper<LoginResult>> register(
            @Field("full_name") String full_name,
            @Field("email") String email,
            @Field("phone_no") String phone_no,
            @Field("dob") String dob,
            @Field("socialmedia_id") String socialmedia_id,
            @Field("socialmedia_type") String socialmedia_type,
            @Field("device_type") String device_type,
            @Field("device_token") String device_token,
            @Field("email_update") int email_update
    );

    @GET("resendCode")
    Call<ResponseWrapper> resendCode(
            @Query("email") String email
    );

    @Multipart
    @POST("updateProfile")
    Call<ResponseWrapper<LoginResult>> updateProfile(
            @Part("full_name") RequestBody full_name,
            @Part("email") RequestBody email,
            @Part("phone_no") RequestBody phone_no,
            @Part("dob") RequestBody dob,
            @Part("verification_code") RequestBody verification_code,
            @Part("notification_status") RequestBody notification_status,
            @Part MultipartBody.Part profile_picture,
            @Part("user_id") RequestBody user_id
    );

    @FormUrlEncoded
    @POST("changePin")
    Call<ResponseWrapper<LoginResult>> changePin(
            @Field("user_id") int user_id,
            @Field("old_pin") String old_pin,
            @Field("new_pin") String new_pin
    );

    @FormUrlEncoded
    @POST("updateDeviceToken")
    Call<ResponseWrapper> updateDeviceToken(
            @Field("user_id") int user_id,
            @Field("device_token") String device_token,
            @Field("device_type") String device_type
    );

    @GET("getSubscriptions")
    Call<ResponseWrapper<ArrayList<SubscriptionResult>>> getSubscriptions(
            @Query("user_id") int user_id
    );


    @GET("checkSubscriptionDiscount")
    Call<ResponseWrapper<ArrayList<SubscriptionResult>>> getDiscountedSubscriptions(
            @Query("user_id") int user_id,
            @Query("subscription_id") int subscription_id,
            @Query("subscription_code") String subscription_code
    );

    @GET("getCms")
    Call<ResponseWrapper<TnCResult>> getTnC();

    @GET("getNews")
    Call<ResponseWrapper<ArrayList<NewsResult>>> getNews();

    @GET("getMagazine")
    Call<ResponseWrapper<MagazineResult>> getMagazine(
            @Query("city_id") String city_id
    );

    @Streaming
    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlAsync(@Url String fileUrl);

    @GET("favouriteListing")
    Call<ResponseWrapper<ArrayList<FavoritesListResult>>> favouriteListing(
            @Query("user_id") int user_id
    );

    @GET("addFavourite")
    Call<ResponseWrapper<FavoritesListResult>> addOrRemoveFavourite(
            @Query("user_id") int user_id,
            @Query("store_id") int store_id
    );

    @FormUrlEncoded
    @POST("contactUs")
    Call<ResponseWrapper> sendContactDetails(
            @Field("user_id") int user_id,
            @Field("full_name") String full_name,
            @Field("email_address") String email_address,
            @Field("phone_no") String phone_no,
            @Field("description") String description
    );

    @GET("getNotifications")
    Call<ResponseWrapper<ArrayList<NotificationsResult>>> getNotifications(
            @Query("user_id") int user_id
    );

    @GET("home")
    Call<ResponseWrapper<HomeResult>> getHomeData(
            @Query("city_id") String city_id
    );

    @GET("deleteNotification")
    Call<ResponseWrapper> deleteNotification(
            @Query("id") int id
    );

    @GET("getStores")
    Call<ResponseWrapper<ArrayList<AllStore>>> getStoresList(
            @Query("city_id") String city_id,
            @Query("type") String type
    );

    @GET("storeDetail")
    Call<ResponseWrapper<DetailPageResult>> getStoreDetails(
            @Query("user_id") int user_id,
            @Query("store_id") String store_id,
            @Query("type") String type
    );

    @GET("suggestedStores")
    Call<ResponseWrapper<ArrayList<SuggestedStore>>> getSuggestedStores(
            @Query("city_id") String city_id,
            @Query("store_id") String store_id,
            @Query("type") String type
    );

    @GET("getBanner")
    Call<ResponseWrapper<ArrayList<BannerResult>>> getBanner(
            @Query("city_id") String city_id,
            @Query("type") String type
    );

    @GET("getStoresFilter")
    Call<ResponseWrapper<ArrayList<AllStore>>> getStoresFilter(
            @Query("city_id") String city_id,
            @Query("type") String type,
            @Query("search") String search,
            @Query("latitude") double latitude,
            @Query("longitude") double longitude
    );

    @GET("advancedSearchData")
    Call<ResponseWrapper<AdvanceSearchDataResult>> getAdvanceSearchData(
            @Query("type") String type
    );

    @GET("advancedSearchResult")
    Call<ResponseWrapper<ArrayList<AllStore>>> advancedSearchResult(
            @Query("city_id") String city_id,
            @Query("type") String type,
            @Query("cuisine") String cuisine,
            @Query("venue_type") String venue_type,
            @Query("dietary") String dietary,
            @Query("quick_search") String quick_search,
            @Query("now_open") int isNowOpen,
            @Query("time_stamp") String timeStamp
    );

    @GET("generalSearch")
    Call<ResponseWrapper<ArrayList<AllStore>>> generalSearch(
            @Query("city_id") String city_id,
            @Query("search") String search
    );

    @GET("intellgentSearch")
    Call<ResponseWrapper<ArrayList<String>>> autoCompleteSearch(
            @Query("search") String search
    );

    @GET("getReviews")
    Call<ResponseWrapper<ArrayList<ReviewResult>>> getReviews(
            @Query("user_id") int user_id,
            @Query("store_id") String store_id
    );

    @FormUrlEncoded
    @POST("reviewLikeDislike")
    Call<ResponseWrapper> reviewLikeDislike(
            @Field("user_id") int user_id,
            @Field("store_id") String store_id,
            @Field("review_id") int review_id,
            @Field("status") String status
    );

    @FormUrlEncoded
    @POST("redeemVoucher")
    Call<ResponseWrapper<SubscribeResult>> sendRedeemVoucher(
            @Field("user_id") int user_id,
            @Field("store_id") String store_id,
            @Field("voucher_id") String voucher_id,
            @Field("type") String type,
            @Field("free_voucher_id") String free_voucher_id
    );

    @FormUrlEncoded
    @POST("purchaseSubscription")
    Call<ResponseWrapper> purchaseSubscription(
            @Field("user_id") int user_id,
            @Field("subscription_id") int subscription_id,
            @Field("subscription_code") String subscription_code,
            @Field("subscription_amount") String subscription_amount
    );

    @Multipart
    @POST("addReview")
    Call<ResponseWrapper> sendReviews(
            @Part("user_id") RequestBody user_id,
            @Part("store_id") RequestBody store_id,
            @Part("review") RequestBody review,
            @Part("quality") RequestBody quality,
            @Part("service") RequestBody service,
            @Part("ambiance") RequestBody ambiance,
            @Part("price") RequestBody price,
            @Part("overall_experience") RequestBody overall_experience,
            @Part("review_date") RequestBody review_date,
            @Part MultipartBody.Part image1,
            @Part MultipartBody.Part image2,
            @Part MultipartBody.Part image3
    );

    @FormUrlEncoded
    @POST("addShare")
    Call<ResponseWrapper> addShare(
            @Field("user_id") int user_id,
            @Field("store_id") String store_id,
            @Field("review_id") int review_id
    );

    @FormUrlEncoded
    @POST("shareVoucher")
    Call<ResponseWrapper> shareVoucher(
            @Field("user_id") int user_id,
            @Field("email") String email,
            @Field("voucher_detail") String voucher_detail
    );

    @GET("getSavings")
    Call<ResponseWrapper<ArrayList<SavingResult>>> getSavings(
            @Query("user_id") int user_id
    );

    @GET("shareVoucherList")
    Call<ResponseWrapper<SharedVoucherList>> getSharedVoucherList(
            @Query("user_id") int user_id
    );

    @GET("over21Year")
    Call<ResponseWrapper> setOver21Year(
            @Query("user_id") int user_id,
            @Query("over_21") int over_21
    );


    //Payfort Payment TOKENIZATION
    @FormUrlEncoded
//    @Headers("Content-Type: application/json")
//    @Headers("Content-Type: application/json")
//    @Headers("Accept-Encoding: identity")
//    @Headers({
//            "Content-Type: application/json",
//            "Accept-Encoding: identity"
//    })
    @POST("paymentPage")
    Call<PayFortData> getPayfortToken(
            @Field("service_command") String service_command,
            @Field("access_code") String access_code,
            @Field("merchant_identifier") String merchant_identifier,
            @Field("merchant_reference") String merchant_reference,
            @Field("language") String language,
            @Field("expiry_date") int expiry_date,
            @Field("card_number") String card_number,
            @Field("card_security_code") String card_security_code,
            @Field("signature") String signature,
            @Field("token_name") String token_name,
            @Field("card_holder_name") String card_holder_name,
            @Field("remember_me") String remember_me,
            @Field("return_url") String return_url
    );
}

