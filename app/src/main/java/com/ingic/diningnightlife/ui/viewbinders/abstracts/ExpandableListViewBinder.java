package com.ingic.diningnightlife.ui.viewbinders.abstracts;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

public abstract class ExpandableListViewBinder<T, E> {

    int mLayoutGroupId;
    int mLayoutChildId;

    public ExpandableListViewBinder() {

    }

    public ExpandableListViewBinder(int layoutGroupId, int layoutChildId) {
        mLayoutGroupId = layoutGroupId;
        mLayoutChildId = layoutChildId;
    }

    public View createGroupView(Activity activity) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(mLayoutGroupId, null);
        view.setTag(createGroupViewHolder(view));
        return view;

    }

    public View createChildView(Activity activity) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(mLayoutChildId, null);
        view.setTag(createChildViewHolder(view));
        return view;

    }


    /* public View createGroupViewUserInfo(Activity activity) {
         LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         final View view = inflater.inflate(mLayoutGroupId, null);
         view.setTag(createGroupViewHolderHeaderUserInfo(view));
         return view;

     }

     public View createChildViewUserInfo(Activity activity) {
         LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         final View view = inflater.inflate(mLayoutChildId, null);
         view.setTag(createChildViewHolderChildUserInfo(view));
         return view;

     }
 */
    public abstract BaseGroupViewHolder createGroupViewHolder(View view);

    public abstract BaseChildViewHolder createChildViewHolder(View view);

 /*   public abstract SettingUserInfoBinder.ViewHolder2 createGroupViewHolderHeaderUserInfo(View view);
    public abstract SettingUserInfoBinder.ViewHolder createChildViewHolderChildUserInfo(View view);

    public abstract SettingCarInfoBinder.ViewHolder2 createGroupViewHolderHeaderCarInfo(View view);
    public abstract SettingCarInfoBinder.ViewHolder createChildViewHolderChildCarInfo(View view);

    public abstract SettingPaymentDetailsBinder.ViewHolder2 createGroupViewHolderHeaderPaymentDeatils(View view);
    public abstract SettingPaymentDetailsBinder.ViewHolder createChildViewHolderChildCarPaymentDetails(View view);

    public abstract SettingInsuranceDetailsBinder.ViewHolder2 createGroupViewHolderHeaderInsuranceDeatils(View view);
    public abstract SettingInsuranceDetailsBinder.ViewHolder createChildViewHolderChildCarInsuranceDetails(View view);*/





    /*public abstract UssageInprogressBinder.ViewHolder2 createGroupViewHolder(View view);
    public abstract UssageInprogressBinder.ViewHolder createChildViewHolder(View view);*/


    /**
     * @param entity
     * @param position
     * @param grpPosition In cases applicable, for e.g in expandable listview
     * @param view
     * @param activity
     */
    public abstract void bindGroupView(T entity, int position, int grpPosition, View view, Activity activity);

    public abstract void bindChildView(E entity, int position, int grpPosition, View view, Activity activity);

    public abstract void onGroupExpand(int groupPosition);

    public abstract void onGroupCollapsed(int groupPosition);

    protected static class BaseGroupViewHolder {

    }

    protected static class BaseChildViewHolder {

    }
}

