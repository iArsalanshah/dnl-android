package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.DetailsPageRatingEntity;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.CustomRatingBar;

/**
 * Created by developer on 8/5/17.
 */

public class DetailsRatingsAdapter extends RecyclerViewListAdapter<DetailsPageRatingEntity> {
    private Context context;

    public DetailsRatingsAdapter(Context context, OnViewHolderClick listener) {
        super(context, listener);
        this.context = context;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_details_rating, viewGroup, false);
    }

    @Override
    protected void bindView(DetailsPageRatingEntity item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            AnyTextView txtTitle = (AnyTextView) viewHolder.getView(R.id.tv_itemDetailPage_rating);
            CustomRatingBar ratingBar = (CustomRatingBar) viewHolder.getView(R.id.rbRating);

            TextViewHelper.setText(txtTitle, item.getTitle());
            ratingBar.setScore(item.getRating());
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
