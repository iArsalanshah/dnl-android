package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.QuickSearch;
import com.ingic.diningnightlife.interfaces.CheckboxListener;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.Util;

/**
 * Created by developer on 8/10/17.
 */

public class CheckboxesAdapter extends RecyclerViewListAdapter<QuickSearch> {
    private CheckboxListener checkboxListener;
    private Context context;

    public CheckboxesAdapter(Context context, OnViewHolderClick listener, CheckboxListener checkboxListener) {
        super(context, listener);
        this.context = context;
        this.checkboxListener = checkboxListener;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_checkbox, viewGroup, false);
    }

    @Override
    protected void bindView(QuickSearch item, final RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            CheckBox cb = (CheckBox) viewHolder.getView(R.id.item_simpleCheckbox);
            if (Util.isNotNullEmpty(item.getName()))
                cb.setText(item.getName());

            //in some cases, it will prevent unwanted situations
            cb.setOnCheckedChangeListener(null);

            cb.setChecked(item.isSelected());

            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    checkboxListener.checkBoxClick(b, viewHolder.getAdapterPosition());
                }
            });
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return position;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
