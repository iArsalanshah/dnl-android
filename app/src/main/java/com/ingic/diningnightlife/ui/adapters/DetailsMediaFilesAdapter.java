package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.activities.YouTubeActivity;
import com.ingic.diningnightlife.entities.StorePhoto;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;

import java.util.Locale;

/**
 * Created by developer on 8/5/17.
 */

public class DetailsMediaFilesAdapter extends RecyclerViewListAdapter<StorePhoto> {
    private Context context;

    public DetailsMediaFilesAdapter(Context context, OnViewHolderClick listener) {
        super(context, listener);
        this.context = context;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.item_details_media_files, viewGroup, false);
        rootView.setTag(AppConstants.FILE_ADAPTER);
        return rootView;
    }

    @Override
    protected void bindView(final StorePhoto item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            ImageView imgMediaFile = (ImageView) viewHolder.getView(R.id.img_itemMediaFiles);
            ImageView imgPlayIcon = (ImageView) viewHolder.getView(R.id.img_itemMediaFiles_playIcon);

            if (item.getFileType() != null && item.getFileType().equals("video")) {
                imgPlayIcon.setVisibility(View.VISIBLE);
                String videoLink = item.getFile();
                String vId = videoLink.substring(videoLink.lastIndexOf('=') + 1, videoLink.length());
                String imgUrl = String.format(Locale.US, "https://img.youtube.com/vi/%s/mqdefault.jpg", vId);
                ImageLoaderHelper.loadImage(imgUrl, imgMediaFile);
                viewHolder.getView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        context.startActivity(new Intent(context, YouTubeActivity.class).putExtra(AppConstants.INTENT_VIDEO_LINK, item.getFile()));
                    }
                });
            } else {
                ImageLoaderHelper.loadImage(item.getPhotoImage(), imgMediaFile);
            }
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
