package com.ingic.diningnightlife.ui.views;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ingic.diningnightlife.R;

public class TitleBar extends RelativeLayout {

    private TextView txtTitle, textView;
    private ImageView btnLeft;
    private ImageView btnRight;

    private TextView txtCity;
    private SearchView etSearch;
    private ImageView imgBtnSearch;
    private RelativeLayout rlSearchBox;
    private AutoCompleteTextView autocompleteSearch;

    private View.OnClickListener menuButtonListener;
    private OnClickListener backButtonListener;

    private Context context;
    private TextView txtSkip;
    private ImageView btnFilter;

    public TitleBar(Context context) {
        super(context);
        this.context = context;
        initLayout(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
        if (attrs != null)
            initAttrs(context, attrs);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initLayout(context);
        if (attrs != null)
            initAttrs(context, attrs);
    }

    private void initAttrs(Context context, AttributeSet attrs) {

    }

    private void bindViews() {
        txtTitle = this.findViewById(R.id.txt_subHead);
        textView = this.findViewById(R.id.textView);
        btnLeft = this.findViewById(R.id.btnLeft);
        btnRight = this.findViewById(R.id.btnRight);
        btnFilter = this.findViewById(R.id.btnFilter);

        txtCity = this.findViewById(R.id.tv_titleBar_city);
        txtSkip = this.findViewById(R.id.tv_header_skip);
        etSearch = this.findViewById(R.id.et_titleBar_search);
        imgBtnSearch = this.findViewById(R.id.img_titleBar_search);
        rlSearchBox = this.findViewById(R.id.searchBox);
        autocompleteSearch = this.findViewById(R.id.autocomplete_search);
    }

    private void initLayout(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.header_main, this);
        bindViews();
    }

    public void hideButtons() {
        btnLeft.setVisibility(View.GONE);
        txtSkip.setVisibility(GONE);
        btnRight.setVisibility(View.GONE);
        hideFilter();
    }

    public void hideTitle() {
        txtTitle.setVisibility(View.GONE);
    }

    public void showBackButton() {
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(backButtonListener);
        btnLeft.setImageResource(R.drawable.ic_back);
    }

    public void showMenuButton() {
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(menuButtonListener);
        btnLeft.setImageResource(R.drawable.ic_menu);
    }

    public void setRightButton(int resourceId, OnClickListener listener) {
        txtSkip.setVisibility(GONE);
        btnRight.setVisibility(VISIBLE);
        btnRight.setImageResource(resourceId);
        btnRight.setOnClickListener(listener);
    }

    public void setCityButton(OnClickListener listener) {
        txtCity.setOnClickListener(listener);
    }

    public void setSubHeading(String heading) {
        //searchBox should be hide before showing title
        rlSearchBox.setVisibility(GONE);

        txtTitle.setVisibility(View.VISIBLE);
        txtTitle.setText(heading);
    }

    public void showSearchBarForListing(String cityName, OnClickListener btnSearchBarListener) {
        //hide title to show searchBar
        txtTitle.setVisibility(View.GONE);
        //show searchBar
        rlSearchBox.setVisibility(VISIBLE);

        //update
        rlSearchBox.setOnClickListener(btnSearchBarListener);
        etSearch.clearFocus();
        etSearch.setVisibility(GONE);
        textView.setVisibility(VISIBLE);

        autocompleteSearch.setVisibility(GONE);
        //clear Text
//        etSearch.setQuery("", false);
        //searchbar right city text
        txtCity.setText(cityName);
//        etSearch.clearFocus();
//        etSearch.setFocusable(false);
//        etSearch.setFocusableInTouchMode(false);
        imgBtnSearch.setOnClickListener(btnSearchBarListener);
        imgBtnSearch.setVisibility(VISIBLE);
    }

    public void setSearchBarForSearchResult(String cityName) {
        //hide title to show searchBar
        txtTitle.setVisibility(View.GONE);
        //show searchBar
        rlSearchBox.setVisibility(VISIBLE);
        etSearch.setVisibility(VISIBLE);
        textView.setVisibility(GONE);//UPDATE
        autocompleteSearch.setVisibility(GONE);
        //clear Text
        etSearch.setQuery("", false);
        //searchbar right city text
        txtCity.setText(cityName);
        etSearch.clearFocus();
        etSearch.setFocusable(false);
        etSearch.setFocusableInTouchMode(false);
        imgBtnSearch.setOnClickListener(null);
        imgBtnSearch.setVisibility(INVISIBLE);
    }

    public void showAutoComplete(String cityId, String cityName, final Context context, AdapterView.OnItemClickListener listener,
                                 OnClickListener searchBtnListener) {
        //hide title to show searchBar
        txtTitle.setVisibility(View.GONE);
        //show searchBar
        rlSearchBox.setVisibility(VISIBLE);
        etSearch.setVisibility(GONE);
        autocompleteSearch.setVisibility(VISIBLE);
        autocompleteSearch.setText("");
        txtCity.setText(cityName);
        int fullWidth = context.getResources().getDisplayMetrics().widthPixels;
        autocompleteSearch.setDropDownWidth(fullWidth);
        autocompleteSearch.setDropDownVerticalOffset((int) Util.convertDpToPixel(8, context));
        autocompleteSearch.setOnItemClickListener(listener);
        imgBtnSearch.setOnClickListener(searchBtnListener);
    }

    public String getSearchText() {
        return (etSearch != null) ? etSearch.getQuery().toString() : "";
    }

    public void showFilter(OnClickListener listener) {
        btnFilter.setVisibility(VISIBLE);
        btnFilter.setOnClickListener(listener);
    }

    public void hideFilter() {
        btnFilter.setVisibility(GONE);
    }

    public AutoCompleteTextView getAutoCompleteTextView() {
        return autocompleteSearch;
    }

    public void clearSearchText() {
        if (etSearch != null) {
            etSearch.setQuery("", false);
        }
    }

    public void showSkipText(OnClickListener listener) {
        txtSkip.setVisibility(VISIBLE);
        txtSkip.setOnClickListener(listener);
    }

    public SearchView getSearchView() {
        return etSearch;
    }

    public void showTitleBar() {
        this.setVisibility(View.VISIBLE);
    }

    public void hideTitleBar() {
        this.setVisibility(View.GONE);
    }

    public void setMenuButtonListener(View.OnClickListener listener) {
        menuButtonListener = listener;
    }

    public void setBackButtonListener(View.OnClickListener listener) {
        backButtonListener = listener;
    }
}