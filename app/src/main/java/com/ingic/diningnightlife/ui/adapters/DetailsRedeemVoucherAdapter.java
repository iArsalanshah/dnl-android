package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.StoreVoucher;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.helpers.DateHelper;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.interfaces.OnRedeemVoucherClicked;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.interfaces.OnVoucherCheckBoxCL;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;

import java.util.Locale;
import java.util.Random;

/**
 * Created by developer on 8/5/17.
 */

public class DetailsRedeemVoucherAdapter extends RecyclerViewListAdapter<StoreVoucher> {
    private Context context;
    private OnRedeemVoucherClicked onRedeemVoucherClicked;
    private OnVoucherCheckBoxCL onVoucherCheckBoxCL;

    public DetailsRedeemVoucherAdapter(Context context, OnViewHolderClick listener, OnRedeemVoucherClicked onRedeemVoucherClicked,
                                       OnVoucherCheckBoxCL onVoucherCheckBoxCL) {
        super(context, listener);
        this.context = context;
        this.onRedeemVoucherClicked = onRedeemVoucherClicked;
        this.onVoucherCheckBoxCL = onVoucherCheckBoxCL;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_details_redeem_voucher, viewGroup, false);
    }

    @Override
    protected void bindView(final StoreVoucher item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            final int position = viewHolder.getAdapterPosition();
            AnyTextView txtCapsule = (AnyTextView) viewHolder.getView(R.id.tv_itemRedeemVoucher_capsule);
            AnyTextView txtTitle = (AnyTextView) viewHolder.getView(R.id.tv_itemRedeemVoucher_title);
            AnyTextView txtDesc = (AnyTextView) viewHolder.getView(R.id.tv_itemRedeemVoucher_desc);
            CheckBox cb = (CheckBox) viewHolder.getView(R.id.cb_itemRedeemVoucher);

            cb.setOnCheckedChangeListener(null);
            cb.setChecked(false);

            if (item.getIsRedeemed() == 1){
                //make it grayed out
                viewHolder.getView().setAlpha(0.5f);
                cb.setEnabled(false);
                viewHolder.getView().setOnClickListener(null);
            } else {
                cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        onVoucherCheckBoxCL.onCheckBoxClick(item, position, isChecked);
                    }
                });

                viewHolder.getView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (onRedeemVoucherClicked != null)
                            onRedeemVoucherClicked.onRedeemItemClicked(item.getVoucher(), String.valueOf(item.getId()), item.getType());
                    }
                });
            }


            TextViewHelper.setText(txtTitle, item.getTitle());
            TextViewHelper.setText(txtCapsule, item.getVoucher());
            String desc = String.format(Locale.US, "Valid for up to %s people until %s", item.getValidUpto(),
                    DateHelper.dateFormatGMT(item.getValidDate(), "MMM dd yyyy", AppConstants.DATE_FORMAT_DATE));
            TextViewHelper.setText(txtDesc, desc);
            txtTitle.setTextColor(getColor(item.getColorCode()));
            setBackground(txtCapsule, context, getColor(item.getColorCode()));

//            if (item.getCapsuleEntity().getName() != null &&
//                    !item.getCapsuleEntity().getName().isEmpty()) {
//                txtCapsule.setText(item.getCapsuleEntity().getName());
//                if (item.getCapsuleEntity().getLeftDrawable() != null) {
//                    txtCapsule.setCompoundDrawablesWithIntrinsicBounds(
//                            ContextCompat.getDrawable(context, item.getCapsuleEntity().getLeftDrawable()),
//                            null, null, null);
//                }
//            } else if (item.getCapsuleEntity().getImgResource() != 0) {
//                txtCapsule.setCompoundDrawablesWithIntrinsicBounds(
//                        ContextCompat.getDrawable(context, item.getCapsuleEntity().getImgResource()),
//                        null, null, null);
//            }
        }
    }

    private int getColor(String color) {
        try {
            return Color.parseColor(color);
        } catch (Exception ex) {
            Log.e("PARSE COLOR", "getColor: ");
            return ContextCompat.getColor(context, R.color.colorOrange);
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }

    private void setBackground(TextView textView, Context context, int color) {
        Drawable tempDrawable = ContextCompat.getDrawable(context, R.drawable.drawable_capsule_redeem_voucher);
        LayerDrawable bubble = (LayerDrawable) tempDrawable; //(cast to root element in xml)
        GradientDrawable solidColor = (GradientDrawable) bubble.findDrawableByLayerId(R.id.outerRectangle);
        solidColor.setColor(color);
        textView.setBackground(tempDrawable);
    }

    private int getRandomBackgroundColor(Context context) {
        Random ran = new Random();
        int x = ran.nextInt(6) + 1; //range 1 to 7
        switch (x) {
            case 1:
                x = ContextCompat.getColor(context, R.color.colorBlueGrid);
                break;
            case 2:
                x = ContextCompat.getColor(context, R.color.colorOrangeDarkGrid);
                break;
            case 3:
                x = ContextCompat.getColor(context, R.color.colorPurpleGrid);
                break;
            case 4:
                x = ContextCompat.getColor(context, R.color.colorPinkGrid);
                break;
            case 5:
                x = ContextCompat.getColor(context, R.color.colorGreenGrid);
                break;
            case 6:
                x = ContextCompat.getColor(context, R.color.colorOrangeLightGrid);
                break;
            default://case 7 or default color
                x = ContextCompat.getColor(context, R.color.colorBlueGrid);
                break;
        }
        return x;
    }
}
