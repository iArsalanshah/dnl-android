package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.HomeGridEntity;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;

/**
 * Created by developer on 7/28/17.
 */

public class HomeGridAdapter extends RecyclerViewListAdapter<HomeGridEntity> {
    private Context context;

    public HomeGridAdapter(Context context, OnViewHolderClick listener) {
        super(context, listener);
        this.context = context;
    }

    @Override
    protected int bindItemViewType(int position) {
        if (position == 3) {
            return 1;
        }
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.item_grid_options, viewGroup, false);
        if (viewType == 0)
            rootView.setTag(AppConstants.GRID_TAG_CLICKED);
        else rootView.setTag(AppConstants.GRID_TAG_LATEST_EDITION);
        return rootView;
    }

    @Override
    protected void bindView(HomeGridEntity item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            ImageView imgIcon = (ImageView) viewHolder.getView(R.id.img_gridOption_icon);
            AnyTextView textTitle = (AnyTextView) viewHolder.getView(R.id.tv_gridOption_text);

            Drawable tempDrawable = ContextCompat.getDrawable(context, R.drawable.drawable_grid_circle);
            LayerDrawable bubble = (LayerDrawable) tempDrawable; //(cast to root element in xml)
            GradientDrawable solidColor = (GradientDrawable) bubble.findDrawableByLayerId(R.id.outerRectangle);
            solidColor.setColor(item.getImgColor());
            imgIcon.setBackground(tempDrawable);
            imgIcon.setImageResource(item.getImgResource());
            textTitle.setText(item.getName());

//            if (viewHolder.getAdapterPosition() == 4) {
//                viewHolder.getView(R.id.extraSize).setVisibility(View.VISIBLE);
//            } else viewHolder.getView(R.id.extraSize).setVisibility(View.GONE);
        }
    }
}