package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.StoreVoucher;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.helpers.DateHelper;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;


public class ShareVoucherAdapter extends RecyclerViewListAdapter<StoreVoucher> {
    public ShareVoucherAdapter(Context context) {
        super(context);
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_share_voucher_list, viewGroup, false);
    }

    @Override
    protected void bindView(StoreVoucher item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            ImageView img = (ImageView) viewHolder.getView(R.id.img_shareVoucher);
            TextView tvTitle = (TextView) viewHolder.getView(R.id.tv_shareVoucher_title);
            TextView tvDesc = (TextView) viewHolder.getView(R.id.tv_shareVoucher_validity);
            tvTitle.setTextColor(getColor(item.getColorCode()));
            ImageLoaderHelper.loadImage(item.getProfilePicture(), img);
            TextViewHelper.setText(tvTitle, item.getTitle());
            TextViewHelper.setText(tvDesc, String.format("Valid for up to %s people until %s", item.getValidUpto(),
                    DateHelper.dateFormatGMT(item.getValidDate(), "MMM dd yyyy", AppConstants.DATE_FORMAT_DATE)));
        }
    }

    private int getColor(String color) {
        try {
            return Color.parseColor(color);
        } catch (Exception ex) {
            return Color.parseColor("#E76A30");
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
