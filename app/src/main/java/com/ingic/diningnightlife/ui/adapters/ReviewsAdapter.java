package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.ReviewResult;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.interfaces.ReviewListeners;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.CustomRatingBar;

import java.util.List;

import static com.ingic.diningnightlife.fragments.ReviewsFragment.DISLIKE;
import static com.ingic.diningnightlife.fragments.ReviewsFragment.LIKE;

public class ReviewsAdapter extends RecyclerViewListAdapter<ReviewResult> {
    private static final String LIKED = "like";
    private static final String DISLIKED = "dislike";
    private static final int USER_REPLY = 0;
    private static final int ADMIN_REPLY = 1;
    private ReviewListeners reviewListener;

    public ReviewsAdapter(Context context, ReviewListeners reviewListener) {
        super(context);
        this.reviewListener = reviewListener;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (viewType == USER_REPLY)
            return inflater.inflate(R.layout.item_reviews, viewGroup, false);
        else return inflater.inflate(R.layout.item_review_admin, viewGroup, false);
    }

    @Override
    protected void bindView(final ReviewResult item, final RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            //for admin reply
            if (viewHolder.getItemViewType() == ADMIN_REPLY) {
                for (ReviewResult obj :
                        item.getReplys()) {
                    if (obj.isSuperAdmin()) { //Super Admin
                        RelativeLayout layout = (RelativeLayout) viewHolder.getView(R.id.containerAdmin);
                        layout.setVisibility(View.VISIBLE);

                        ImageView profileImage = (ImageView) viewHolder.getView(R.id.img_itemReviewAdmin);
                        AnyTextView tvName = (AnyTextView) viewHolder.getView(R.id.tv_itemReviewAdminName);
                        AnyTextView tvMessage = (AnyTextView) viewHolder.getView(R.id.tv_itemReviewAdminMsg);
                        ImageLoaderHelper.loadImage(obj.getProfilePicture(), profileImage);
                        tvName.setText(obj.getUserName());
                        tvMessage.setText(obj.getReview());

                    } else { //Sub Admin
                        RelativeLayout layout = (RelativeLayout) viewHolder.getView(R.id.containerSubAdmin);
                        layout.setVisibility(View.VISIBLE);

                        ImageView image = (ImageView) viewHolder.getView(R.id.subAdminPicture);
                        AnyTextView name = (AnyTextView) viewHolder.getView(R.id.subAdminName);
                        AnyTextView review = (AnyTextView) viewHolder.getView(R.id.subAdminReview);
                        ImageLoaderHelper.loadImage(obj.getProfilePicture(), image);
                        name.setText(obj.getUserName());
                        review.setText(obj.getReview());
                    }
                }
            }


            ImageView profileImage = (ImageView) viewHolder.getView(R.id.img_itemReviewProfileImage);
            AnyTextView tvName = (AnyTextView) viewHolder.getView(R.id.tv_itemReviewProfileName);
            AnyTextView tvMessage = (AnyTextView) viewHolder.getView(R.id.tv_itemReviewMessage);
            final AnyTextView tvLike = (AnyTextView) viewHolder.getView(R.id.tv_itemReviewLike);
            final AnyTextView tvDislike = (AnyTextView) viewHolder.getView(R.id.tv_itemReviewDislike);
            final AnyTextView tvShare = (AnyTextView) viewHolder.getView(R.id.tv_itemReviewShare);
            ImageView imgGallery1 = (ImageView) viewHolder.getView(R.id.img_itemReviews_galleryPic1);
            ImageView imgGallery2 = (ImageView) viewHolder.getView(R.id.img_itemReviews_galleryPic2);
            ImageView imgGallery3 = (ImageView) viewHolder.getView(R.id.img_itemReviews_galleryPic3);
            CustomRatingBar rbQuality = (CustomRatingBar) viewHolder.getView(R.id.rbQuality);
            CustomRatingBar rbService = (CustomRatingBar) viewHolder.getView(R.id.rbService);
            CustomRatingBar rbAmbiance = (CustomRatingBar) viewHolder.getView(R.id.rbAmbiance);
            CustomRatingBar rbPrice = (CustomRatingBar) viewHolder.getView(R.id.rbPrice);
            CustomRatingBar rbOverAllExperience = (CustomRatingBar) viewHolder.getView(R.id.rbOverAllExperience);


            ImageLoaderHelper.loadImage(item.getProfilePicture(), profileImage);
            tvName.setText(item.getUserName());
            tvMessage.setText(item.getReview());
            tvLike.setText(item.getLikeCount() + "");
            tvDislike.setText(item.getDislikeCount() + "");

            TextViewHelper.setText(tvShare, String.valueOf(item.getShareCount()));

            if (item.getIsLike() == LIKE) {
                tvLike.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.like_selected, 0, 0, 0);
            } else
                tvLike.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.like_unselected, 0, 0, 0);

            if (item.getIsDislike() == DISLIKE) {
                tvDislike.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.dislike_selected, 0, 0, 0);
            } else
                tvDislike.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.dislike_unselected, 0, 0, 0);

            if (item.getReviewImages() != null) {
                if (item.getReviewImages().size() > 0 && item.getReviewImages().get(0) != null) {
                    ImageLoaderHelper.loadImage(item.getReviewImages().get(0).getReviewImage(), imgGallery1);
                    imgGallery1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            reviewListener.imageClick(item.getReviewImages(), 0);
                        }
                    });
                    if (item.getReviewImages().size() > 1 && item.getReviewImages().get(1) != null) {
                        ImageLoaderHelper.loadImage(item.getReviewImages().get(1).getReviewImage(), imgGallery2);
                        imgGallery2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                reviewListener.imageClick(item.getReviewImages(), 1);
                            }
                        });
                    }
                    if (item.getReviewImages().size() > 2 && item.getReviewImages().get(2) != null) {
                        ImageLoaderHelper.loadImage(item.getReviewImages().get(2).getReviewImage(), imgGallery3);
                        imgGallery3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                reviewListener.imageClick(item.getReviewImages(), 2);
                            }
                        });
                    }
                }
            }

            //disable star scrolling
            rbQuality.setScrollToSelect(false);
            rbService.setScrollToSelect(false);
            rbAmbiance.setScrollToSelect(false);
            rbPrice.setScrollToSelect(false);
            rbOverAllExperience.setScrollToSelect(false);

            rbQuality.setScore(item.getQuality());
            rbService.setScore(item.getService());
            rbAmbiance.setScore(item.getAmbiance());
            rbPrice.setScore(item.getPrice());
            rbOverAllExperience.setScore(item.getOverallExperience());

            if (item.getIsLike() == 0) {
                tvLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        tvLike.setClickable(false);
                        reviewListener.likeOrDislike(item.getId(), LIKED);
                    }
                });
            } else {
                tvLike.setOnClickListener(null);
            }

            if (item.getIsDislike() == 0) {
                tvDislike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        tvDislike.setClickable(false);
                        reviewListener.likeOrDislike(item.getId(), DISLIKED);
                    }
                });
            } else {
                tvDislike.setOnClickListener(null);
            }

            tvShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvShare.setClickable(false);
                    if (reviewListener != null) {
                        reviewListener.share(item.getId(), item);
                    }
                }
            });
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        List<ReviewResult> replies = (getList() != null && getList().size() > position) ? getList().get(position).getReplys() : null;
        if (replies == null || replies.size() == 0)
            return USER_REPLY;
        else return ADMIN_REPLY;
    }

    @Override
    protected int bindItemId(int position) {
        return getItem(position).getId();
    }
}