package com.ingic.diningnightlife.ui.views;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.webkit.URLUtil;
import android.widget.TextView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.helpers.UIHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class Util {

    public static Map<String, Typeface> typefaceCache = new HashMap<String, Typeface>();

    @SuppressLint("StringFormatInvalid")
    public static void setTypeface(AttributeSet attrs, TextView textView) {
        Context context = textView.getContext();

        TypedArray values = context.obtainStyledAttributes(attrs,
                R.styleable.AnyTextView);
        String typefaceName = values
                .getString(R.styleable.AnyTextView_typeface);

        if (typefaceCache.containsKey(typefaceName)) {
            textView.setTypeface(typefaceCache.get(typefaceName));
        } else {
            Typeface typeface;
            try {
                typeface = Typeface.createFromAsset(textView.getContext()
                                .getAssets(),
                        context.getString(R.string.assets_fonts_folder)
                                + typefaceName);
            } catch (Exception e) {
                Log.v(context.getString(R.string.app), String.format(
                        context.getString(R.string.typeface_not_found),
                        typefaceName));
                return;
            }

            typefaceCache.put(typefaceName, typeface);
            textView.setTypeface(typeface);
        }
        values.recycle();
    }

    @SuppressLint("StringFormatInvalid")
    public static void setTypefaceUpdated(AttributeSet attrs, TextView textView) {
        Context context = textView.getContext();

        TypedArray values = context.obtainStyledAttributes(attrs,
                R.styleable.AnyTextView);
        String typefaceName = values
                .getString(R.styleable.AnyTextView_typeface);

        if (typefaceCache.containsKey(typefaceName)) {
            textView.setTypeface(typefaceCache.get(typefaceName));
        } else {
            Typeface typeface;
            try {
                typeface = Typeface.createFromAsset(textView.getContext()
                                .getAssets(),
                        context.getString(R.string.assets_fonts_folder)
                                + typefaceName);
            } catch (Exception e) {
                Log.v(context.getString(R.string.app), String.format(
                        context.getString(R.string.typeface_not_found),
                        typefaceName));
                return;
            }

            typefaceCache.put(typefaceName, typeface);
            textView.setTypeface(typeface);
        }

        values.recycle();
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    public static boolean isNotNullEmpty(@Nullable String string) {
        return !(string == null || string.trim().length() == 0);
    }

    public static void watchYoutubeVideo(Context context, String id) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + id));
        try {
            context.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            context.startActivity(webIntent);
        }
    }

    public static int getParsedInteger(String discountAmount) {
        try {
            return Integer.parseInt(discountAmount);
        } catch (Exception ex) {
            return 0;
        }
    }

    public static String getFormedInteger(int num) {
        return num < 10 ? "0" + num : num + "";
    }

    public static double getParsedDouble(String val) {
        try {
            return Double.parseDouble(val);
        } catch (Exception ex) {
            return 0.0;
        }
    }

    public static long getParsedLong(String val) {
        try {
            return Long.parseLong(val);
        } catch (Exception ex) {
            return 0;
        }
    }

    public static float getParsedFloat(String val) {
        try {
            return Float.parseFloat(val);
        } catch (Exception ex) {
            return 0;
        }
    }

    public static void openCustomChromeTabs(Context context, @NonNull String url) {
        String errorMessage = context.getResources().getString(R.string.error_url_not_valid);
        if (TextUtils.isEmpty(url) && !URLUtil.isValidUrl(url) && !Patterns.WEB_URL.matcher(url).matches()) {
            UIHelper.showLongToastInCenter(context, errorMessage);
            return;
        }

        try {
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder
                    .setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setStartAnimations(context, R.anim.slide_in_right, R.anim.slide_out_left)
                    .setExitAnimations(context, android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .build();
            customTabsIntent.launchUrl(context, Uri.parse(url));
        } catch (Exception ex) {
            ex.printStackTrace();
            UIHelper.showShortToastInCenter(context, ex.getMessage());
//            UIHelper.showShortToastInCenter(context,context.getResources().getString(R.string.failure_response));
        }
    }

    public static long Daybetween(String date1, String date2, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
        Date Date1 = null, Date2 = null;
        try {
            Date1 = sdf.parse(date1);
            Date2 = sdf.parse(date2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (Date2.getTime() - Date1.getTime()) / (24 * 60 * 60 * 1000);
    }

    public static Date getFormatedDate(String mString) {
//        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd hh:mm:ss");
//        return formatter.parseDateTime(mString);
        Date date = null;
//        String dtStart = "2010-10-15T09:27:37Z";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = format.parse(mString);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}