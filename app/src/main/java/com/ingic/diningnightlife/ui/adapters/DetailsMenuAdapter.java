package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.StoreMenu;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;

/**
 * Created by developer on 8/5/17.
 */

public class DetailsMenuAdapter extends RecyclerViewListAdapter<StoreMenu> {
    private Context context;

    public DetailsMenuAdapter(Context context, OnViewHolderClick listener) {
        super(context, listener);
        this.context = context;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.item_details_menu, viewGroup, false);
        rootView.setTag(AppConstants.MENU_ADAPTER);
        return rootView;
    }

    @Override
    protected void bindView(final StoreMenu item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            ImageView imgMenu = (ImageView) viewHolder.getView();
            if (imgMenu != null) {
                ImageLoaderHelper.loadImage(item.getMenuImage(), imgMenu);
            }
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
