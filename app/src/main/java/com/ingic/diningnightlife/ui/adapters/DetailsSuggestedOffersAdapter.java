package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.SuggestedStore;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.interfaces.OnSuggestedClicked;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;

/**
 * Created by developer on 8/5/17.
 */

public class DetailsSuggestedOffersAdapter extends RecyclerViewListAdapter<SuggestedStore> {
    private Context context;
    private OnSuggestedClicked onSuggestedClicked;

    public DetailsSuggestedOffersAdapter(Context context, OnViewHolderClick listener, OnSuggestedClicked onSuggestedClicked) {
        super(context, listener);
        this.context = context;
        this.onSuggestedClicked = onSuggestedClicked;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_details_suggested_offers, viewGroup, false);
    }

    @Override
    protected void bindView(SuggestedStore item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            ImageView imgMenu = (ImageView) viewHolder.getView();
            if (imgMenu != null) {
                ImageLoaderHelper.loadImage(item.getProfile_image(), imgMenu);
                final int storeId = item.getId();
                if (onSuggestedClicked != null) {
                    imgMenu.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onSuggestedClicked.onSuggestedStoreClicked(storeId);
                        }
                    });
                }
            }
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
