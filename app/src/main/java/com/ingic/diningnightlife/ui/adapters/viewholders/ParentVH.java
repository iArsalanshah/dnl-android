package com.ingic.diningnightlife.ui.adapters.viewholders;


import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.ParentEntity;


public class ParentVH extends ParentViewHolder {
    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;

    private TextView mTitle;
    private TextView tvCount;
    private ImageView arrowDownIcon;

    public ParentVH(View itemView) {
        super(itemView);
        mTitle = itemView.findViewById(R.id.tv_item_parent_myVouchers_title);
        tvCount = itemView.findViewById(R.id.tv_item_parent_myVouchers_count);
        arrowDownIcon = itemView.findViewById(R.id.img_arrow_down);
        arrowDownIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isExpanded()) collapseView();
                else expandView();
            }
        });
    }

    public void bind(@NonNull ParentEntity entity) {
        mTitle.setText(entity.getTitle());
        if (entity.getChildList() != null) {
            tvCount.setText(String.valueOf(entity.getChildList().size()));
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void setExpanded(boolean expanded) {
        super.setExpanded(expanded);
        if (expanded) {
            arrowDownIcon.setRotation(ROTATED_POSITION);
        } else {
            arrowDownIcon.setRotation(INITIAL_POSITION);
        }
    }

    @Override
    public void onExpansionToggled(final boolean expanded) {
        super.onExpansionToggled(expanded);
        RotateAnimation rotateAnimation;
        if (expanded) { // rotate clockwise
            rotateAnimation = new RotateAnimation(ROTATED_POSITION,
                    INITIAL_POSITION,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        } else { // rotate counterclockwise
            rotateAnimation = new RotateAnimation(-1 * ROTATED_POSITION,
                    INITIAL_POSITION,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                    RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        }

        rotateAnimation.setDuration(200);
        rotateAnimation.setFillAfter(true);
        arrowDownIcon.startAnimation(rotateAnimation);
    }
}
