package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.AllStore;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.StoreTag;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.retrofit.WebService;
import com.ingic.diningnightlife.retrofit.WebServiceFactory;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.CustomRatingBar;
import com.ingic.diningnightlife.ui.views.FlowLayout;
import com.ingic.diningnightlife.ui.views.Util;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AutocompleteSearchAdapter extends ArrayAdapter implements Filterable {
    private static final int MIN_CHARECTER_SEARCH = 3;
    private final Context context;
    private final String cityId;
    private ArrayList<AllStore> storeArrayList;
    private LayoutInflater inflater;
    private boolean isCustomFilter;

    public AutocompleteSearchAdapter(@NonNull Context context, @LayoutRes int resource, String city_id) {
        super(context, resource);
        this.context = context;
        this.cityId = city_id;
        storeArrayList = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return storeArrayList.size();
    }

    @Override
    public AllStore getItem(int position) {
        return storeArrayList.get(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null && (isCustomFilter || constraint.length() >= MIN_CHARECTER_SEARCH)) {
                    isCustomFilter = false;
                    try {
                        //get data from the web
                        storeArrayList.clear();
                        getSearchedData(constraint.toString());
                    } catch (Exception e) {
                        Log.d("TAG", "EXCEPTION " + e);
                    }
                    filterResults.values = storeArrayList;
                    filterResults.count = storeArrayList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }

    public void getCustomFilterResult(@NonNull String text) {
        isCustomFilter = true;
        getFilter().filter(text);
    }

    @RestAPI
    private void getSearchedData(String key) {
        WebService webService = WebServiceFactory.getWebServiceInstanceWithDefaultInterceptor(context, WebServiceConstants.SERVICE_BASE_URL);
        webService.generalSearch(cityId, key).enqueue(new Callback<ResponseWrapper<ArrayList<AllStore>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<AllStore>>> call, Response<ResponseWrapper<ArrayList<AllStore>>> response) {
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(context, context.getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    //home data
                    storeArrayList = response.body().getResult();
                    notifyDataSetChanged();
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(context, response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(context, context.getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<AllStore>>> call, Throwable t) {
                UIHelper.showLongToastInCenter(context, t.getMessage());
            }
        });
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(R.layout.item_offers_list, parent, false);

        AllStore item = storeArrayList.get(position);
        ImageView imgNew = convertView.findViewById(R.id.img_itemOffers_new);
        ImageView imgResource = convertView.findViewById(R.id.img_itemOffersList);
        ImageView ic_taq = convertView.findViewById(R.id.ic_taq);
        AnyTextView textTitle = convertView.findViewById(R.id.tv_offersList_title);
        AnyTextView textValueCuisine = convertView.findViewById(R.id.tv_offersList_cuisine);
        AnyTextView textAddress = convertView.findViewById(R.id.tv_offersList_address);
        CustomRatingBar ratingBar = convertView.findViewById(R.id.rbRating);
        FlowLayout flTags = convertView.findViewById(R.id.fl_offersList_tags);
        flTags.removeAllViews();

        ImageLoaderHelper.loadImage(item.getProfileImage(), imgResource);
        TextViewHelper.setText(textTitle, item.getFullName());
        TextViewHelper.setText(textAddress, item.getLocation());
        ratingBar.setScore(item.getTotalRating());

        Date currentTime = Calendar.getInstance().getTime();

        int daysBetween;
        if (item.getCreatedAt() != null) {
            daysBetween = Days.daysBetween(new DateTime(Util.getFormatedDate(item.getCreatedAt())), new DateTime(currentTime)).getDays();

//            if (item.getCreatedAt() != null && Util.Daybetween(String.valueOf(currentTime), item.getCreatedAt(), pattern) < 7) {
            if (daysBetween < AppConstants.NEW_TAG_DAYS_COUNT) {
                imgNew.setVisibility(View.VISIBLE);
            } else imgNew.setVisibility(View.GONE);
        } else imgNew.setVisibility(View.GONE);


        List<StoreTag> tagsList = item.getStoreTags();
        //Tags
        if (tagsList != null && tagsList.size() > 0) {
            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(
                    FlowLayout.LayoutParams.WRAP_CONTENT,
                    FlowLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 4, 4, 4);
            for (int t = 0; t < tagsList.size() && t < 3; t++) {
                StoreTag tag = tagsList.get(t);
                TextView mTagView = new TextView(context);
                mTagView.setTextColor(ContextCompat.getColor(context, R.color.white));
                mTagView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                mTagView.setCompoundDrawablePadding(16);
                mTagView.setLayoutParams(params);
                setBackground(mTagView, tag.getColorCode());

                if (tag.getTagType() != null)
                    switch (tag.getTagType()) {
                        case "icon":
                            mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                    ContextCompat.getDrawable(context, R.drawable.ic_wifi),
                                    null, null, null);
                            mTagView.setCompoundDrawablePadding(0);
                            break;
                        case "default":
                            TextViewHelper.setText(mTagView, tag.getTagName());
                            break;
                        case "holiday":
                            TextViewHelper.setText(mTagView, tag.getTagName());
                            if (tag.getIsOpen() != null) {
                                if (tag.getIsOpen().equals("1")) {
                                    mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                            ContextCompat.getDrawable(context, R.drawable.ic_tick_white),
                                            null, null, null);
                                } else {
                                    mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                            ContextCompat.getDrawable(context, R.drawable.ic_close_white),
                                            null, null, null);
                                }
                            }
                            break;
                        case "user":
                            TextViewHelper.setText(mTagView, tag.getTagName());
                            mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                    ContextCompat.getDrawable(context, R.drawable.ic_user_white),
                                    null, null, null);
                            break;
                        default:
                            break;
                    }
                flTags.addView(mTagView);
            }
        } else ic_taq.setVisibility(View.INVISIBLE);

        if (item.getStoreCuisines() != null && item.getStoreCuisines().size() > 0
                && item.getStoreCuisines().get(0) != null) {
            TextViewHelper.setText(textValueCuisine, item.getStoreCuisines().get(0).getName());
        }
        return convertView;
    }

    private void setBackground(TextView textView, String color) {
        Drawable tempDrawable = ContextCompat.getDrawable(context, R.drawable.drawable_capsule);
        LayerDrawable bubble = (LayerDrawable) tempDrawable; //(cast to root element in xml)
        GradientDrawable solidColor = (GradientDrawable) bubble.findDrawableByLayerId(R.id.outerRectangle);
        solidColor.setColor(getColor(color));
        textView.setBackground(tempDrawable);
    }

    private int getColor(String color) {
        try {
            return Color.parseColor(color);
        } catch (Exception ex) {
            Log.e("PARSE COLOR", "getColor: ");
            return ContextCompat.getColor(context, R.color.colorOrange);
        }
    }
}
