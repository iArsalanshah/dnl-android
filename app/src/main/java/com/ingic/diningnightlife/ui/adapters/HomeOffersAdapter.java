package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.FeaturedStore;
import com.ingic.diningnightlife.entities.StoreTag;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.interfaces.OnOffersClick;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.CustomRatingBar;
import com.ingic.diningnightlife.ui.views.FlowLayout;
import com.ingic.diningnightlife.ui.views.Util;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by developer on 7/28/17.
 */

public class HomeOffersAdapter extends RecyclerViewListAdapter<FeaturedStore> {
    private Context context;
    private OnOffersClick onOffersClick;

    public HomeOffersAdapter(Context context, OnViewHolderClick listener, OnOffersClick onOffersClick) {
        super(context, listener);
        this.context = context;
        this.onOffersClick = onOffersClick;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_offers_list, viewGroup, false);
    }

    @Override
    protected void bindView(final FeaturedStore item, final RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            ImageView imgResource = (ImageView) viewHolder.getView(R.id.img_itemOffersList);
            ImageView imgNew = (ImageView) viewHolder.getView(R.id.img_itemOffers_new);
            ImageView ic_taq = (ImageView) viewHolder.getView(R.id.ic_taq);
            AnyTextView textTitle = (AnyTextView) viewHolder.getView(R.id.tv_offersList_title);
            AnyTextView textValueCuisine = (AnyTextView) viewHolder.getView(R.id.tv_offersList_cuisine);
            AnyTextView textLabelCuisine = (AnyTextView) viewHolder.getView(R.id.tv_offersList_labelCuisine);
            AnyTextView textAddress = (AnyTextView) viewHolder.getView(R.id.tv_offersList_address);
            CustomRatingBar ratingBar = (CustomRatingBar) viewHolder.getView(R.id.rbRating);
            FlowLayout flTags = (FlowLayout) viewHolder.getView(R.id.fl_offersList_tags);
            flTags.removeAllViews();

            ImageLoaderHelper.loadImage(item.getProfileImage(), imgResource);
            TextViewHelper.setText(textTitle, item.getFullName());
            TextViewHelper.setText(textAddress, item.getLocation());
            /*todo update it */
//            ratingBar.setScore(0);
            ratingBar.setScore(item.getTotalRating());

            Date currentTime = Calendar.getInstance().getTime();
            int daysBetween;
            if (item.getCreatedAt() != null) {
                daysBetween = Days.daysBetween(new DateTime(Util.getFormatedDate(item.getCreatedAt())), new DateTime(currentTime)).getDays();

//            if (item.getCreatedAt() != null && Util.Daybetween(String.valueOf(currentTime), item.getCreatedAt(), pattern) < 7) {
                if (daysBetween < AppConstants.NEW_TAG_DAYS_COUNT) {
                    imgNew.setVisibility(View.VISIBLE);
                } else imgNew.setVisibility(View.GONE);
            } else imgNew.setVisibility(View.GONE);

            viewHolder.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onOffersClick != null) {
                        onOffersClick.onOfferItemClicked(viewHolder.getAdapterPosition(), item.getId());
                    }
                }
            });

            List<StoreTag> tagsList = item.getStoreTags();
            //Tags
            if (tagsList != null && tagsList.size() > 0) {
                FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(
                        FlowLayout.LayoutParams.WRAP_CONTENT,
                        FlowLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(10, 4, 4, 4);
                for (int t = 0; t < tagsList.size() && t < 3; t++) {
                    StoreTag tag = tagsList.get(t);
                    TextView mTagView = new TextView(context);
                    mTagView.setTextColor(ContextCompat.getColor(context, R.color.white));
                    mTagView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
                    mTagView.setCompoundDrawablePadding(16);
                    mTagView.setLayoutParams(params);
                    setBackground(mTagView, tag.getColorCode());

                    if (tag.getTagType() != null)
                        switch (tag.getTagType()) {
                            case "icon":
                                mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                        ContextCompat.getDrawable(context, R.drawable.ic_wifi),
                                        null, null, null);
                                mTagView.setCompoundDrawablePadding(0);
                                break;
                            case "default":
                                TextViewHelper.setText(mTagView, tag.getTagName());
                                break;
                            case "holiday":
                                TextViewHelper.setText(mTagView, tag.getTagName());
                                if (tag.getIsOpen() != null) {
                                    if (tag.getIsOpen().equals("1")) {
                                        mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                                ContextCompat.getDrawable(context, R.drawable.ic_tick_white),
                                                null, null, null);
                                    } else {
                                        mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                                ContextCompat.getDrawable(context, R.drawable.ic_close_white),
                                                null, null, null);
                                    }
                                }
                                break;
                            case "user":
                                TextViewHelper.setText(mTagView, tag.getTagName());
                                mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                        ContextCompat.getDrawable(context, R.drawable.ic_user_white),
                                        null, null, null);
                                break;
                            default:
                                break;
                        }
                    flTags.addView(mTagView);
                }
            } else ic_taq.setVisibility(View.INVISIBLE);

            if (item.getStoreCuisines() != null && item.getStoreCuisines().size() > 0
                    && item.getStoreCuisines().get(0) != null) {
                TextViewHelper.setText(textValueCuisine, item.getStoreCuisines().get(0).getName());
            }
        }
    }

    private void setBackground(TextView textView, String color) {
        Drawable tempDrawable = ContextCompat.getDrawable(context, R.drawable.drawable_capsule);
        LayerDrawable bubble = (LayerDrawable) tempDrawable; //(cast to root element in xml)
        GradientDrawable solidColor = (GradientDrawable) bubble.findDrawableByLayerId(R.id.outerRectangle);
        solidColor.setColor(getColor(color));
        textView.setBackground(tempDrawable);
    }

    private int getColor(String color) {
        try {
            return Color.parseColor(color);
        } catch (Exception ex) {
            Log.e("PARSE COLOR", "getColor: ");
            return ContextCompat.getColor(context, R.color.colorOrange);
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }

}
