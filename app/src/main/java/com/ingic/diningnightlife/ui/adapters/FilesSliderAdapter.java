package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.StoreMenu;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by developer on 11/14/17.
 */

public class FilesSliderAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<StoreMenu> imageFiles;

    public FilesSliderAdapter(Context context, List<StoreMenu> list) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        if (list != null)
            this.imageFiles = list;
        else this.imageFiles = new ArrayList<>();
    }

    public void addAll(List<StoreMenu> list) {
        if (list == null) return;
        this.imageFiles = list;
        notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        if (layoutInflater == null)
            layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View itemViewPager = layoutInflater.inflate(R.layout.item_image_slider_files, container, false);

        ImageView imageView = itemViewPager.findViewById(R.id.img_item_slider);
        ImageLoaderHelper.loadImage(imageFiles.get(position).getMenuImage(), imageView);

        container.addView(itemViewPager);

        return itemViewPager;
    }

    @Override
    public int getCount() {
        return imageFiles.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }
}