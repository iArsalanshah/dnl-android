package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.ParentEntity;
import com.ingic.diningnightlife.entities.SharedVoucher;
import com.ingic.diningnightlife.interfaces.ChildItemClickListener;
import com.ingic.diningnightlife.ui.adapters.viewholders.ChildVH;
import com.ingic.diningnightlife.ui.adapters.viewholders.ParentVH;

import java.util.List;

public class MyVouchersAdapter extends ExpandableRecyclerAdapter<ParentEntity, SharedVoucher, ParentVH, ChildVH> {
    private final boolean isReceived;

    private LayoutInflater mInflater;
    private List<ParentEntity> parentEntityList;
    private ChildItemClickListener childItemClickListener;

    public MyVouchersAdapter(Context context, @NonNull List<ParentEntity> parentList,
                             boolean isReceived, ChildItemClickListener childItemClickListener) {
        super(parentList);
        parentEntityList = parentList;
        mInflater = LayoutInflater.from(context);
        this.isReceived = isReceived;
        this.childItemClickListener = childItemClickListener;
    }

    @NonNull
    @Override
    public ParentVH onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View recipeView = mInflater.inflate(R.layout.item_myvouchers_list_parent, parentViewGroup, false);
        return new ParentVH(recipeView);
    }

    @NonNull
    @Override
    public ChildVH onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View layoutText = mInflater.inflate(R.layout.item_myvouchers_list_child, childViewGroup, false);
        return new ChildVH(layoutText);
    }

    @Override
    public void onBindParentViewHolder(@NonNull ParentVH parentViewHolder, int parentPosition, @NonNull ParentEntity parent) {
        parentViewHolder.bind(parent);
    }

    @Override
    public void onBindChildViewHolder(@NonNull ChildVH childViewHolder, int parentPosition, int childPosition, @NonNull SharedVoucher child) {
        childViewHolder.bind(child, isReceived, childItemClickListener);
    }
}
