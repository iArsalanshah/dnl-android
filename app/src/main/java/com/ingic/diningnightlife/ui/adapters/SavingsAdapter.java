package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.SavingResult;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.helpers.DateHelper;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.Util;

import java.util.Locale;
import java.util.Random;

/**
 * Created by developer on 8/3/17.
 */

public class SavingsAdapter extends RecyclerViewListAdapter<SavingResult> {
    private Context context;

    public SavingsAdapter(Context context, OnViewHolderClick listener) {
        super(context, listener);
        this.context = context;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_savings, viewGroup, false);
    }

    @Override
    protected void bindView(SavingResult item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            String date = DateHelper.dateFormatGMT(item.getUpdatedAt(), "dd-MM-yy", AppConstants.DATE_FORMAT_DATE);
            String time = DateHelper.dateFormatGMT(item.getUpdatedAt(), "HH:mm", AppConstants.DATE_FORMAT_TIMESTAMP);
//            String time = DateHelper.dateFormatGMT(item.getUpdatedAt(), "HH:MM", AppConstants.DATE_FORMAT_DATE);

            int position = viewHolder.getAdapterPosition();
            ImageView imgIcon = (ImageView) viewHolder.getView(R.id.img_itemSavings);
            AnyTextView tvTitle = (AnyTextView) viewHolder.getView(R.id.tv_itemSavings_title);
            AnyTextView tvSaveAmount = (AnyTextView) viewHolder.getView(R.id.tv_itemSavings_saveAmount);
            AnyTextView tvCapsule = (AnyTextView) viewHolder.getView(R.id.tv_itemSavings_capsule);
            AnyTextView tvDate = (AnyTextView) viewHolder.getView(R.id.tv_itemSavings_date);
            AnyTextView tvTime = (AnyTextView) viewHolder.getView(R.id.tv_itemSavings_time);

            if (item.getStoreDetail() != null) {
                ImageLoaderHelper.loadImage(item.getStoreDetail().getProfileImage(), imgIcon);
                tvTitle.setText(item.getStoreDetail().getFullName());
            }
//            imgIcon.setImageResource(item.getStoreDetail().getProfileImage()));
            tvSaveAmount.setText(String.format(Locale.US, "Saved : AED %.2f",
                    item.getDiscountAmount() == null ? 0 : Util.getParsedFloat(item.getDiscountAmount())));
            tvDate.setText(date);
            tvTime.setText(time);

            //capsule
            if (item.getVoucherDetail() != null) {
                if (item.getVoucherDetail().getVoucher() != null && !item.getVoucherDetail().getVoucher().isEmpty()) {
                    tvCapsule.setText(item.getVoucherDetail().getVoucher());
                }
                if (item.getVoucherDetail().getColorCode() != null && !item.getVoucherDetail().getColorCode().isEmpty()) {
                    setBackground(tvCapsule, item.getVoucherDetail().getColorCode());
                }
            } else
                tvCapsule.setText("-");
        }
    }

    private void setBackground(AnyTextView textView, String myColor) {
        Drawable tempDrawable = ContextCompat.getDrawable(context, R.drawable.drawable_capsule);
        LayerDrawable bubble = (LayerDrawable) tempDrawable; //(cast to root element in xml)
        GradientDrawable solidColor = (GradientDrawable) bubble.findDrawableByLayerId(R.id.outerRectangle);
        solidColor.setColor(getColor(myColor));
        textView.setBackground(tempDrawable);
    }

    private int getColor(String color) {
        try {
            return Color.parseColor(color);
        } catch (Exception ex) {
            Log.e("PARSE COLOR", "getColor: ");
            return ContextCompat.getColor(context, R.color.colorOrange);
        }
    }

    private int getRandomBackgroundColor() {
        Random ran = new Random();
        int x = ran.nextInt(6) + 1; //range 1 to 7
        switch (x) {
            case 1:
                x = ContextCompat.getColor(context, R.color.colorBlueGrid);
                break;
            case 2:
                x = ContextCompat.getColor(context, R.color.colorOrangeDarkGrid);
                break;
            case 3:
                x = ContextCompat.getColor(context, R.color.colorPurpleGrid);
                break;
            case 4:
                x = ContextCompat.getColor(context, R.color.colorPinkGrid);
                break;
            case 5:
                x = ContextCompat.getColor(context, R.color.colorGreenGrid);
                break;
            case 6:
                x = ContextCompat.getColor(context, R.color.colorOrangeLightGrid);
                break;
            default://case 7 or default color
                x = ContextCompat.getColor(context, R.color.colorBlueGrid);
                break;
        }
        return x;
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
