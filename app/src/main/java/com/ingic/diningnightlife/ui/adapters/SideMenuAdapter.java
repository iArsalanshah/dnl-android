package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;

/**
 * Created by developer on 7/28/17.
 */

public class SideMenuAdapter extends RecyclerViewListAdapter<String> {
    public SideMenuAdapter(Context context, OnViewHolderClick listener) {
        super(context, listener);
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_sidemenu, viewGroup, false);
    }

    @Override
    protected void bindView(String item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            AnyTextView itemSideMenu = (AnyTextView) viewHolder.getView(R.id.tv_itemSideMenu);
            itemSideMenu.setText(item);
        }
    }
}
