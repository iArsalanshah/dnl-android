package com.ingic.diningnightlife.ui.adapters.viewholders;


import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.SharedVoucher;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.helpers.DateHelper;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.interfaces.ChildItemClickListener;


public class ChildVH extends ChildViewHolder {
    private ImageView imgItemChildMyVouchers;
    private TextView tvItemChildMyVouchersTitleRestaurant;
    private TextView tvItemChildMyVouchersTitle;
    private TextView tvItemChildMyVouchersSentOrRecivedBy;
    private TextView tvItemChildMyVouchersValidity;
    private View itemView;

    public ChildVH(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
        imgItemChildMyVouchers = itemView.findViewById(R.id.img_item_child_myVouchers);
        tvItemChildMyVouchersTitleRestaurant = itemView.findViewById(R.id.tv_item_child_myVouchers_title_restaurant);
        tvItemChildMyVouchersTitle = itemView.findViewById(R.id.tv_item_child_myVouchers_title);
        tvItemChildMyVouchersSentOrRecivedBy = itemView.findViewById(R.id.tv_item_child_myVouchers_sentOrReceivedBy);
        tvItemChildMyVouchersValidity = itemView.findViewById(R.id.tv_item_child_myVouchers_validity);
    }

    public void bind(final SharedVoucher entity, boolean isReceived, final ChildItemClickListener childItemClickListener) {
        if (isReceived)
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    childItemClickListener.onClickListener(entity);
                }
            });
        ImageLoaderHelper.loadImage(entity.getProfileImage(), imgItemChildMyVouchers);
        TextViewHelper.setText(tvItemChildMyVouchersTitleRestaurant, entity.getFullName());
        TextViewHelper.setText(tvItemChildMyVouchersTitle, String.format("%s %s", entity.getVoucher(), entity.getTitle()));
        tvItemChildMyVouchersTitle.setTextColor(getColor(entity.getColorCode()));
        if (isReceived)
            TextViewHelper.setText(tvItemChildMyVouchersSentOrRecivedBy, String.format("Received from: %s", entity.getUserName()));
        else
            TextViewHelper.setText(tvItemChildMyVouchersSentOrRecivedBy, String.format("Send to: %s", entity.getUserName()));
        TextViewHelper.setText(tvItemChildMyVouchersValidity, String.format("Valid for up to %s people until %s", entity.getValidUpto(),
                DateHelper.dateFormatGMT(entity.getValidDate(), "MMM dd yyyy", AppConstants.DATE_FORMAT_DATE)));
    }

    private int getColor(String color) {
        try {
            return Color.parseColor(color);
        } catch (Exception ex) {
            return Color.parseColor("#E76A30");
        }
    }
}
