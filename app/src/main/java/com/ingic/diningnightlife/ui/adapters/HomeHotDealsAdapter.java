package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.FeaturedStore;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.interfaces.OnHotDealsClicked;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;

import java.util.Random;

/**
 * Created by developer on 7/28/17.
 */

public class HomeHotDealsAdapter extends RecyclerViewListAdapter<FeaturedStore> {
    private Context context;
    private OnHotDealsClicked onHotDealsClicked;

    public HomeHotDealsAdapter(Context context, OnViewHolderClick listener, OnHotDealsClicked onHotDealsClicked) {
        super(context, listener);
        this.context = context;
        this.onHotDealsClicked = onHotDealsClicked;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_hot_deals, viewGroup, false);
    }

    @Override
    protected void bindView(final FeaturedStore item, final RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            ImageView imgIcon = (ImageView) viewHolder.getView(R.id.img_itemHotDeals);
            AnyTextView tvLabel = (AnyTextView) viewHolder.getView(R.id.tv_itemHotDeals_label);//label HOT DEAL
            AnyTextView tvTitle = (AnyTextView) viewHolder.getView(R.id.tv_itemHotDeals_title);
//            AnyTextView tvDesc = (AnyTextView) viewHolder.getView(R.id.tv_itemHotDeals_desc);

            viewHolder.getView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onHotDealsClicked != null)
                        onHotDealsClicked.onHotDealsItemClicked(viewHolder.getAdapterPosition(), item.getId());
                }
            });

            ImageLoaderHelper.loadImage(item.getProfileImage(), imgIcon);
            tvTitle.setText(item.getFullName());
            tvLabel.setBackgroundColor(getRandomBackgroundColor()); //adding random bg color
        }
    }

    private int getRandomBackgroundColor() {
        Random ran = new Random();
        int x = ran.nextInt(6) + 1; //range 1 to 7
        switch (x) {
            case 1:
                x = ContextCompat.getColor(context, R.color.colorBlueGrid);
                break;
            case 2:
                x = ContextCompat.getColor(context, R.color.colorOrangeDarkGrid);
                break;
            case 3:
                x = ContextCompat.getColor(context, R.color.colorPurpleGrid);
                break;
            case 4:
                x = ContextCompat.getColor(context, R.color.colorPinkGrid);
                break;
            case 5:
                x = ContextCompat.getColor(context, R.color.colorGreenGrid);
                break;
            case 6:
                x = ContextCompat.getColor(context, R.color.colorOrangeLightGrid);
                break;
            default://case 7 or default black color
                x = ContextCompat.getColor(context, R.color.black);
                break;
        }
        return x;
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
