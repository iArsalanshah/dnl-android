package com.ingic.diningnightlife.ui.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.NotificationsResult;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;

public class NotificationsAdapter extends RecyclerViewListAdapter<NotificationsResult> {
    private Context context;

    public NotificationsAdapter(Context context, OnViewHolderClick listener) {
        super(context, listener);
        this.context = context;
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.item_notifications, viewGroup, false);
    }

    @Override
    protected void bindView(NotificationsResult item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            int position = viewHolder.getAdapterPosition();
//            ImageView imgIcon = (ImageView) viewHolder.getView(R.id.img_itemNotification);
            AnyTextView tvNotifications = (AnyTextView) viewHolder.getView(R.id.tv_itemNotification);
            LinearLayout viewForground = (LinearLayout) viewHolder.getView(R.id.view_foreground);

            if (position % 2 != 0) {
                viewForground.setBackgroundColor(ContextCompat.getColor(context, R.color.colorNotificationBg));
            } else
                viewForground.setBackgroundColor(ContextCompat.getColor(context, R.color.defaultBackgroundColor));
            tvNotifications.setText(item.getMessage());
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return position;
    }
}
