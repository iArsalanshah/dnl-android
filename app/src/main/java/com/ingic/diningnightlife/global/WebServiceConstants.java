package com.ingic.diningnightlife.global;

public class WebServiceConstants {
    public static final String SERVICE_BASE_URL = "http://diningandnightlife.stagingic.com/api/";  //// TODO: 9/26/17 Live Linux DB
//    public static final String SERVICE_BASE_URL = "http://10.1.18.153/dinningandnightlife/api/";  // TODO: 9/26/17 local DB

    public static final String FACEBOOK = "facebook";
    public static final String Google = "google";
    public static final String DEVICE = "android";
    public static final String NULL_RESPONSE = "Null response";
    public static final String SUCCESS = "2000";
    public static final String FAILURE = "1000";
    public static final String PAYFORT_TEST_BASE_URL = "https://sbcheckout.PayFort.com/FortAPI/"; //Test Environment URL
//    public static final String PAYFORT_TEST_BASE_URL = "https://sbpaymentservices.payfort.com/"; //Test Environment URL
//    public static final String PAYFORT_LIVE_BASE_URL = "https://checkout.PayFort.com/FortAPI/"; //Live Environment URL
}