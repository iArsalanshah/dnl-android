package com.ingic.diningnightlife.global;

/**
 * Created by developer on 8/4/17.
 */

public class AppConstants {
    public static final String GRID_TAG_CLICKED = "gridTagClicked";
    public static final String GRID_TAG_LATEST_EDITION = "gridTagLatestEdition";
    public static final String REDEEM_VOUCHER_CLICKED = "redeemVoucherClicked";
    public static final String DATE_FORMAT_TIMESTAMP = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_TIME = "HH:mm:ss";
    public static final String DATE_FORMAT_DATE = "yyyy-MM-dd";

    public static final String BUNDLE_LOGIN_RESULT = "bundleLoginResult";
    public static final String BUNDLE_EMAIL_UPDATE_STATE = "bundleEmailUpdateState";
    public static final String BUNDLE_STORE_TYPE = "bundleStoreType";
    public static final String BUNDLE_STORE_ID = "bundleStoreId";
    public static final String BUNDLE_LAT = "bundleLat";
    public static final String BUNDLE_LNG = "bundleLng";
    public static final String BUNDLE_STORE_NAME = "bundleStoreName";
    public static final String BUNDLE_VOUCHER_NAME = "bundleVoucherName";
    public static final String BUNDLE_VOUCHER_STORE_TYPE = "bundleVoucherStoreType";
    public static final String BUNDLE_IS_ADVANCE_SEARCH = "bundleIsAdvanceSearch";
    public static final String BUNDLE_CITY_ID = "bundleCityId";
    public static final String BUNDLE_CUISINE = "bundleCuisine";
    public static final String BUNDLE_VENUE = "bundleVenueType";
    public static final String BUNDLE_DIETARY = "bundleDietary";
    public static final String BUNDLE_QUICK_SEARCH = "bundleQuickSearch";

    public static final String INTENT_VIDEO_LINK = "intentVideoLink";
    public static final String INTENT_FULLSCREEN_IMGURL = "intentImageUrl";

    public static final String HOME = "home";
    public static final String NOTIFCATIONS = "notification";
    public static final String SUBSCRIPTION = "subscription";
    public static final String REDEEM = "redeem";
    public static final String STORE_ID = "storeId";
    public static final String REDEEM_VOUCHER_LABEL = "redeemVoucherLabel";
    public static final int YES = 1;
    public static final int NO = 0;
    public static final String INTENT_CITY_ID = "city_id";
    public static final int NEW_TAG_DAYS_COUNT = 8;
    public static final String BUNDLE_CREATE_NEW_PIN = "createNewPin";
    public static final String BUNDLE_SEARCH_RESULT = "search_result";
    public static final String BUNDLE_RECEIVED_VOUCHERS = "bundle_received_vouchers";
    public static final String BUNDLE_SENT_VOUCHERS = "bundle_sent_vouchers";
    public static final String BUNDLE_FREE_VOUCHER_ID = "free_voucher_id";
    public static boolean FETCH_LOCATION = false;

    public static final String BUNDLE_STORE_VOUCHER_ID = "voucherId";

    public static final String SIDE_MENU_UPDATOR = "sideMenuUpdator";
    public static final String BUNDLE_QUICK_SEARCH_NOW_OPEN = "isNowOpen";

    public static final int DEFAULT_BANNER_REFRESH_RATE = 10000;

    public static final String INTENT_FULLSCREEN_SLIDER = "intent_full_screen_slider";
    public static final String INTENT_FULLSCREEN_SLIDER_POSITION = "intent_full_screen_slider_position";
    public static final String MENU_ADAPTER = "menu_adapter";
    public static final String FILE_ADAPTER = "file_adapter";

    public static final String BUNDLE_LOGIN_USER_TYPE = "loginUserType";
    public static final String BUNDLE_NEW_USER_SIGN_UP = "newUserSignUp";
    public static final String BUNDLE_EXITING_USER_LOGIN = "existing_user_login";
//    public static final String WEBSITE_LINK = "http://www.diningandnightlifeabudhabi.ae/";
    public static final String WEBSITE_LINK = "https://diningandnightlife.com/";
}
