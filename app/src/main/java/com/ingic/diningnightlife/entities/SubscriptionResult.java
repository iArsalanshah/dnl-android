package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionResult {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("days")
    @Expose
    private int days;
    @SerializedName("period")
    @Expose
    private String period;
    @SerializedName("discounted_percentage")
    @Expose
    private String discountedPercentage;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("is_subscribe")
    @Expose
    private int isSubcribe;
    private boolean check100PercentDiscounted;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getIsSubcribe() {
        return isSubcribe;
    }

    public void setIsSubcribe(int isSubcribe) {
        this.isSubcribe = isSubcribe;
    }

    public String getDiscountedPercentage() {
        return discountedPercentage;
    }

    public void setDiscountedPercentage(String discountedPercentage) {
        this.discountedPercentage = discountedPercentage;
    }

    public boolean isSubscribed() {
        return this.isSubcribe == 1;
    }

    public boolean isCheck100PercentDiscounted() {
        return check100PercentDiscounted;
    }

    public void setCheck100PercentDiscounted(boolean check100PercentDiscounted) {
        this.check100PercentDiscounted = check100PercentDiscounted;
    }
}