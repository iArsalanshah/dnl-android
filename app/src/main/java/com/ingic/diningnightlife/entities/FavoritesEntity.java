package com.ingic.diningnightlife.entities;

import java.util.ArrayList;

/**
 * Created by developer on 8/3/17.
 */

public class FavoritesEntity {
    private String imgResource;
    private String title;
    private String cuisine;
    private String address;
    private float rating;
    private boolean isFavorite;
    private ArrayList<CapsuleEntity> pillList;

    public FavoritesEntity(String imgResource, String title, String cuisine, String address, float rating, ArrayList<CapsuleEntity> pillList, boolean isFavorite) {
        this.imgResource = imgResource;
        this.title = title;
        this.cuisine = cuisine;
        this.address = address;
        this.rating = rating;
        this.pillList = pillList;
        this.isFavorite = isFavorite;
    }

    public String getImgResource() {
        return imgResource;
    }

    public String getTitle() {
        return title;
    }

    public String getCuisine() {
        return cuisine;
    }

    public String getAddress() {
        return address;
    }

    public float getRating() {
        return rating;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public ArrayList<CapsuleEntity> getPillList() {
        return pillList;
    }
}