package com.ingic.diningnightlife.entities;

/**
 * Created by developer on 7/28/17.
 */

public class HomeGridEntity {
    private int imgColor;
    private int imgResource;
    private String name;

    public HomeGridEntity(int imgColor, int imgResource, String name) {
        this.imgColor = imgColor;
        this.imgResource = imgResource;
        this.name = name;
    }

    public int getImgColor() {
        return imgColor;
    }

    public void setImgColor(int imgColor) {
        this.imgColor = imgColor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImgResource() {
        return imgResource;
    }

    public void setImgResource(int imgResource) {
        this.imgResource = imgResource;
    }
}
