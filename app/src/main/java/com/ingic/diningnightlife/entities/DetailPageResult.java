package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailPageResult {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("role_id")
    @Expose
    private int roleId;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("is_featured")
    @Expose
    private int isFeatured;
    @SerializedName("is_favourite")
    @Expose
    private int isFavourite;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("city_id")
    @Expose
    private int cityId;
    @SerializedName("verification_code")
    @Expose
    private String verificationCode;
    @SerializedName("socialmedia_type")
    @Expose
    private String socialmediaType;
    @SerializedName("socialmedia_id")
    @Expose
    private String socialmediaId;
    @SerializedName("notification_status")
    @Expose
    private String notificationStatus;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("device_token")
    @Expose
    private String deviceToken;
    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("email_update")
    @Expose
    private int emailUpdate;
    @SerializedName("is_show_review_button")
    @Expose
    private int is_show_review_button;
    @SerializedName("is_delete")
    @Expose
    private String isDelete;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("deleted_at")
    @Expose
    private String deletedAt;
    @SerializedName("last_login")
    @Expose
    private String lastLogin;
    @SerializedName("quality_rating")
    @Expose
    private float qualityRating;
    @SerializedName("service_rating")
    @Expose
    private float serviceRating;
    @SerializedName("ambiance_rating")
    @Expose
    private float ambianceRating;
    @SerializedName("price_rating")
    @Expose
    private float priceRating;
    @SerializedName("overall_rating")
    @Expose
    private float overallRating;
    @SerializedName("total_rating")
    @Expose
    private float totalRating;
    @SerializedName("store_vouchers")
    @Expose
    private List<StoreVoucher> storeVouchers = null;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("store_tags")
    @Expose
    private List<StoreTag> storeTags = null;
    @SerializedName("store_menus")
    @Expose
    private List<StoreMenu> storeMenus = null;
    @SerializedName("store_photos")
    @Expose
    private List<StorePhoto> storePhotos = null;
    @SerializedName("store_hours")
    @Expose
    private List<StoreHour> storeHours = null;
    @SerializedName("store_banners")
    @Expose
    private List<StoreBanners> storeBanners = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public int getIsFeatured() {
        return isFeatured;
    }

    public void setIsFeatured(int isFeatured) {
        this.isFeatured = isFeatured;
    }

    public int getIsFavourite() {
        return isFavourite;
    }

    public void setIsFavourite(int isFavourite) {
        this.isFavourite = isFavourite;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getSocialmediaType() {
        return socialmediaType;
    }

    public void setSocialmediaType(String socialmediaType) {
        this.socialmediaType = socialmediaType;
    }

    public String getSocialmediaId() {
        return socialmediaId;
    }

    public void setSocialmediaId(String socialmediaId) {
        this.socialmediaId = socialmediaId;
    }

    public String getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(String notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public int getEmailUpdate() {
        return emailUpdate;
    }

    public void setEmailUpdate(int emailUpdate) {
        this.emailUpdate = emailUpdate;
    }

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public float getQualityRating() {
        return qualityRating;
    }

    public void setQualityRating(float qualityRating) {
        this.qualityRating = qualityRating;
    }

    public float getServiceRating() {
        return serviceRating;
    }

    public void setServiceRating(float serviceRating) {
        this.serviceRating = serviceRating;
    }

    public float getAmbianceRating() {
        return ambianceRating;
    }

    public void setAmbianceRating(float ambianceRating) {
        this.ambianceRating = ambianceRating;
    }

    public float getPriceRating() {
        return priceRating;
    }

    public void setPriceRating(float priceRating) {
        this.priceRating = priceRating;
    }

    public float getOverallRating() {
        return overallRating;
    }

    public void setOverallRating(float overallRating) {
        this.overallRating = overallRating;
    }

    public float getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(float totalRating) {
        this.totalRating = totalRating;
    }

    public List<StoreVoucher> getStoreVouchers() {
        return storeVouchers;
    }

    public void setStoreVouchers(List<StoreVoucher> storeVouchers) {
        this.storeVouchers = storeVouchers;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public List<StoreTag> getStoreTags() {
        return storeTags;
    }

    public void setStoreTags(List<StoreTag> storeTags) {
        this.storeTags = storeTags;
    }

    public List<StoreMenu> getStoreMenus() {
        return storeMenus;
    }

    public void setStoreMenus(List<StoreMenu> storeMenus) {
        this.storeMenus = storeMenus;
    }

    public List<StorePhoto> getStorePhotos() {
        return storePhotos;
    }

    public void setStorePhotos(List<StorePhoto> storePhotos) {
        this.storePhotos = storePhotos;
    }

    public List<StoreHour> getStoreHours() {
        return storeHours;
    }

    public void setStoreHours(List<StoreHour> storeHours) {
        this.storeHours = storeHours;
    }

    public List<StoreBanners> getStoreBanners() {
        return storeBanners;
    }

    public void setStoreBanners(List<StoreBanners> storeBanners) {
        this.storeBanners = storeBanners;
    }

    public int getIs_show_review_button() {
        return is_show_review_button;
    }
}
