package com.ingic.diningnightlife.entities;

/**
 * Created by developer on 8/7/17.
 */

public class DetailsPageRedeemVoucherEntity {
    private CapsuleEntity capsuleEntity;
    private String title;
    private String desc;

    public DetailsPageRedeemVoucherEntity(CapsuleEntity capsuleEntity, String title, String desc) {
        this.capsuleEntity = capsuleEntity;
        this.title = title;
        this.desc = desc;
    }

    public CapsuleEntity getCapsuleEntity() {
        return capsuleEntity;
    }

    public String getTitle() {
        return title;
    }

    public String getDesc() {
        return desc;
    }
}
