package com.ingic.diningnightlife.entities;

/**
 * Created by developer on 8/1/17.
 */

public class CapsuleEntity {
    private String name;
    private int imgResource;
    private Integer leftDrawable;

    public CapsuleEntity(String name) {
        this.name = name;
    }

    public CapsuleEntity(String name, int imgResource) {
        this.name = name;
        this.imgResource = imgResource;
    }

    public CapsuleEntity(String name, int imgResource, Integer leftDrawable) {
        this.name = name;
        this.imgResource = imgResource;
        this.leftDrawable = leftDrawable;
    }

    public Integer getLeftDrawable() {
        return leftDrawable;
    }

    public String getName() {
        return name;
    }

    public int getImgResource() {
        return imgResource;
    }
}
