package com.ingic.diningnightlife.entities;

/**
 * Created by developer on 8/3/17.
 */

public class SavingsEntity {
    private String imgResource;
    private String title;
    private String amountSaved;
    private CapsuleEntity mCapsule;
    private String date;
    private String time;

    public SavingsEntity(String imgResource, String title, String amountSaved, CapsuleEntity mCapsule, String date, String time) {
        this.imgResource = imgResource;
        this.title = title;
        this.amountSaved = amountSaved;
        this.mCapsule = mCapsule;
        this.date = date;
        this.time = time;
    }

    public String getImgResource() {
        return imgResource;
    }

    public String getTitle() {
        return title;
    }

    public String getAmountSaved() {
        return amountSaved;
    }

    public CapsuleEntity getmCapsule() {
        return mCapsule;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }
}
