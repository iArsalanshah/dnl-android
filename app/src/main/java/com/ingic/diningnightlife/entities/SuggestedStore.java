package com.ingic.diningnightlife.entities;

/**
 * Created by developer on 8/29/17.
 */

public class SuggestedStore {
    private int id;
    private String profile_image;

    public int getId() {
        return id;
    }

    public String getProfile_image() {
        return profile_image;
    }
}
