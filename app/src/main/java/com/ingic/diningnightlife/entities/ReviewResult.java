package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class  ReviewResult {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("replier_id")
    @Expose
    private int replier_id;
    @SerializedName("store_id")
    @Expose
    private int storeId;
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("quality")
    @Expose
    private int quality;
    @SerializedName("service")
    @Expose
    private int service;
    @SerializedName("ambiance")
    @Expose
    private int ambiance;
    @SerializedName("price")
    @Expose
    private int price;
    @SerializedName("overall_experience")
    @Expose
    private int overallExperience;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("like_count")
    @Expose
    private int likeCount;
    @SerializedName("share_count")
    @Expose
    private int shareCount;
    @SerializedName("dislike_count")
    @Expose
    private int dislikeCount;
    @SerializedName("is_like")
    @Expose
    private int isLike;
    @SerializedName("is_dislike")
    @Expose
    private int isDislike;
    @SerializedName("profile_picture")
    @Expose
    private String profilePicture;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("likeClick")
    @Expose
    private boolean likeClick;
    @SerializedName("dislikeClick")
    @Expose
    private boolean dislikeClick;
    @SerializedName("review_images")
    @Expose
    private List<ReviewImage> reviewImages = null;
    @SerializedName("replys")
    @Expose
    private List<ReviewResult> replys = null;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public boolean isSuperAdmin() {
        return replier_id == 1;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public int getService() {
        return service;
    }

    public void setService(int service) {
        this.service = service;
    }

    public int getAmbiance() {
        return ambiance;
    }

    public void setAmbiance(int ambiance) {
        this.ambiance = ambiance;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getOverallExperience() {
        return overallExperience;
    }

    public void setOverallExperience(int overallExperience) {
        this.overallExperience = overallExperience;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public int getDislikeCount() {
        return dislikeCount;
    }

    public void setDislikeCount(int dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    public int getIsLike() {
        return isLike;
    }

    public void setIsLike(int isLike) {
        this.isLike = isLike;
    }

    public int getIsDislike() {
        return isDislike;
    }

    public void setIsDislike(int isDislike) {
        this.isDislike = isDislike;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public List<ReviewImage> getReviewImages() {
        return reviewImages;
    }

    public void setReviewImages(List<ReviewImage> reviewImages) {
        this.reviewImages = reviewImages;
    }

    public boolean isLikeClick() {
        return likeClick;
    }

    public void setLikeClick(boolean likeClick) {
        this.likeClick = likeClick;
    }

    public boolean isDislikeClick() {
        return dislikeClick;
    }

    public void setDislikeClick(boolean dislikeClick) {
        this.dislikeClick = dislikeClick;
    }

    public int getShareCount() {
        return shareCount;
    }

    public List<ReviewResult> getReplys() {
        return replys;
    }

    public void setReplys(List<ReviewResult> replys) {
        this.replys = replys;
    }
}
