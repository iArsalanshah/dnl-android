package com.ingic.diningnightlife.entities;

import java.util.ArrayList;

/**
 * Created by developer on 7/28/17.
 */

public class HomeOfferEntity {
    private String imgResource;
    private String title;
    private String cuisine;
    private String address;
    private float rating;
    private ArrayList<CapsuleEntity> pillList;

    public HomeOfferEntity(String imgResource, String title, String cuisine, String address, float rating, ArrayList<CapsuleEntity> pillList) {
        this.imgResource = imgResource;
        this.title = title;
        this.cuisine = cuisine;
        this.address = address;
        this.rating = rating;
        this.pillList = pillList;
    }

    public String getImgResource() {
        return imgResource;
    }

    public String getTitle() {
        return title;
    }

    public String getCuisine() {
        return cuisine;
    }

    public String getAddress() {
        return address;
    }

    public float getRating() {
        return rating;
    }

    public ArrayList<CapsuleEntity> getPillList() {
        return pillList;
    }
}
