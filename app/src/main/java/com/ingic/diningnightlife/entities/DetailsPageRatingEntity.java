package com.ingic.diningnightlife.entities;

/**
 * Created by developer on 8/7/17.
 */

public class DetailsPageRatingEntity {
    private String title;
    private float rating;

    public DetailsPageRatingEntity(String title, float rating) {
        this.title = title;
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public float getRating() {
        return rating;
    }
}
