
package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Sent {

    @SerializedName("dinning")
    @Expose
    private List<SharedVoucher> dinning = null;
    @SerializedName("brunch")
    @Expose
    private List<SharedVoucher> brunch = null;
    @SerializedName("nightlife")
    @Expose
    private List<SharedVoucher> nightlife = null;
    @SerializedName("exclusive")
    @Expose
    private List<SharedVoucher> exclusive = null;
    @SerializedName("last_minute")
    @Expose
    private List<SharedVoucher> lastMinute = null;

    public List<SharedVoucher> getDinning() {
        return dinning;
    }

    public void setDinning(List<SharedVoucher> dinning) {
        this.dinning = dinning;
    }

    public List<SharedVoucher> getBrunch() {
        return brunch;
    }

    public void setBrunch(List<SharedVoucher> brunch) {
        this.brunch = brunch;
    }

    public List<SharedVoucher> getNightlife() {
        return nightlife;
    }

    public void setNightlife(List<SharedVoucher> nightlife) {
        this.nightlife = nightlife;
    }

    public List<SharedVoucher> getExclusive() {
        return exclusive;
    }

    public void setExclusive(List<SharedVoucher> exclusive) {
        this.exclusive = exclusive;
    }

    public List<SharedVoucher> getLastMinute() {
        return lastMinute;
    }

    public void setLastMinute(List<SharedVoucher> lastMinute) {
        this.lastMinute = lastMinute;
    }

}
