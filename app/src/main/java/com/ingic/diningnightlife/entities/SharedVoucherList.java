
package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SharedVoucherList {

    @SerializedName("sent")
    @Expose
    private Sent sent;
    @SerializedName("received")
    @Expose
    private Received received;

    public Sent getSent() {
        return sent;
    }

    public void setSent(Sent sent) {
        this.sent = sent;
    }

    public Received getReceived() {
        return received;
    }

    public void setReceived(Received received) {
        this.received = received;
    }

}
