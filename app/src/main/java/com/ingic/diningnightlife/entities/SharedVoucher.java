
package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SharedVoucher {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("free_voucher_id")
    @Expose
    private int free_voucher_id;
    @SerializedName("store_id")
    @Expose
    private int storeId;
    @SerializedName("store_type")
    @Expose
    private String storeType;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("voucher")
    @Expose
    private String voucher;
    @SerializedName("voucher_percentage")
    @Expose
    private String voucherPercentage;
    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("valid_upto")
    @Expose
    private String validUpto;
    @SerializedName("valid_from")
    @Expose
    private String validFrom;
    @SerializedName("valid_to")
    @Expose
    private String validTo;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("period")
    @Expose
    private Object period;
    @SerializedName("unit")
    @Expose
    private Object unit;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("is_delete")
    @Expose
    private int isDelete;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("valid_date")
    @Expose
    private String validDate;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getStoreType() {
        return storeType;
    }

    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public String getVoucherPercentage() {
        return voucherPercentage;
    }

    public void setVoucherPercentage(String voucherPercentage) {
        this.voucherPercentage = voucherPercentage;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getValidUpto() {
        return validUpto;
    }

    public void setValidUpto(String validUpto) {
        this.validUpto = validUpto;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getPeriod() {
        return period;
    }

    public void setPeriod(Object period) {
        this.period = period;
    }

    public Object getUnit() {
        return unit;
    }

    public void setUnit(Object unit) {
        this.unit = unit;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getValidDate() {
        return validDate;
    }

    public void setValidDate(String validDate) {
        this.validDate = validDate;
    }

    public String getUserName() {
        return userName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public int getFree_voucher_id() {
        return free_voucher_id;
    }

    public String getFullName() {
        return fullName;
    }
}
