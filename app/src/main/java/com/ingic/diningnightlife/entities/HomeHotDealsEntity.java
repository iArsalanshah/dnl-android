package com.ingic.diningnightlife.entities;

/**
 * Created by developer on 7/28/17.
 */

public class HomeHotDealsEntity {
    private int imgUrl;
    private String dealName;
    private String description;

    public HomeHotDealsEntity(int imgUrl, String dealName, String description) {
        this.imgUrl = imgUrl;
        this.dealName = dealName;
        this.description = description;
    }

    public int getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(int imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
