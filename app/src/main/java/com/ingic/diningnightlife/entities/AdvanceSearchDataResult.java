package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AdvanceSearchDataResult {

    @SerializedName("cuisines")
    @Expose
    private List<Cuisine> cuisines = null;
    @SerializedName("venue_type")
    @Expose
    private List<VenueType> venueType = null;
    @SerializedName("dietary")
    @Expose
    private List<Dietary> dietary = null;
    @SerializedName("quick_search")
    @Expose
    private List<QuickSearch> quickSearch = null;

    public List<Cuisine> getCuisines() {
        return cuisines;
    }

    public void setCuisines(List<Cuisine> cuisines) {
        this.cuisines = cuisines;
    }

    public List<VenueType> getVenueType() {
        return venueType;
    }

    public void setVenueType(List<VenueType> venueType) {
        this.venueType = venueType;
    }

    public List<Dietary> getDietary() {
        return dietary;
    }

    public void setDietary(List<Dietary> dietary) {
        this.dietary = dietary;
    }

    public List<QuickSearch> getQuickSearch() {
        return quickSearch;
    }

    public void setQuickSearch(List<QuickSearch> quickSearch) {
        this.quickSearch = quickSearch;
    }

}
