package com.ingic.diningnightlife.entities;

/**
 * Created by developer on 8/10/17.
 */

public class SimpleCheckbox {
    private String text;
    private boolean isSelected;

    public SimpleCheckbox(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
