package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoreVoucher {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("store_id")
    @Expose
    private int storeId;
    @SerializedName("store_type")
    @Expose
    private String storeType;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("voucher")
    @Expose
    private String voucher;
    @SerializedName("color_code")
    @Expose
    private String colorCode;
    @SerializedName("valid_upto")
    @Expose
    private String validUpto;
    @SerializedName("valid_date")
    @Expose
    private String validDate;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("name")
    @Expose
    private String senderOrReceiverName;
    @SerializedName("profile_image")
    @Expose
    private String profilePicture;
    @SerializedName("is_redeem")
    @Expose
    private int isRedeemed;
    private int position;

    public StoreVoucher(String title, String voucher, String colorCode, String validUpto, String validDate, String senderOrReceiverName, String profilePicture) {
        this.title = title;
        this.voucher = voucher;
        this.colorCode = colorCode;
        this.validUpto = validUpto;
        this.validDate = validDate;
        this.senderOrReceiverName = senderOrReceiverName;
        this.profilePicture = profilePicture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVoucher() {
        return voucher;
    }

    public String getColorCode() {
        return colorCode;
    }

    public String getValidUpto() {
        return validUpto;
    }

    public String getValidDate() {
        return validDate;
    }

    public String getType() {
        return type;
    }

    public int getStatus() {
        return status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getStoreType() {
        return storeType;
    }

    public String getSenderOrReceiverName() {
        return senderOrReceiverName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getIsRedeemed() {
        return isRedeemed;
    }
}
