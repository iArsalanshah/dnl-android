package com.ingic.diningnightlife.entities;


import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.List;

public class ParentEntity implements Parent<SharedVoucher> {
    private final List<SharedVoucher> childList;
    private String title;

    public ParentEntity(String title, List<SharedVoucher> list) {
        this.title = title;
        childList = list;
    }

    public String getTitle() {
        return title;
    }

    public SharedVoucher getChild(int position) {
//        if (childList == null || childList.size() < position) {
//            return null;
//        } else
            return childList.get(position);
    }

    @Override
    public List<SharedVoucher> getChildList() {
        return childList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
