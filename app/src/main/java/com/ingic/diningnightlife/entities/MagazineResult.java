package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MagazineResult {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("created_at")
    @Expose
    private Object createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("magazine_file")
    @Expose
    private String magazineFile;
    @SerializedName("magazine_url")
    @Expose
    private String magazineUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getMagazineFile() {
        return magazineFile;
    }

    public void setMagazineFile(String magazineFile) {
        this.magazineFile = magazineFile;
    }

    public String getMagazineUrl() {
        return magazineUrl;
    }
}