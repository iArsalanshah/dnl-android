package com.ingic.diningnightlife.entities;

/**
 * Created by developer on 8/7/17.
 */

public class DetailsPageMediaFileEntity {
    private int imgFile;
    private boolean isVideo;

    public DetailsPageMediaFileEntity(int imgFile, boolean isVideo) {
        this.imgFile = imgFile;
        this.isVideo = isVideo;
    }

    public int getImgFile() {
        return imgFile;
    }

    public boolean isVideo() {
        return isVideo;
    }
}
