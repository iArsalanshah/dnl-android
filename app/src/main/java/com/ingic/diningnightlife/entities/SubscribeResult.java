package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscribeResult {

    @SerializedName("is_subscribe")
    @Expose
    private int isSubscribe;
    @SerializedName("unique_code")
    @Expose
    private String uniqueCode;

    public int getIsSubscribe() {
        return isSubscribe;
    }

    public String getUniqueCode() {
        return uniqueCode;
    }
}