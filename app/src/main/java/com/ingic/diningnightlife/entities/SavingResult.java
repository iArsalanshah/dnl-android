package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SavingResult {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("store_id")
    @Expose
    private int storeId;
    @SerializedName("user_id")
    @Expose
    private int userId;
    @SerializedName("store_voucher_id")
    @Expose
    private int storeVoucherId;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("discount_percentage")
    @Expose
    private String discountPercentage;
    @SerializedName("discount_amount")
    @Expose
    private String discountAmount;
    @SerializedName("remaining_amount")
    @Expose
    private String remainingAmount;
    @SerializedName("is_availed")
    @Expose
    private Object isAvailed;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("store_detail")
    @Expose
    private StoreDetail storeDetail;
    @SerializedName("voucher_detail")
    @Expose
    private VoucherDetail voucherDetail;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStoreVoucherId() {
        return storeVoucherId;
    }

    public void setStoreVoucherId(int storeVoucherId) {
        this.storeVoucherId = storeVoucherId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(String remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public Object getIsAvailed() {
        return isAvailed;
    }

    public void setIsAvailed(Object isAvailed) {
        this.isAvailed = isAvailed;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public StoreDetail getStoreDetail() {
        return storeDetail;
    }

    public void setStoreDetail(StoreDetail storeDetail) {
        this.storeDetail = storeDetail;
    }

    public VoucherDetail getVoucherDetail() {
        return voucherDetail;
    }

    public void setVoucherDetail(VoucherDetail voucherDetail) {
        this.voucherDetail = voucherDetail;
    }
}
