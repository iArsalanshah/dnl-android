package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeResult {

    @SerializedName("featuredStores")
    @Expose
    private List<FeaturedStore> featuredStores = null;
    @SerializedName("allStores")
    @Expose
    private List<AllStore> allStores = null;

    public List<FeaturedStore> getFeaturedStores() {
        return featuredStores;
    }

    public void setFeaturedStores(List<FeaturedStore> featuredStores) {
        this.featuredStores = featuredStores;
    }

    public List<AllStore> getAllStores() {
        return allStores;
    }

    public void setAllStores(List<AllStore> allStores) {
        this.allStores = allStores;
    }

}
