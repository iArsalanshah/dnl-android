package com.ingic.diningnightlife.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PayFortData {
    public String paymentResponse = "";
    //Response Params
    @SerializedName("access_code")
    @Expose
    private String accessCode;
    @SerializedName("sdk_token")
    @Expose
    private String sdkToken;
    @SerializedName("response_message")
    @Expose
    private String responseMessage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response_code")
    @Expose
    private String responseCode;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("service_command")
    @Expose
    private String serviceCommand;
    @SerializedName("signature")
    @Expose
    private String signature;
    @SerializedName("merchant_identifier")
    @Expose
    private String merchantIdentifier;
    @SerializedName("eci")
    @Expose
    private String eci;
    @SerializedName("card_number")
    @Expose
    private String cardNumber;
    @SerializedName("fort_id")
    @Expose
    private String fortId;
    @SerializedName("customer_email")
    @Expose
    private String customerEmail;
    @SerializedName("customer_ip")
    @Expose
    private String customerIp;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("merchant_reference")
    @Expose
    private String merchantReference;
    @SerializedName("command")
    @Expose
    private String command;
    @SerializedName("payment_option")
    @Expose
    private String paymentOption;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;
    @SerializedName("authorization_code")
    @Expose
    private String authorizationCode;
    @SerializedName("return_url")
    @Expose
    private String return_url;
    @SerializedName("remember_me")
    @Expose
    private String remember_me;
    @SerializedName("card_holder_name")
    @Expose
    private String card_holder_name;
    @SerializedName("card_bin")
    @Expose
    private String card_bin;
    @SerializedName("token_name")
    @Expose
    private String token_name;

    public String getPaymentResponse() {
        return paymentResponse;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public String getSdkToken() {
        return sdkToken;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public String getStatus() {
        return status;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getLanguage() {
        return language;
    }

    public String getServiceCommand() {
        return serviceCommand;
    }

    public String getSignature() {
        return signature;
    }

    public String getMerchantIdentifier() {
        return merchantIdentifier;
    }

    public String getEci() {
        return eci;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getFortId() {
        return fortId;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public String getCustomerIp() {
        return customerIp;
    }

    public String getCurrency() {
        return currency;
    }

    public String getAmount() {
        return amount;
    }

    public String getMerchantReference() {
        return merchantReference;
    }

    public String getCommand() {
        return command;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public String getReturn_url() {
        return return_url;
    }

    public String getRemember_me() {
        return remember_me;
    }

    public String getCard_holder_name() {
        return card_holder_name;
    }

    public String getCard_bin() {
        return card_bin;
    }

    public String getToken_name() {
        return token_name;
    }
}
