package com.ingic.diningnightlife.interfaces;

/**
 * Created by developer on 8/10/17.
 */

public interface CheckboxListener {
    void checkBoxClick(boolean isChecked, int position);
}
