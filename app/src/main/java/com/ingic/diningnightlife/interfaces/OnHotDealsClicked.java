package com.ingic.diningnightlife.interfaces;

/**
 * Created by developer on 8/29/17.
 */

public interface OnHotDealsClicked {
    void onHotDealsItemClicked(int position, int storeId);
}
