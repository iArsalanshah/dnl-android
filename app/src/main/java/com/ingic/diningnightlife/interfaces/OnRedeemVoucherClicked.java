package com.ingic.diningnightlife.interfaces;

/**
 * Created by developer on 8/29/17.
 */

public interface OnRedeemVoucherClicked {
    void onRedeemItemClicked(String voucherName, String voucherId, String storeType);
}
