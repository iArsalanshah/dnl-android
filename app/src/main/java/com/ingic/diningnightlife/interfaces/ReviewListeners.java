package com.ingic.diningnightlife.interfaces;

import com.ingic.diningnightlife.entities.ReviewImage;
import com.ingic.diningnightlife.entities.ReviewResult;

import java.util.List;

/**
 * Created by syedatafseer on 8/25/2017.
 */

public interface ReviewListeners {
    void likeOrDislike(int reviewId, String status);

    void share(int id, ReviewResult item);

    void imageClick(List<ReviewImage> reviewImageList, int position);
}
