package com.ingic.diningnightlife.interfaces;

import com.ingic.diningnightlife.entities.PayFortData;

public interface IPaymentRequestCallBack {
    void onPaymentRequestResponse(int responseType, PayFortData responseData);
}
