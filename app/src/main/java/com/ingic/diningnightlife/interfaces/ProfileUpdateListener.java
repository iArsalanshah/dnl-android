package com.ingic.diningnightlife.interfaces;

/**
 * Created by developer on 8/24/17.
 */

public interface ProfileUpdateListener {
    void profileUpdate();
}
