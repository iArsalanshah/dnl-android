package com.ingic.diningnightlife.interfaces;

/**
 * Created by developer on 8/29/17.
 */

public interface OnOffersClick {
    void onOfferItemClicked(int position, int storeId);
}
