package com.ingic.diningnightlife.interfaces;

import com.ingic.diningnightlife.entities.SharedVoucher;

/**
 * Created by developer on 12/2/17.
 */

public interface ChildItemClickListener {
    void onClickListener(SharedVoucher entity);
}
