package com.ingic.diningnightlife.interfaces;

public interface LoadingListener {

	void onLoadingStarted();

	void onLoadingFinished();

	/**
	 * @param percentLoaded
	 *            should be between 1 and 100
	 */
    void onProgressUpdated(int percentLoaded);
}
