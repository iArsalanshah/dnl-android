package com.ingic.diningnightlife.interfaces;

import com.ingic.diningnightlife.entities.StoreVoucher;


public interface OnVoucherCheckBoxCL {
    void onCheckBoxClick(StoreVoucher item, int position, boolean isChecked);
}
