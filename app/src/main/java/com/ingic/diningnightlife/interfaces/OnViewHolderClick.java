package com.ingic.diningnightlife.interfaces;

import android.view.View;

/**
 * Created by developer on 7/27/17.
 */

public interface OnViewHolderClick {
    void onItemClick(View view, int position);
}