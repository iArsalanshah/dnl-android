package com.ingic.diningnightlife.interfaces;

/**
 * Created by developer on 8/9/17.
 */

public interface RouteFailedListener {
    void onRouteCreationFailed();
}
