/*
 * Created by Ingic on 11/9/17 4:31 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 11/9/17 3:31 PM
 */

package com.ingic.diningnightlife.wheel;

/**
 * Wheel adapter interface
 * 
 * @deprecated Use WheelViewAdapter
 */
public interface WheelAdapter {
	/**
	 * Gets items count
	 * @return the count of wheel items
	 */
    int getItemsCount();
	
	/**
	 * Gets a wheel item by index.
	 * 
	 * @param index the item index
	 * @return the wheel item text or null
	 */
    String getItem(int index);
	
	/**
	 * Gets maximum item length. It is used to determine the wheel width. 
	 * If -1 is returned there will be used the default wheel width.
	 * 
	 * @return the maximum item length or -1
	 */
    int getMaximumLength();
}
