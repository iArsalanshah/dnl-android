/*
 * Created by Ingic on 11/9/17 4:30 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 11/9/17 3:31 PM
 */

package com.ingic.diningnightlife.wheel.adapters;

import android.content.Context;

import com.ingic.diningnightlife.wheel.WheelAdapter;


/**
 * Adapter class for old wheel adapter (deprecated WheelAdapter class).
 * 
 * @deprecated Will be removed soon
 */
public class AdapterWheel extends AbstractWheelTextAdapter {

    // Source adapter
    private WheelAdapter adapter;
    
    /**
     * Constructor
     * @param context the current context
     * @param adapter the source adapter
     */
    public AdapterWheel(Context context, WheelAdapter adapter) {
        super(context);
        
        this.adapter = adapter;
    }

    /**
     * Gets original adapter
     * @return the original adapter
     */
    public WheelAdapter getAdapter() {
        return adapter;
    }
    
    @Override
    public int getItemsCount() {
        return adapter.getItemsCount();
    }

    @Override
    public CharSequence getItemText(int index) {
        return adapter.getItem(index);
    }

}
