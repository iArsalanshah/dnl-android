package com.ingic.diningnightlife.wheel.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.base.Function;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.activities.DockActivity;
import com.ingic.diningnightlife.entities.AllStore;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.abstracts.MatchableRVArrayAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;

public class AdapterAutoComplete extends RecyclerViewListAdapter<String> {

    private LayoutInflater inflater;

    public AdapterAutoComplete(Context context, OnViewHolderClick clickListener) {
        super(context, clickListener);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    protected View createView(Context context, ViewGroup viewGroup, int viewType) {
        return inflater.inflate(R.layout.item_autocomplete, viewGroup, false);
    }

    @Override
    protected void bindView(String item, RecyclerviewViewHolder viewHolder) {
        if (item != null) {
            TextView textView = (TextView) viewHolder.getView(R.id.textView);
            textView.setText(item);
        }
    }

    @Override
    protected int bindItemViewType(int position) {
        return 0;
    }

    @Override
    protected int bindItemId(int position) {
        return 0;
    }

}
