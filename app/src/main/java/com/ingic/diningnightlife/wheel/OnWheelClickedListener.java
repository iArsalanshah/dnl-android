/*
 * Created by Ingic on 11/9/17 4:31 PM
 * Copyright (c) 2017. All rights reserved.
 *
 * Last modified 11/9/17 3:31 PM
 */

package com.ingic.diningnightlife.wheel;

/**
 * Wheel clicked listener interface.
 * <p>The onItemClicked() method is called whenever a wheel item is clicked
 * <li> New Wheel position is set
 * <li> Wheel view is scrolled
 */
public interface OnWheelClickedListener {
    /**
     * Callback method to be invoked when current item clicked
     * @param wheel the wheel view
     * @param itemIndex the index of clicked item
     */
    void onItemClicked(WheelView wheel, int itemIndex);
}
