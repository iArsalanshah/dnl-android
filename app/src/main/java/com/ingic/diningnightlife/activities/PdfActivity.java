package com.ingic.diningnightlife.activities;

import android.support.v7.app.AppCompatActivity;

public class PdfActivity extends AppCompatActivity
//        implements OnPageChangeListener, OnLoadCompleteListener, OnErrorListener
{
//    private static final String TAG = PdfActivity.class.getSimpleName();
//    @BindView(R.id.toolbar)
//    Toolbar mToolbar;
//    @BindView(R.id.pdfView)
//    PDFView pdfView;
//    @BindView(R.id.tv_pdfActivity)
//    TextView tvPageNumber;
//    private int pageNumber = 1;
//    private File pdfDownloadedFile;
//    private boolean writtenToDisk;
//    private ProgressDialog progressDialog;
//    private ResponseBody respBody;
//    private String completePdfPath;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_pdf);
//        String cityId = getIntent().getStringExtra(AppConstants.INTENT_CITY_ID);
//        if (TextUtils.isEmpty(cityId)) {
//            cityId = "1";
//        }
//        ButterKnife.bind(this);
//        initToolbar();
//        init();
//        if (InternetHelper.CheckInternetConectivityandShowToast(this)) {
//            getMagazine(cityId);
//        }
//    }
//
//    private void initToolbar() {
//        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
//    }
//
//    private void init() {
//        progressDialog = new ProgressDialog(this);
//        progressDialog.setMessage("Please wait...");
//        progressDialog.setCancelable(false);
//    }
//
//    private void displayFromFile(File file) {
//        pdfView.fromFile(file)
//                .defaultPage(pageNumber)
//                .enableDoubletap(true)
//                .onPageChange(PdfActivity.this)
//                .swipeVertical(false)
//                .showMinimap(false)
//                .enableAnnotationRendering(false)
//                .password(null)
//                .showPageWithAnimation(true)
//                .onError(PdfActivity.this)
//                .onLoad(PdfActivity.this)
//                .load();
//    }
//
//    @Override
//    public void onPageChanged(int page, int pageCount) {
//        pageNumber = page;
//        tvPageNumber.setText(String.format(Locale.US, "%d", page));
//    }
//
//    @Override
//    public void loadComplete(int nbPages) {
//        PdfDocument.Meta meta = pdfView.getDocumentMeta();
//        Log.e(TAG, "title = " + meta.getTitle());
//        Log.e(TAG, "author = " + meta.getAuthor());
//        Log.e(TAG, "subject = " + meta.getSubject());
//        Log.e(TAG, "keywords = " + meta.getKeywords());
//        Log.e(TAG, "creator = " + meta.getCreator());
//        Log.e(TAG, "producer = " + meta.getProducer());
//        Log.e(TAG, "creationDate = " + meta.getCreationDate());
//        Log.e(TAG, "modDate = " + meta.getModDate());
//    }
//
//    @Override
//    public void onError(Throwable t) {
//        Log.e(TAG, "error = " + t.toString());
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        hideProgress();
//    }
//
//    @RestAPI
//    private void getMagazine(String cityId) {
//        showProgress();
//        WebService webService = WebServiceFactory.getWebServiceInstanceWithDefaultInterceptor(this, WebServiceConstants.SERVICE_BASE_URL);
//        webService.getMagazine(cityId).enqueue(new Callback<ResponseWrapper<MagazineResult>>() {
//            @Override
//            public void onResponse(Call<ResponseWrapper<MagazineResult>> call, Response<ResponseWrapper<MagazineResult>> response) {
//                if (PdfActivity.this.isFinishing()) {
//                    //Activity is no longer available 
//                    hideProgress();
//                    return;
//                }
//                if (response.body() == null || response.body().getResponse() == null) {
//                    UIHelper.showLongToastInCenter(PdfActivity.this, getResources().getString(R.string.null_response));
//                    hideProgress();
//                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
//                    updateUI(response.body().getResult());
//                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
//                    hideProgress();
//                    UIHelper.showLongToastInCenter(PdfActivity.this, response.body().getMessage());
//                } else {
//                    hideProgress();
//                    UIHelper.showLongToastInCenter(PdfActivity.this, getResources().getString(R.string.failure_response));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseWrapper<MagazineResult>> call, Throwable t) {
//                hideProgress();
//                UIHelper.showLongToastInCenter(PdfActivity.this, t.getMessage());
//            }
//        });
//    }
//
//    private void updateUI(MagazineResult result) {
//        if (result == null || result.getMagazineFile().isEmpty()) {
//            hideProgress();
//            if (PdfActivity.this.isDestroyed() || PdfActivity.this.isFinishing())
//                return;
//            else {
//                UIHelper.showShortToastInCenter(this, getResources().getString(R.string.failure_response));
//                return;
//            }
//        }
//        String pdfUrl = result.getMagazineFile();
//        String file = pdfUrl.substring(pdfUrl.lastIndexOf('/') + 1, pdfUrl.length());
////        String fileExtension = pdfUrl.substring(pdfUrl.lastIndexOf("."));
//        completePdfPath = this.getFilesDir() + File.separator + file;
//
//        if (isFileExist(completePdfPath)) {
//            //load from storage here
//            displayFromFile(new File(completePdfPath));
//            hideProgress();
//        } else {
//            //download file
//            getPdfFile(pdfUrl);
//        }
//    }
//
//    public boolean isFileExist(String fname) {
//        File file = new File(fname);
//        return file.exists();
//    }
//
//    private class DownloadFilesTask extends AsyncTask<Void, Void, Void> {
//
//        @Override
//        protected Void doInBackground(Void... params) {
//            writtenToDisk = writeResponseBodyToDisk(respBody);
//            return null;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            Log.d(TAG, "onPreExecute: ");
//            showProgress();
//        }
//
//        @Override
//        protected void onPostExecute(Void result) {
//            Log.d(TAG, "onPostExecute: ");
//            hideProgress();
//            if (writtenToDisk) {
//                displayFromFile(pdfDownloadedFile);
//            } else {
//                Log.d(TAG, "onPostExecute: write to disk error");
//                UIHelper.showLongToastInCenter(PdfActivity.this, getResources().getString(R.string.failure_response));
//            }
//        }
//    }
//
//    private void getPdfFile(String pdfUrl) {
//        WebService webService = WebServiceFactory.getWebServiceInstanceWithDefaultInterceptor(this, WebServiceConstants.SERVICE_BASE_URL);
//        Call<ResponseBody> call = webService.downloadFileWithDynamicUrlAsync(pdfUrl);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if (response.isSuccessful()) {
//                    respBody = response.body();
//                    Log.d(TAG, "server contacted and has file");
//                    new DownloadFilesTask().execute();
//                } else {
//                    hideProgress();
//                    UIHelper.showLongToastInCenter(PdfActivity.this, "Unable to download file!");
//                    Log.d(TAG, "server contact failed");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Log.d(TAG, "onFailure: " + t.getMessage());
//                hideProgress();
//                UIHelper.showLongToastInCenter(PdfActivity.this, t.getMessage());
//            }
//        });
//    }
//
//    private boolean writeResponseBodyToDisk(ResponseBody body) {
//        try {
//            // file location for each
//            pdfDownloadedFile = new File(completePdfPath);
//
//            InputStream inputStream = null;
//            OutputStream outputStream = null;
//
//            try {
//                byte[] fileReader = new byte[4096];
//
//                long fileSize = body.contentLength();
//                long fileSizeDownloaded = 0;
//
//                inputStream = body.byteStream();
//                outputStream = new FileOutputStream(pdfDownloadedFile);
//
//                while (true) {
//                    int read = inputStream.read(fileReader);
//
//                    if (read == -1) {
//                        break;
//                    }
//
//                    outputStream.write(fileReader, 0, read);
//
//                    fileSizeDownloaded += read;
//
//                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
//                }
//
//                outputStream.flush();
//
//                return true;
//            } catch (IOException e) {
//                return false;
//            } finally {
//                if (inputStream != null) {
//                    inputStream.close();
//                }
//
//                if (outputStream != null) {
//                    outputStream.close();
//                }
//            }
//        } catch (IOException e) {
//            return false;
//        }
//    }
//
//    private void showProgress() {
//        if (progressDialog != null && !PdfActivity.this.isFinishing() && !PdfActivity.this.isDestroyed())
//            progressDialog.show();
//    }
//
//    private void hideProgress() {
//        if (progressDialog != null && progressDialog.isShowing()
//                && !PdfActivity.this.isFinishing() && !PdfActivity.this.isDestroyed())
//            progressDialog.dismiss();
//    }
}