package com.ingic.diningnightlife.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import com.ingic.diningnightlife.R;

import java.util.Timer;
import java.util.TimerTask;


public class SplashActivity extends AppCompatActivity {
    final int MIN_TIME_INTERVAL_FOR_SPLASH = 2500; // in millis
    private ProgressBar mProgressBar;
    private int i = 0;
    private Timer splashTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mProgressBar = findViewById(R.id.progress);
        setProgressBarColor();
    }

    private void setProgressBarColor() {
        LayerDrawable layerDrawable = (LayerDrawable) mProgressBar.getProgressDrawable();
        Drawable progressDrawable = layerDrawable.findDrawableByLayerId(android.R.id.progress);
        progressDrawable.setColorFilter(ContextCompat.getColor(this, R.color.black), PorterDuff.Mode.SRC_IN);
    }

    @Override
    public void onResume() {
        super.onResume();
        startLoader();
        launchTimerAndTask();
    }

    private void startLoader() {
        mProgressBar.setProgress(i);
        CountDownTimer mCountDownTimer = new CountDownTimer(MIN_TIME_INTERVAL_FOR_SPLASH, 7) {

            @Override
            public void onTick(long millisUntilFinished) {
                i++;
                mProgressBar.setProgress(i * 100 / (MIN_TIME_INTERVAL_FOR_SPLASH / 10));
            }

            @Override
            public void onFinish() {
            }
        };
        mCountDownTimer.start();
    }

    private void launchTimerAndTask() {
        splashTimer = new Timer();
        splashTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                showMainActivity();
            }
        }, MIN_TIME_INTERVAL_FOR_SPLASH);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (splashTimer != null)
            splashTimer.cancel();
    }

    private void showMainActivity() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }
}