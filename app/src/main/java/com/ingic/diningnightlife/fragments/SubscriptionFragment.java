package com.ingic.diningnightlife.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.flyco.animation.BounceEnter.BounceEnter;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.BannerResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.SubscriptionResult;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.ui.views.AnyEditTextView;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ingic.diningnightlife.global.AppConstants.DEFAULT_BANNER_REFRESH_RATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubscriptionFragment extends BaseFragment implements View.OnClickListener {
    private static final int ONE_MONTH = 1;
    private static final int SIX_MONTHS = 2;
    private static final int ONE_YEAR = 3;
    private static boolean comesFromLogin;
    private final String BASE_URL_PAYFORT = "http://diningandnightlife.stagingic.com/public/payfort-php-sdk/";

    float sixMonthSaving, oneYearSaving;

    //Top Banner
    @BindView(R.id.img_subscription_BannerTop)
    ImageView img_BANNER;
    //Offer Containers
    @BindView(R.id.ll_subscription_oneMonth)
    LinearLayout llOneMonth;
    @BindView(R.id.ll_subscription_sixMonths)
    LinearLayout llSixMonths;
    @BindView(R.id.ll_subscription_oneYear)
    LinearLayout llOneYear;
    //Offer TextViews
    @BindView(R.id.tv_subscription_oneMonth)
    AnyTextView tvOneMonth_label;
    @BindView(R.id.tv_subscription_sixMonths)
    AnyTextView tvSixMonths_label;
    @BindView(R.id.tv_subscription_oneYear)
    AnyTextView tvOneYear_label;
    @BindView(R.id.v_subscription_oneMonth)
    View vOneMonth_line;
    @BindView(R.id.v_subscription_sixMonths)
    View vSixMonths_line;
    @BindView(R.id.v_subscription_oneYear)
    View vOneYear_line;
    @BindView(R.id.tv_subscription_oneMonth_price)
    AnyTextView tvOneMonth_price;
    @BindView(R.id.tv_subscription_sixMonths_price)
    AnyTextView tvSixMonths_price;
    @BindView(R.id.tv_subscription_oneYear_price)
    AnyTextView tvOneYear_price;
    @BindView(R.id.tv_subscription_oneMonth_mostPopular)
    AnyTextView tvOneMonths_mostPopular;
    @BindView(R.id.tv_subscription_sixMonths_mostPopular)
    AnyTextView tvSixMonths_mostPopular;
    @BindView(R.id.tv_subscription_oneYear_mostPopular)
    AnyTextView tvOneYear_mostPopular;
    @BindView(R.id.tv_subscription_oneMonth_savePercentage)
    AnyTextView tvOneMonths_savePercentage;
    @BindView(R.id.tv_subscription_sixMonth_savePercentage)
    AnyTextView tvSixMonths_savePercentage;
    @BindView(R.id.tv_subscription_oneYear_savePercentage)
    AnyTextView tvOneYear_savePercentage;
    //Enter Promo Code EditText
    @BindView(R.id.et_subscription_promoCode)
    AnyEditTextView etPromoCode;
    @BindView(R.id.tv_subcription_apply)
    AnyTextView tvApplyPromoCode;
    //UI Buttons
    @BindView(R.id.rl_subscription_subscribe)
    RelativeLayout rlSubscribe;
    @BindView(R.id.rl_subscription_notNow)
    RelativeLayout rlNotNow;
    @BindView(R.id.rl_subscription_upgrade)
    RelativeLayout rlUpgrade;
    ArrayList<SubscriptionResult> subscriptionResult;
    //For banner list:
    List<BannerResult> bannerList;
    Handler handler = new Handler();
    Runnable r;
    private Unbinder mBinder;
    private int subcription_id;
    private String subcription_amount;
    private String sub_code = "";
    private boolean mIsRestoredFromBackstack;

    public SubscriptionFragment() {
        // Required empty public constructor
    }

    public static SubscriptionFragment newInstance(boolean _comesFromLogin) {
        comesFromLogin = _comesFromLogin;
        return new SubscriptionFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIsRestoredFromBackstack = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_subscription, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        setListener();
        if (!mIsRestoredFromBackstack)
            getSubscriptions(prefHelper.getUser().getId());
//        getSubscriptions(96);
        getBanner(AppConstants.SUBSCRIPTION);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    private boolean isSubscribed() {
        if (subscriptionResult != null)
            for (SubscriptionResult item : subscriptionResult) {
                if (item.isSubscribed()) return true;
            }
        return false;
    }

    private void successDialog() {
        final NormalDialog dialog = new NormalDialog(getDockActivity());
        dialog.content(getResources().getString(R.string.message_success_subscription))
                .btnNum(1)
                .style(NormalDialog.STYLE_TWO)
                .title(getResources().getString(R.string.success))
                .titleTextSize(23)
                .contentGravity(Gravity.CENTER)
                .showAnim(new BounceEnter())
                .btnText(getResources().getString(android.R.string.ok));

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                        ChangePinFragment fragment = ChangePinFragment.newInstance();
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(AppConstants.BUNDLE_CREATE_NEW_PIN, true);
                        fragment.setArguments(bundle);
                        getDockActivity().replaceDockableFragment(fragment, ChangePinFragment.class.getSimpleName());
                    }
                });
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        handler.post(r);
    }

    private void isOfferSelected(boolean offerSelected) {
        if (offerSelected) {
            rlUpgrade.setVisibility(View.VISIBLE);
            rlSubscribe.setVisibility(View.GONE);
            rlNotNow.setVisibility(View.GONE);
//            unSelectAllExcept(ONE_MONTH);
        } else {
            rlUpgrade.setVisibility(View.GONE);
            rlSubscribe.setVisibility(View.VISIBLE);
            rlNotNow.setVisibility(View.VISIBLE);
        }
    }

    private void setListener() {
        llOneMonth.setOnClickListener(this);
        llSixMonths.setOnClickListener(this);
        llOneYear.setOnClickListener(this);

        rlSubscribe.setOnClickListener(this);
        rlNotNow.setOnClickListener(this);
        rlUpgrade.setOnClickListener(this);
        tvApplyPromoCode.setOnClickListener(this);
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getDockActivity().getResources().getString(R.string.subscription));
        titleBar.showSkipText(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToHome();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
        handler.removeCallbacks(r);
    }

    private void unSelectAllExcept(int selectedIndex) {
        switch (selectedIndex) {
            case ONE_MONTH:
                //stroke color
                llOneMonth.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_green));
                // llSixMonths.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
                //llOneYear.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));

                //label color
                tvOneMonth_label.setTextColor(getDockActivity().getResources().getColor(R.color.colorGreen));
//                tvSixMonths_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
//                tvOneYear_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));

                //Price
                tvOneMonth_price.setTextColor(getDockActivity().getResources().getColor(R.color.colorGreen));
                //View line
                vOneMonth_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.colorGreen));
//                vSixMonths_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
//                vOneYear_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));

                //Most Popular label
//                tvSixMonths_mostPopular.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_round_solid_orange));
                break;
            case SIX_MONTHS:
                //stroke color
                llSixMonths.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_green));
//                llOneMonth.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
//                llOneYear.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));

                //label color
                tvSixMonths_label.setTextColor(getDockActivity().getResources().getColor(R.color.colorGreen));
//                tvOneMonth_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
//                tvOneYear_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));

                //Price
                tvSixMonths_price.setTextColor(getDockActivity().getResources().getColor(R.color.colorGreen));
                //View line
                vSixMonths_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.colorGreen));
//                vOneMonth_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
//                vOneYear_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));

                //Most Popular label
//                tvSixMonths_mostPopular.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_round_solid_green));
                break;
            case ONE_YEAR:
                //stroke color
//                llOneMonth.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
//                llSixMonths.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_orange));
                llOneYear.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_green));

                //label color
//                tvOneMonth_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
//                tvSixMonths_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvOneYear_label.setTextColor(getDockActivity().getResources().getColor(R.color.colorGreen));

                //Price
                tvOneYear_price.setTextColor(getDockActivity().getResources().getColor(R.color.colorGreen));

                //View line
//                vOneMonth_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
//                vSixMonths_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
                vOneYear_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.colorGreen));

                //Most Popular label
//                tvSixMonths_mostPopular.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_round_solid_orange));
                break;
            default:
                //stroke color
                llOneMonth.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
                llSixMonths.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_orange));
                llOneYear.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));

                //label color
                tvOneMonth_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvSixMonths_label.setTextColor(getDockActivity().getResources().getColor(R.color.colorOrange));
                tvOneYear_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));

                //Price
                tvOneMonth_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvSixMonths_price.setTextColor(getDockActivity().getResources().getColor(R.color.colorOrange));
                tvOneYear_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));

                //View line
                vOneMonth_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
                vSixMonths_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.colorOrange));
                vOneYear_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));

                //Most Popular label
                tvSixMonths_mostPopular.setVisibility(View.VISIBLE);

                tvSixMonths_savePercentage.setVisibility(View.VISIBLE);

                // tvSixMonths_mostPopular.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_round_solid_orange));
                break;
        }
    }

    private void unSelectAllExcept1(int selectedIndex) {

        switch (selectedIndex) {
            case ONE_MONTH:

                llOneMonth.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_orange));
                llSixMonths.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
                llOneYear.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));

                //label color
                tvOneMonth_label.setTextColor(getDockActivity().getResources().getColor(R.color.colorOrange));
                tvSixMonths_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvOneYear_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));

                //Price
                tvOneMonth_price.setTextColor(getDockActivity().getResources().getColor(R.color.colorOrange));
                tvSixMonths_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvOneYear_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));

                //View line
                vOneMonth_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.colorOrange));
                vSixMonths_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
                vOneYear_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));

                //Most Popular label
//                tvOneMonths_mostPopular.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_round_solid_orange));
//                tvSixMonths_mostPopular

                tvOneMonths_mostPopular.setVisibility(View.VISIBLE);
                tvSixMonths_mostPopular.setVisibility(View.GONE);
                tvOneYear_mostPopular.setVisibility(View.GONE);

                tvOneMonths_savePercentage.setVisibility(View.VISIBLE);
                tvSixMonths_savePercentage.setVisibility(View.GONE);
                tvOneYear_savePercentage.setVisibility(View.GONE);

                updateSubscriptionUI(subscriptionResult, ONE_MONTH);

                break;
            case SIX_MONTHS:
//                updateSubscriptionUI(subscriptionResult, AppConstants.SELECTED);

                //stroke color
                llSixMonths.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_orange));
                llOneMonth.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
                llOneYear.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));

                //label color
                tvSixMonths_label.setTextColor(getDockActivity().getResources().getColor(R.color.colorOrange));
                tvOneMonth_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvOneYear_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));

                //Price
                tvSixMonths_price.setTextColor(getDockActivity().getResources().getColor(R.color.colorOrange));
                tvOneMonth_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvOneYear_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));

                //View line
                vSixMonths_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.colorOrange));
                vOneMonth_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
                vOneYear_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));

                //Most Popular label
                tvOneMonths_mostPopular.setVisibility(View.GONE);
                tvSixMonths_mostPopular.setVisibility(View.VISIBLE);
                tvOneYear_mostPopular.setVisibility(View.GONE);

                tvOneMonths_savePercentage.setVisibility(View.GONE);
                tvSixMonths_savePercentage.setVisibility(View.VISIBLE);
                tvOneYear_savePercentage.setVisibility(View.GONE);
                updateSubscriptionUI(subscriptionResult, SIX_MONTHS);

//                tvSixMonths_mostPopular.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_round_solid_green));
                break;

            case ONE_YEAR:
//                updateSubscriptionUI(subscriptionResult, AppConstants.SELECTED);

                //stroke color
                llOneMonth.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
                llSixMonths.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
                llOneYear.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_orange));

                //label color
                tvOneMonth_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvSixMonths_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvOneYear_label.setTextColor(getDockActivity().getResources().getColor(R.color.colorOrange));

                //Price
                tvOneMonth_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvSixMonths_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvOneYear_price.setTextColor(getDockActivity().getResources().getColor(R.color.colorOrange));

                //View line
                vOneMonth_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
                vSixMonths_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
                vOneYear_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.colorOrange));

                //Most Popular label
                tvOneMonths_mostPopular.setVisibility(View.GONE);
                tvSixMonths_mostPopular.setVisibility(View.GONE);
                tvOneYear_mostPopular.setVisibility(View.VISIBLE);

                tvOneMonths_savePercentage.setVisibility(View.GONE);
                tvSixMonths_savePercentage.setVisibility(View.GONE);
                tvOneYear_savePercentage.setVisibility(View.VISIBLE);
                updateSubscriptionUI(subscriptionResult, ONE_YEAR);

//                tvSixMonths_mostPopular.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_round_solid_orange));
                break;
            default:
                //stroke color
                llOneMonth.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
                llSixMonths.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
                llOneYear.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));

                //label color
                tvOneMonth_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvSixMonths_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvOneYear_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));

                //Price
                tvOneMonth_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvSixMonths_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));
                tvOneYear_price.setTextColor(getDockActivity().getResources().getColor(R.color.black));


                //View line
                vOneMonth_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
                vSixMonths_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
                vOneYear_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));

                //Most Popular label
                updateSubscriptionUI(subscriptionResult, 0);
//                tvSixMonths_mostPopular.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_round_solid_orange));
                break;
        }
    }

    public void setMiddleSelected() {
        //stroke color
//        llOneMonth.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));
        llSixMonths.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_orange));
//        llOneYear.setBackgroundDrawable(getDockActivity().getResources().getDrawable(R.drawable.drawable_stroke_gray));

        //label color
//        tvOneMonth_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
        tvSixMonths_label.setTextColor(getDockActivity().getResources().getColor(R.color.colorOrange));
//        tvOneYear_label.setTextColor(getDockActivity().getResources().getColor(R.color.black));
        //Price
        tvSixMonths_price.setTextColor(getDockActivity().getResources().getColor(R.color.colorOrange));
        //View line
//        vOneMonth_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));
        vSixMonths_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.colorOrange));
//        vOneYear_line.setBackgroundColor(getDockActivity().getResources().getColor(R.color.black));

        //Most Popular label
        tvSixMonths_mostPopular.setVisibility(View.VISIBLE);

        tvSixMonths_savePercentage.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        if (checkLoading())
            switch (view.getId()) {
                case R.id.ll_subscription_oneMonth:
                    unSelectAllExcept1(ONE_MONTH);
                    setSubIdAndAmount(0);
                    break;
                case R.id.ll_subscription_sixMonths:
                    unSelectAllExcept1(SIX_MONTHS);
                    setSubIdAndAmount(1);
                    break;
                case R.id.ll_subscription_oneYear:
                    unSelectAllExcept1(ONE_YEAR);
                    setSubIdAndAmount(2);
                    break;
                case R.id.rl_subscription_upgrade:
//                    if (UIHelper.doubleClickCheck() && checkLoading()) {
//                        if (!etPromoCode.isEnabled())
//                            sub_code = etPromoCode.getText().toString();
//                        else
//                            sub_code = "";
//                        if (checkData()) {
//                            isFirstTimeSubscription = false;
//                            purchaseSubcriptions(prefHelper.getUser().getId(),
//                                    subcription_id, sub_code,
//                                    subcription_amount);
//                        } else {
//                            isFirstTimeSubscription = false;
//                            setSubIdAndAmount(1);
//                            purchaseSubcriptions(prefHelper.getUser().getId(),
//                                    subcription_id, sub_code,
//                                    subcription_amount);
//                        }
//                    }
//                    break;
                case R.id.rl_subscription_subscribe:
//                    if (UIHelper.doubleClickCheck() && checkLoading()) {
//                        if (!etPromoCode.isEnabled())
//                            sub_code = etPromoCode.getText().toString();
//                        else
//                            sub_code = "";
//
//                        if (checkData()) {
//                            isFirstTimeSubscription = true;
//                            purchaseSubcriptions(prefHelper.getUser().getId(),
//                                    subcription_id, sub_code,
//                                    subcription_amount);
//                        } else {
//                            isFirstTimeSubscription = true;
//                            setSubIdAndAmount(1);
//                            purchaseSubcriptions(prefHelper.getUser().getId(),
//                                    subcription_id, sub_code,
//                                    subcription_amount);
//                        }
//                    }
//                    if (view.getId() == R.id.rl_subscription_subscribe)
//                        isFirstTimeSubscription = true;
                    if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()) && UIHelper.doubleClickCheck()) {
                        if (is100PercentAvailed()) {
                            purchaseSubcriptions(prefHelper.getUser().getId(),
                                    subcription_id == 0 ? 2 : subcription_id, sub_code
                            );
                        } else {
                            etPromoCode.setText("");
                            String url = String.format(Locale.US, "%s?uid=%d&sid=%d&code=%s",
                                    BASE_URL_PAYFORT, prefHelper.getUser().getId(), subcription_id == 0 ? 2 : subcription_id, sub_code);
//                            Util.openCustomChromeTabs(getDockActivity(), url);
//                            openWebPage(url);
                            getDockActivity().replaceDockableFragment(PurchaseSubscriptionFragment.newInstance(url, "Purchase Subscription"));
                        }
                    }
                    break;
                case R.id.rl_subscription_notNow:
                    if (UIHelper.doubleClickCheck())
                        navigateToHome();
                    break;

                case R.id.tv_subcription_apply:
                    if (UIHelper.doubleClickCheck()) {
                        if (isValidate()) {
                            getDiscountedSubscriptions(prefHelper.getUser().getId(), etPromoCode.getText().toString());
                        }
                    }
                    break;
                default:
                    break;
            }
    }

    public void openWebPage(String url) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        startActivity(intent);
//        Uri webpage = Uri.parse(url);
//        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
//        if (intent.resolveActivity(getDockActivity().getPackageManager()) != null) {
//            startActivity(intent);
//        }
    }

    private boolean isValidate() {
        return etPromoCode.testValidity();
    }

    private boolean checkData() {
        return subcription_id != 0 && !subcription_amount.isEmpty();
    }

    private void navigateToHome() {

//        if (subscriptionResult!= null && subscriptionResult.size() > 0 &&
//                subscriptionResult.get(0).getIsSubcribe() == 1, prefHelper.getIsUserSignUp()) {
//            ChangePinFragment fragment = ChangePinFragment.newInstance();
//            Bundle bundle = new Bundle();
//            bundle.putBoolean(AppConstants.BUNDLE_CREATE_NEW_PIN, true);
//            fragment.setArguments(bundle);
//            getDockActivity().replaceDockableFragment(fragment, ChangePinFragment.class.getSimpleName());
//        } else
        if (comesFromLogin) {
            getDockActivity().popBackStackTillEntry(0);
            getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(),
                    HomeFragment_SelectLocation.class.getSimpleName());
        } else {
            getDockActivity().popFragment();
        }

//        if (isFirstTimeSubscription) {
//            ChangePinFragment fragment = ChangePinFragment.newInstance();
//            Bundle bundle = new Bundle();
//            bundle.putBoolean(AppConstants.BUNDLE_CREATE_NEW_PIN, true);
//            fragment.setArguments(bundle);
//            getDockActivity().replaceDockableFragment(fragment, ChangePinFragment.class.getSimpleName());
//        } else {
//            if (comesFromLogin) {
//                getDockActivity().popBackStackTillEntry(0);
//                getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(), HomeFragment_SelectLocation.class.getSimpleName());
////                if (prefHelper.getIsUserSignUp()) {
////                    prefHelper.setIsUserSignUp(false);
////                    //TODO Abu Dhabi Only
////                    getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(), HomeFragment_SelectLocation.class.getSimpleName());
//////                    getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false), HomeFragment.class.getSimpleName());
////                } else {
////                    //TODO Abu Dhabi Only
////                    getDockActivity().replaceDockableFragment(HomeFragment.newInstance(true), HomeFragment.class.getSimpleName());
//////                    getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false), HomeFragment.class.getSimpleName());
////                }
//            } else {
//                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false), HomeFragment.class.getSimpleName());
//            }
////            if (!comesFromLogin)
////                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false), HomeFragment.class.getSimpleName());
////            else {
////                getDockActivity().popBackStackTillEntry(0);
////                if (prefHelper.getIsUserSignUp()) {
////                    prefHelper.setIsUserSignUp(false);
////                    //TODO Abu Dhabi Only
////                    getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(), HomeFragment_SelectLocation.class.getSimpleName());
//////                    getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false), HomeFragment.class.getSimpleName());
////                } else {
////                    //TODO Abu Dhabi Only
////                    getDockActivity().replaceDockableFragment(HomeFragment.newInstance(true), HomeFragment.class.getSimpleName());
//////                    getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false), HomeFragment.class.getSimpleName());
////                }
////            }
//        }

    }

    private void setSubIdAndAmount(int index) {
        subcription_id = 0;
        subcription_amount = "";
        if (subscriptionResult != null) {
            subcription_id = subscriptionResult.get(index) == null ? 0 : subscriptionResult.get(index).getId();
            subcription_amount = subscriptionResult.get(index) == null ? "" : subscriptionResult.get(index).getPrice();
        }
    }

    @RestAPI
    private void purchaseSubcriptions(final int user_id, int subscription_id, String subscription_code) {
        loadingStarted();
        webService.purchaseSubscription(user_id, subscription_id, subscription_code, "0")
                .enqueue(new Callback<ResponseWrapper>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper> call,
                                           Response<ResponseWrapper> response) {
                        loadingFinished();
                        if (SubscriptionFragment.this.isDetached() || SubscriptionFragment.this.isRemoving()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            //do something
                            UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.subscription_success));
                            navigateToHome();
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showShortToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                        loadingFinished();
                        UIHelper.showShortToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    @RestAPI
    private void getSubscriptions(int user_id) {
        loadingStarted();
        webService.getSubscriptions(user_id)
                .enqueue(new Callback<ResponseWrapper<ArrayList<SubscriptionResult>>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<ArrayList<SubscriptionResult>>> call,
                                           Response<ResponseWrapper<ArrayList<SubscriptionResult>>> response) {
                        loadingFinished();
                        if (SubscriptionFragment.this.isDetached() || SubscriptionFragment.this.isRemoving()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            //do something
                            subscriptionResult = response.body().getResult();
                            setDataWithSavingPercentageAmount(subscriptionResult);
                            updateSubscriptionUI(response.body().getResult(), 0);
                            setMiddleSelected();
                            checkFirstTimePurchase();
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showShortToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<ArrayList<SubscriptionResult>>> call, Throwable t) {
                        loadingFinished();
                        UIHelper.showShortToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    private void checkFirstTimePurchase() {
        if (prefHelper.getIsUserSignUp() && isSubscribed()) {
            successDialog();
        }
    }

    @RestAPI
    private void getDiscountedSubscriptions(int user_id, final String promoCode) {
        loadingStarted();
        webService.getDiscountedSubscriptions(
                user_id,
                subcription_id == 0 ? 2 : subcription_id,
                promoCode
        ).enqueue(new Callback<ResponseWrapper<ArrayList<SubscriptionResult>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<SubscriptionResult>>> call,
                                   Response<ResponseWrapper<ArrayList<SubscriptionResult>>> response) {
                loadingFinished();
                if (SubscriptionFragment.this.isDetached() || SubscriptionFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    sub_code = promoCode;
                    subscriptionResult = response.body().getResult();
                    setDataWithSavingPercentageAmount(subscriptionResult);
                    etPromoCode.setEnabled(false);
                    tvApplyPromoCode.setEnabled(false);
                    tvApplyPromoCode.setText("Applied");
                    totalAmountWithDiscountedValue(subscriptionResult);
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showShortToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<SubscriptionResult>>> call, Throwable t) {
                loadingFinished();
                UIHelper.showShortToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    private float localDiscountedValue(String orignalStringVal, String discountString) {
        float orignalDoubleVal = Util.getParsedFloat(orignalStringVal);
        float discount = Util.getParsedFloat(discountString);
        return orignalDoubleVal - (orignalDoubleVal * (discount / 100));
    }

    private void totalAmountWithDiscountedValue(ArrayList<SubscriptionResult> subscriptionResult) {
        String orignalStringVal = "";
        String discountString = "";
        for (int i = 0; i < subscriptionResult.size(); i++) {
            orignalStringVal = subscriptionResult.get(i).getPrice();
            discountString = subscriptionResult.get(i).getDiscountedPercentage();
            double orignalDoubleVal = Util.getParsedDouble(orignalStringVal);
            double discount = Util.getParsedDouble(discountString);

            double discountedVal = 0;
            discountedVal = orignalDoubleVal - (orignalDoubleVal * (discount / 100));
            SubscriptionResult obj = subscriptionResult.get(i);
            obj.setPrice(String.valueOf(discountedVal));
            //read 100% discount
            if (discount == 100) {
                obj.setCheck100PercentDiscounted(true);
            } else {
                obj.setCheck100PercentDiscounted(false);
            }
            subscriptionResult.set(i, obj);
        }
        this.subscriptionResult = subscriptionResult;
    }

    private boolean is100PercentAvailed() {
        for (int i = 0; i < subscriptionResult.size(); i++) {

            int index = i + 1;
            SubscriptionResult item = subscriptionResult.get(i);

            if (index == subcription_id /* is same container selected in which promo code applied */
                    && item.isCheck100PercentDiscounted()) {
                return true;
            }
        }
        return false;
    }

    private void updateSubscriptionUI(ArrayList<SubscriptionResult> subscriptionResult, int selected) {
        if (subscriptionResult == null || !SubscriptionFragment.this.isVisible()) return;
        for (int i = 0; i < subscriptionResult.size(); i++) {
            switch (i) {
                case 0:
                    if (subscriptionResult.get(i).getIsSubcribe() == AppConstants.YES
                            && selected != ONE_MONTH) {
                        isOfferSelected(true);
                        unSelectAllExcept(ONE_MONTH);
                    }
                    break;

                case 1:
                    if (subscriptionResult.get(i).getIsSubcribe() == AppConstants.YES
                            && selected != SIX_MONTHS) {
                        isOfferSelected(true);
                        unSelectAllExcept(SIX_MONTHS);
                    }
                    break;

                case 2:
                    if (subscriptionResult.get(i).getIsSubcribe() == AppConstants.YES
                            && selected != ONE_YEAR) {
                        unSelectAllExcept(ONE_YEAR);
                        isOfferSelected(true);
                    }
                    break;
                default:
                    break;
            }

        }
    }

    @RestAPI
    private void getBanner(String type) {
        loadingStarted();
        String cityId = prefHelper.getUser().getSelectedCity();
        webService.getBanner(cityId, type)
                .enqueue(new Callback<ResponseWrapper<ArrayList<BannerResult>>>() {

                             @Override
                             public void onResponse(Call<ResponseWrapper<ArrayList<BannerResult>>> call,
                                                    Response<ResponseWrapper<ArrayList<BannerResult>>> response) {
                                 loadingFinished();
                                 if (SubscriptionFragment.this.isDetached() || SubscriptionFragment.this.isRemoving()) {
                                     return;
                                 }
                                 if (response == null || response.body() == null || response.body().getResponse() == null) {
                                     UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                                 } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                                     try {
                                         bannerList = response.body().getResult();
                                         if (bannerList != null && bannerList.size() > 0) {
                                             //for exclusive
                                             if (bannerList.get(0).getIs_exclusive() == 1) {
                                                 ImageLoaderHelper.loadImageWithPicasso(getDockActivity(), bannerList.get(0).getBannerImage(), img_BANNER, true);

                                                 img_BANNER.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View view) {
                                                         if (UIHelper.doubleClickCheck())
                                                             Util.openCustomChromeTabs(getDockActivity(),
                                                                     bannerList.get(0).getLink());
                                                     }
                                                 });
                                             } else {
                                                 usingAlarmLogic(bannerList);
                                                 handler.post(r);
                                             }
                                         }
                                     } catch (Exception e) {
                                         e.printStackTrace();
                                     }
                                 } else {
                                     UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                                 }
                             }

                             @Override
                             public void onFailure(Call<ResponseWrapper<ArrayList<BannerResult>>> call, Throwable t) {
                                 loadingFinished();
                                 UIHelper.showShortToastInCenter(getDockActivity(), t.getMessage());
                             }
                         }
                );
    }

    private void usingAlarmLogic(final List<BannerResult> bannerList) {
        final int[] i = {0};
        r = new Runnable() {
            @Override
            public void run() {
                if (bannerList.size() > i[0] && img_BANNER != null) {
                    final int clickCount = i[0];
                    int setTime;
                    setTime = (TextUtils.isEmpty(bannerList.get(i[0]).getTime())) ? DEFAULT_BANNER_REFRESH_RATE :
                            (Util.getParsedInteger(bannerList.get(i[0]).getTime()) * 1000);

                    handler.postDelayed(this, setTime); //run the runnable in a minute again
                    i[0]++;

                    ImageLoaderHelper.loadImageWithPicasso(getDockActivity(), bannerList.get(clickCount).getBannerImage(), img_BANNER, false);

                    img_BANNER.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (UIHelper.doubleClickCheck())
                                Util.openCustomChromeTabs(getDockActivity(), bannerList.get(clickCount).getLink());
                        }
                    });

                } else {
                    i[0] = 0;

//                        UIHelper.showShortToastInCenter(getDockActivity(), "removeCallbacks " + i[0]);

                    handler.removeCallbacks(this);
                    handler.post(r);
                }
            }
        };
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void setDataWithSavingPercentageAmount(ArrayList<SubscriptionResult> subscriptionResult) {
        float oneMonthPrice = 0, sixMonthPrice, oneYearPrice;
        for (SubscriptionResult obj :
                subscriptionResult) {

            String discount = obj.getDiscountedPercentage();
            String stringPrice = obj.getPrice();
            float price = Util.getParsedFloat(stringPrice);
            int months = obj.getDays();

            if (months == 6) { //Six Months
                if (discount != null) {
                    sixMonthPrice = localDiscountedValue(stringPrice, discount);
                } else {
                    sixMonthPrice = price;
                }
                sixMonthSaving = 100 - ((sixMonthPrice / (months * oneMonthPrice)) * 100);
                tvSixMonths_price.setText(String.format(Locale.getDefault(), "AED  %.2f", sixMonthPrice));
//                tvSixMonths_savePercentage.setText(String.format(Locale.getDefault(), "Save %.0f%%", sixMonthSaving));
                tvSixMonths_savePercentage.setText("");
            } else if (months == 12) { //12 Months or One Year
                if (discount != null) {
                    oneYearPrice = localDiscountedValue(stringPrice, discount);
                } else {
                    oneYearPrice = price;
                }
                tvOneYear_price.setText(String.format(Locale.getDefault(), "AED  %.2f", oneYearPrice));
                oneYearSaving = 100 - ((oneYearPrice / (months * oneMonthPrice)) * 100);
//                tvOneYear_savePercentage.setText(String.format(Locale.getDefault(), "Save %.0f%%", oneYearSaving));
                tvOneYear_savePercentage.setText("");
            } else {//One Month
                float discountPrice = 0;
                oneMonthPrice = price;
                if (discount != null && !discount.equals("0")) {
                    discountPrice = localDiscountedValue(stringPrice, discount);
//                    tvOneMonths_savePercentage.setText(String.format(Locale.getDefault(), "Save %s%%", discount));
                    tvOneMonths_savePercentage.setText("");
                } else {
                    discountPrice = price;
                    tvOneMonths_savePercentage.setText("");
                }
                tvOneMonth_price.setText(String.format(Locale.getDefault(), "AED  %.2f", discountPrice));
            }
        }
//        for (int i = 0; i < subscriptionResult.size(); i++) {
//            if (i == 0) {//One Month
//                if (subscriptionResult.get(i).getDiscountedPercentage() != null) {
//                    oneMonthPrice = localDiscountedValue(
//                            subscriptionResult.get(i).getPrice(),
//                            subscriptionResult.get(i).getDiscountedPercentage()
//                    );
//                } else {
//                    oneMonthPrice = Util.getParsedFloat(subscriptionResult.get(i).getPrice());
//                }
////                oneMonthPrice = Util.getParsedInteger(subscriptionResult.get(i).getPrice());
//            } else if (subscriptionResult.get(i).getDays() == 6) {//Six Months
//                if (subscriptionResult.get(i).getDiscountedPercentage() != null) {
//                    sixMonthPrice = localDiscountedValue(subscriptionResult.get(i).getPrice(), subscriptionResult.get(i).getDiscountedPercentage());
//                } else {
//                    sixMonthPrice = Util.getParsedFloat(subscriptionResult.get(i).getPrice());
//                }
//                sixMonthSaving = 100 - ((sixMonthPrice / (subscriptionResult.get(i).getDays() * oneMonthPrice)) * 100);
//            } else if (subscriptionResult.get(i).getDays() == 12) {//Twelve Months
//                if (subscriptionResult.get(i).getDiscountedPercentage() != null) {
//                    oneYearPrice = localDiscountedValue(subscriptionResult.get(i).getPrice(), subscriptionResult.get(i).getDiscountedPercentage());
//                } else {
//                    oneYearPrice = Util.getParsedFloat(subscriptionResult.get(i).getPrice());
//                }
//                oneYearSaving = 100 - ((oneYearPrice / (subscriptionResult.get(i).getDays() * oneMonthPrice)) * 100);
//            }
//        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mIsRestoredFromBackstack = true;
    }

}