package com.ingic.diningnightlife.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.activities.FullScreenSliderActivity;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.ReviewImage;
import com.ingic.diningnightlife.entities.ReviewResult;
import com.ingic.diningnightlife.entities.StoreMenu;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.SelectShareIntent;
import com.ingic.diningnightlife.helpers.SimpleDividerItemDecoration;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.ReviewListeners;
import com.ingic.diningnightlife.ui.adapters.ReviewsAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.TitleBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ingic.diningnightlife.global.AppConstants.INTENT_FULLSCREEN_SLIDER;
import static com.ingic.diningnightlife.global.AppConstants.INTENT_FULLSCREEN_SLIDER_POSITION;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsFragment extends BaseFragment implements ReviewListeners {
    public static int LIKE = 1;
    public static int DISLIKE = 1;
    @BindView(R.id.rv_reviews)
    RecyclerView rvReviews;
    private Unbinder mBinder;
    private RecyclerViewListAdapter adapter;
    private String storeId;
    private ArrayList<ReviewResult> reviewResultList;

    public ReviewsFragment() {
        // Required empty public constructor
    }

    public static ReviewsFragment newInstance() {
        return new ReviewsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_reviews, container, false);
        storeId = "";
        if (getArguments() != null) {
            storeId = getArguments().getString(AppConstants.BUNDLE_STORE_ID, "");
        }
        mBinder = ButterKnife.bind(this, rootView);
        initRecyclerView();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
            getReviews(prefHelper.getUser().getId(), storeId);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    private void initRecyclerView() {
        rvReviews.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.VERTICAL, false));
        rvReviews.addItemDecoration(new SimpleDividerItemDecoration(getDockActivity()));
        adapter = new ReviewsAdapter(getDockActivity(), this);
        rvReviews.setAdapter(adapter);
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.reviews));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @RestAPI
    private void getReviews(int user_id, String store_id) {
        loadingStarted();
        webService.getReviews(user_id, store_id)
                .enqueue(new Callback<ResponseWrapper<ArrayList<ReviewResult>>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<ArrayList<ReviewResult>>> call,
                                           Response<ResponseWrapper<ArrayList<ReviewResult>>> response) {
                        loadingFinished();
                        if (ReviewsFragment.this.isDetached() || ReviewsFragment.this.isRemoving()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                            if (reviewResultList != null) {
                                adapter.addAll(reviewResultList);
                            }
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
//                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                            reviewResultList = response.body().getResult();
                            adapter.addAll(reviewResultList);

                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<ArrayList<ReviewResult>>> call, Throwable t) {
                        loadingFinished();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                        if (reviewResultList != null)
                            adapter.addAll(reviewResultList);
                    }
                });
    }

    @Override
    public void likeOrDislike(int reviewId, String status) {
        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
            setLikeOrDislike(prefHelper.getUser().getId(), storeId, reviewId, status);
        else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void share(int id, ReviewResult item) {
        shareReviewAPI(prefHelper.getUser().getId(), storeId, id);
        shareIntent(item);
    }

    @RestAPI
    private void setLikeOrDislike(int user_id, String store_id, int review_id, String status) {
        loadingStarted();
        webService.reviewLikeDislike(user_id, store_id, review_id, status)
                .enqueue(new Callback<ResponseWrapper>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper> call,
                                           Response<ResponseWrapper> response) {
                        loadingFinished();
                        if (ReviewsFragment.this.isDetached() || ReviewsFragment.this.isRemoving()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {

                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                        updateList();
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                        loadingFinished();
                        updateList();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    private void updateList() {
        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
            getReviews(prefHelper.getUser().getId(), storeId);
        else {
            adapter.notifyDataSetChanged();
        }
    }

    private void shareIntent(ReviewResult item) {
        String restuarantName = item.getStoreName().trim();
        String review = item.getReview().trim();
        String textToShare = String.format(Locale.US, "Check out this review on Dining and Nightlife Discounts of %s \"%s\"",
                restuarantName, review);
        SelectShareIntent.selectIntent(getDockActivity(), getResources().getString(R.string.share_review), restuarantName, textToShare);
    }


    @RestAPI
    private void shareReviewAPI(int userId, String storeId, int reviewId) {
        webService.addShare(userId, storeId, reviewId).enqueue(new Callback<ResponseWrapper>() {
            @Override
            public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                if (ReviewsFragment.this.isDetached() || ReviewsFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    updateList();
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                loadingFinished();
            }
        });
    }

    @Override
    public void imageClick(List<ReviewImage> reviewImageList, int position) {
        List<StoreMenu> imagesList = new ArrayList<>();
        if (reviewImageList != null) {
            for (int i = 0; i < reviewImageList.size(); i++) {
                ReviewImage obj = reviewImageList.get(i);
                imagesList.add(new StoreMenu(obj.getId(), 0,
                        obj.getReviewImage()));
            }
            getDockActivity().startActivity(new Intent(getDockActivity(), FullScreenSliderActivity.class)
                    .putExtra(INTENT_FULLSCREEN_SLIDER, new Gson().toJson(imagesList))
                    .putExtra(INTENT_FULLSCREEN_SLIDER_POSITION, position));
        }
    }
}