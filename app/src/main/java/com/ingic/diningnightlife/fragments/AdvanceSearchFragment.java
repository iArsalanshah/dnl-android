package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.AdvanceSearchDataResult;
import com.ingic.diningnightlife.entities.Cuisine;
import com.ingic.diningnightlife.entities.Dietary;
import com.ingic.diningnightlife.entities.QuickSearch;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.VenueType;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.GridSpacingItemDecoration;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.CheckboxListener;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.CheckboxesAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdvanceSearchFragment extends BaseFragment implements OnViewHolderClick, View.OnClickListener, CheckboxListener, AdapterView.OnItemSelectedListener {
    @BindView(R.id.rv_advanceSearch_checkBoxes)
    RecyclerView rvCheckboxes;
    @BindView(R.id.tv_advanceSearch_reset)
    AnyTextView tvReset;
    @BindView(R.id.img_advanceSearch_close)
    ImageView imgClose;
    @BindView(R.id.sp_advanceSearch_citySpinner)
    Spinner citySpinner;
    @BindView(R.id.sp_advanceSearch_storeTypeSpinner)
    Spinner storeTypeSpinner;
    @BindView(R.id.sp_advanceSearch_foodTypeSpinner)
    Spinner cuisineSpinner;
    @BindView(R.id.sp_advanceSearch_restaurantTypeSpinner)
    Spinner venueTypeSpinner;
    @BindView(R.id.sp_advanceSearch_dietTypeSpinner)
    Spinner dietTypeSpinner;
    @BindView(R.id.rl_advanceSearch_apply)
    RelativeLayout rlBtnApply;
    @BindView(R.id.container_cuisines)
    LinearLayout containerCuisines;
    @BindView(R.id.container_venueType)
    LinearLayout containerVenueType;
    @BindView(R.id.container_dietary)
    LinearLayout containerDietary;
    int check = 0;
    RecyclerViewListAdapter adapterListCheckBoxes;
    private Unbinder mBinder;
    private ArrayAdapter<String> spinnerCityAdapter, spinnerStoreTypeAdapter, spinnerCuisinesAdapter,
            spinnerVenueTypeAdapter, spinnerDietaryAdapter;

    private List<QuickSearch> simpleCheckboxList = new ArrayList<>();

    private boolean isLoadingResponse;

    public AdvanceSearchFragment() {
        // Required empty public constructor
    }

    public static AdvanceSearchFragment newInstance() {
        return new AdvanceSearchFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_advance_search, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        initRecyclerView();
        initAdapters();
//        setAdapter();
        setListener();
        return rootView;
    }

    private void initRecyclerView() {
        adapterListCheckBoxes = new CheckboxesAdapter(getDockActivity(), this, this);
        rvCheckboxes.setLayoutManager(new GridLayoutManager(getDockActivity(), 2));
        int spanCount = 2; // default 3 columns
        int spacing = 50; // default 50px
        rvCheckboxes.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, true));
        rvCheckboxes.setAdapter(adapterListCheckBoxes);
    }

    private void initAdapters() {
        spinnerCityAdapter = new ArrayAdapter<>(getDockActivity(), android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.city));
        spinnerStoreTypeAdapter = new ArrayAdapter<>(getDockActivity(), android.R.layout.simple_spinner_dropdown_item,
                getResources().getStringArray(R.array.StoreType));
//        spinnerCuisinesAdapter = new ArrayAdapter<>(getDockActivity(), android.R.layout.simple_spinner_dropdown_item,
//                getResources().getStringArray(R.array.foodType));
//        spinnerVenueTypeAdapter = new ArrayAdapter<>(getDockActivity(), android.R.layout.simple_spinner_dropdown_item,
//                getResources().getStringArray(R.array.restaurantType));
//        spinnerDietaryAdapter = new ArrayAdapter<>(getDockActivity(), android.R.layout.simple_spinner_dropdown_item,
//                getResources().getStringArray(R.array.dietType));
//
//        adapterListCheckBoxes = new CheckboxesAdapter(getDockActivity(), this, this);
        citySpinner.setAdapter(spinnerCityAdapter);
        String cityVal = prefHelper.getUser().getSelectedCity();
        //TODO Abu Dhabi Only
//        citySpinner.setSelection((cityVal.isEmpty() || cityVal.equals("1") ? 0 : 1));
        storeTypeSpinner.setAdapter(spinnerStoreTypeAdapter);
        storeTypeSpinner.setOnItemSelectedListener(this);
    }

    private void setAdapter() {
        cuisineSpinner.setAdapter(spinnerCuisinesAdapter);
        venueTypeSpinner.setAdapter(spinnerVenueTypeAdapter);
        dietTypeSpinner.setAdapter(spinnerDietaryAdapter);
    }

    private void setListener() {
        tvReset.setOnClickListener(this);
        imgClose.setOnClickListener(this);
        rlBtnApply.setOnClickListener(this);
//        storeTypeSpinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //get advance search data
        String storeType = "dinning";
        if (getArguments() != null) {
            String type = getArguments().getString(AppConstants.BUNDLE_STORE_TYPE);
            if (Util.isNotNullEmpty(type))
                storeType = type;
        }
        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
            getSearchData(storeType);
    }

    private void resetAllSpinners() {
        citySpinner.setSelection(0);
        storeTypeSpinner.setSelection(0);
        cuisineSpinner.setSelection(0);
        venueTypeSpinner.setSelection(0);
        dietTypeSpinner.setSelection(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().releaseDrawer();
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setRightButton(R.drawable.ic_search_filter, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLoadingResponse)
                    UIHelper.showLongToastInCenter(getActivity(),
                            R.string.message_wait);
                else
                    applyAdvanceSearch();
            }
        });
        titleBar.setSubHeading(getResources().getString(R.string.search));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_advanceSearch_reset:
                //reset all views
            case R.id.img_advanceSearch_close:
                //close ???
                resetAllSpinners();
                resetAllCheckBoxes();
                break;
            case R.id.rl_advanceSearch_apply:
                if (isLoadingResponse)
                    UIHelper.showLongToastInCenter(getActivity(),
                            R.string.message_wait);
                else
                    applyAdvanceSearch();
                break;
            default:
                break;
        }
    }

    private void applyAdvanceSearch() {
        String cityId = "2";//TODO Abu Dhabi Only
//        if (citySpinner.getSelectedItemPosition() == 1) {
//            cityId = "2";
//        }

        String storeType = "";
        switch (storeTypeSpinner.getSelectedItemPosition()) {
            case 0:
                storeType = "dinning";
                break;
            case 1:
                storeType = "brunch";
                break;
            case 2:
                storeType = "last_minute";
                break;
            case 3:
                storeType = "exclusive";
                break;
            case 4:
                storeType = "nightlife";
                break;
            default:
                break;
        }


        String cuisine = "", venueType = "", dietary = "";
        try {
            if (cuisineSpinner.getSelectedItemPosition() != 0)
                cuisine = cuisineSpinner.getSelectedItem().toString();
            if (venueTypeSpinner.getSelectedItemPosition() != 0)
                venueType = venueTypeSpinner.getSelectedItem().toString();
            if (dietTypeSpinner.getSelectedItemPosition() != 0)
                dietary = dietTypeSpinner.getSelectedItem().toString();
        } catch (Exception ex) {//ignore
        }

        String quickSearch = "";
        int now_open = 0;
        StringBuilder result = new StringBuilder();
        for (QuickSearch obj :
                simpleCheckboxList) {
            if (obj.getName() != null && !obj.getName().equals("Now Open")
                    && obj.isSelected()) {
                result.append(obj.getName());
                result.append(",");
            }
            if (obj.getName() != null && obj.getName().equals("Now Open")
                    && obj.isSelected()) {
                now_open = 1;
            }
        }
        if (result.length() > 0)
            quickSearch = result.substring(0, result.length() - 1);

        onAdvanceSearchSubmit(cityId, storeType, cuisine, venueType, dietary, quickSearch, now_open);
    }

    private void resetAllCheckBoxes() {
        for (QuickSearch obj :
                simpleCheckboxList) {
            obj.setSelected(false);
        }
        adapterListCheckBoxes.addAll(simpleCheckboxList);
    }

    @Override
    public void checkBoxClick(boolean isChecked, int position) {
        if (simpleCheckboxList != null && simpleCheckboxList.size() > position) {
            simpleCheckboxList.get(position).setSelected(isChecked);
        }
    }

    @RestAPI
    private void getSearchData(String type) {
        isLoadingResponse = true;
        loadingStarted();
        webService.getAdvanceSearchData(type).enqueue(new Callback<ResponseWrapper<AdvanceSearchDataResult>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<AdvanceSearchDataResult>> call, Response<ResponseWrapper<AdvanceSearchDataResult>> response) {
                if (AdvanceSearchFragment.this.isDetached() || AdvanceSearchFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    try {
                        updateUI(response.body().getResult());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
                loadingFinished();
            }

            @Override
            public void onFailure(Call<ResponseWrapper<AdvanceSearchDataResult>> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    @UiThread
    private void updateUI(AdvanceSearchDataResult result) {
        if (result == null) return;

        //quick search data
        if (result.getQuickSearch() != null) {
            simpleCheckboxList = result.getQuickSearch();
            adapterListCheckBoxes.addAll(result.getQuickSearch());
        }

        //Cuisines
        List<Cuisine> cuisines = result.getCuisines();
        ArrayList<String> stringList1 = new ArrayList<>();

        if (cuisines.size() > 0) {
            containerCuisines.setVisibility(View.VISIBLE);
            stringList1.add(getDockActivity().getResources().getString(R.string.select_cuisine));
            for (int i = 0; i < cuisines.size(); i++) {
                stringList1.add(cuisines.get(i).getName());
            }
        } else containerCuisines.setVisibility(View.GONE);

        spinnerCuisinesAdapter = new ArrayAdapter<>(getDockActivity(), android.R.layout.simple_spinner_dropdown_item,
                stringList1);

        //venue_type
        List<VenueType> vanueType = result.getVenueType();
        ArrayList<String> stringList2 = new ArrayList<>();

        if (vanueType.size() > 0) {
            containerVenueType.setVisibility(View.VISIBLE);
            stringList2.add(getDockActivity().getResources().getString(R.string.select_venue));
            for (int i = 0; i < vanueType.size(); i++) {
                stringList2.add(vanueType.get(i).getName());
            }
        } else containerVenueType.setVisibility(View.GONE);

        spinnerVenueTypeAdapter = new ArrayAdapter<>(getDockActivity(), android.R.layout.simple_spinner_dropdown_item,
                stringList2);

        //dietary
        List<Dietary> dietaryList = result.getDietary();
        ArrayList<String> stringList3 = new ArrayList<>();

        if (dietaryList.size() > 0) {
            containerDietary.setVisibility(View.VISIBLE);
            stringList3.add(getDockActivity().getResources().getString(R.string.select_dietary));
            for (int i = 0; i < dietaryList.size(); i++) {
                stringList3.add(dietaryList.get(i).getName());
            }
        } else containerDietary.setVisibility(View.GONE);

        spinnerDietaryAdapter = new ArrayAdapter<>(getDockActivity(), android.R.layout.simple_spinner_dropdown_item,
                stringList3);

        setAdapter();
        isLoadingResponse = false;
    }

    private void onAdvanceSearchSubmit(String cityId, String storeType, String cuisine,
                                       String venueType, String dietary, String quickSearch, int now_open) {
        StoreListingFiterableFragment fragment = new StoreListingFiterableFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_STORE_TYPE, storeType);
        bundle.putBoolean(AppConstants.BUNDLE_IS_ADVANCE_SEARCH, true);
        bundle.putString(AppConstants.BUNDLE_CITY_ID, cityId);
        bundle.putString(AppConstants.BUNDLE_CUISINE, cuisine);
        bundle.putString(AppConstants.BUNDLE_VENUE, venueType);
        bundle.putString(AppConstants.BUNDLE_DIETARY, dietary);
        bundle.putString(AppConstants.BUNDLE_QUICK_SEARCH, quickSearch);
        bundle.putInt(AppConstants.BUNDLE_QUICK_SEARCH_NOW_OPEN, now_open);
        fragment.setArguments(bundle);
        getDockActivity().popFragment();
        getDockActivity().replaceDockableFragment(fragment, StoreListingFiterableFragment.class.getSimpleName());
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (++check > 1) {
            String storeType = "";
            switch (i) {
                case 0:
                    storeType = "dinning";
                    break;
                case 1:
                    storeType = "brunch";
                    break;
                case 2:
                    storeType = "last_minute";
                    break;
                case 3:
                    storeType = "exclusive";
                    break;
                case 4:
                    storeType = "nightlife";
                    break;
                default:
                    break;
            }
            if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
                getSearchData(storeType);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}