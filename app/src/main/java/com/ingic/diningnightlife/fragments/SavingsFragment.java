package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.SavingResult;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.SimpleDividerItemDecoration;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.SavingsAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SavingsFragment extends BaseFragment implements OnViewHolderClick {
    private final static String TAG = SavingsFragment.class.getSimpleName();
    @BindView(R.id.rv_savings)
    RecyclerView rvSavings;
    @BindView(R.id.tv_savings_savedAmountText)
    AnyTextView txtSavedAmount;
    @BindView(R.id.line)
    View line;
    private RecyclerViewListAdapter adapter;
    private Unbinder mBinder;
    private final String userAvailedAnyVoucher = "Congrats, so far you\'ve saved AED %.2f! ";
    private final String userNotAvailedAnyVoucher = "So far you\'ve saved AED %.2f! ";

    public SavingsFragment() {
        // Required empty public constructor
    }

    public static SavingsFragment newInstance() {
        return new SavingsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_savings, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        initRecyclerView();
        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
            getSavings(prefHelper.getUser().getId());
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindData();
    }

    private void bindData() {
//        List<String> imgResources = new ArrayList<>();
//        imgResources.add(String.valueOf(R.drawable.food_images_1));
//        imgResources.add(String.valueOf(R.drawable.food_images_2));
//        imgResources.add(String.valueOf(R.drawable.food_images_3));
//        imgResources.add(String.valueOf(R.drawable.food_images_4));
//        imgResources.add(String.valueOf(R.drawable.food_images_5));
//
//        List<String> titleNames = new ArrayList<>();
//        titleNames.add("Villa Barcelona Cafe");
//        titleNames.add("Nefertiti");
//        titleNames.add("Diademas");
//        titleNames.add("Azure");
//        titleNames.add("Gazebo Cuisine");
//
//        ArrayList<CapsuleEntity> pillsEntities = new ArrayList<>();
//        pillsEntities.add(new CapsuleEntity("25%"));
//        pillsEntities.add(new CapsuleEntity("241"));
//        pillsEntities.add(new CapsuleEntity("50%"));
//        pillsEntities.add(new CapsuleEntity("", R.drawable.ic_wifi));
//        pillsEntities.add(new CapsuleEntity("", R.drawable.ic_wifi));

        List<SavingResult> savingsEntityList = new ArrayList<>();
//        for (int i = 0; i < 5; i++) {
//            savingsEntityList.add(new SavingResult(imgResources.get(i), titleNames.get(i), "AED 50.00",
//                    pillsEntities.get(i), "31-05-07", "16:36"));
//        }
        adapter.addAll(savingsEntityList);
    }

    private void initRecyclerView() {
        adapter = new SavingsAdapter(getDockActivity(), this);
        rvSavings.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.VERTICAL, false));
        rvSavings.addItemDecoration(new SimpleDividerItemDecoration(getDockActivity()));
        rvSavings.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.savings));
    }

    @Override
    public void onItemClick(View view, int position) {
    }

    @RestAPI
    private void getSavings(int user_id) {
        Log.d(TAG + "User_id = ", String.valueOf(user_id));
        loadingStarted();
        webService.getSavings(user_id)
                .enqueue(new Callback<ResponseWrapper<ArrayList<SavingResult>>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<ArrayList<SavingResult>>> call,
                                           Response<ResponseWrapper<ArrayList<SavingResult>>> response) {
                        loadingFinished();
                        if (SavingsFragment.this.isDetached() || SavingsFragment.this.isRemoving()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            float amount = getTotalAmount(response.body().getResult());
                            if (amount > 0) {
                                txtSavedAmount.setText(String.format(Locale.getDefault(),
                                        userAvailedAnyVoucher, amount
                                ));
                                line.setVisibility(View.VISIBLE);
                            } else {
                                txtSavedAmount.setText(String.format(Locale.getDefault(),
                                        userNotAvailedAnyVoucher, amount
                                ));
                                line.setVisibility(View.GONE);
                            }
                            adapter.addAll(response.body().getResult());
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<ArrayList<SavingResult>>> call, Throwable t) {
                        loadingFinished();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    private float getTotalAmount(ArrayList<SavingResult> result) {
        float totalSaving = 0;
        for (int i = 0; i < result.size(); i++) {
            totalSaving += result.get(i).getDiscountAmount() == null ? 0 : Util.getParsedFloat(result.get(i).getDiscountAmount());
        }
        return totalSaving;
    }
}
