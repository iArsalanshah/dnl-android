package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.BannerResult;
import com.ingic.diningnightlife.entities.NotificationsResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.RecyclerItemTouchHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.NotificationsAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ingic.diningnightlife.global.AppConstants.DEFAULT_BANNER_REFRESH_RATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends BaseFragment implements OnViewHolderClick, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    @BindView(R.id.img_notifications_BANNER)
    ImageView img_BANNER;
    @BindView(R.id.rv_notifications)
    RecyclerView rvNotifications;
    int timerIteration = 0;
    @BindView(R.id.alternateText)
    AnyTextView alternateText;
    private RecyclerViewListAdapter adapter;
    private Unbinder mBinder;
    private Timer timer = new Timer();
    private View rootView;
    private int deletedIndex;
    private NotificationsResult deletedItem;
    private ArrayList<BannerResult> bannerList;

    Handler handler = new Handler();
    Runnable r;

    public NotificationsFragment() {
        // Required empty public constructor
    }

    public static NotificationsFragment newInstance() {
        return new NotificationsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_notifications, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        initRecyclerView();
        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity())) {
            getNotifications(prefHelper.getUser().getId());
            getBanner(AppConstants.NOTIFCATIONS);
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindData();
    }

    private void bindData() {
//        ArrayList<NotificationsResult> entityArrayList = new ArrayList<>();
//        entityArrayList.add(new NotificationsEntity("You have Successfully subscribed to annual plan"));
//        entityArrayList.add(new NotificationsEntity("You have Successfully subscribed to annual plan and " +
//                "saved AED 150"));
//        entityArrayList.add(new NotificationsEntity("You have Successfully subscribed to annual plan"));
//        entityArrayList.add(new NotificationsEntity("You have Successfully subscribed to annual plan"));
//        adapter.addAll(entityArrayList);
    }

    private void initRecyclerView() {
        adapter = new NotificationsAdapter(getDockActivity(), this);
        rvNotifications.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.VERTICAL, false));
//        rvNotifications.addItemDecoration(new SimpleDividerItemDecoration(getDockActivity()));
        rvNotifications.setAdapter(adapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rvNotifications);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();


    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.notifications));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
        handler.removeCallbacks(r);
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onStart() {
        super.onStart();
        handler.post(r);
    }

    @RestAPI
    private void getNotifications(int user_id) {
        loadingStarted();
        webService.getNotifications(user_id)
                .enqueue(new Callback<ResponseWrapper<ArrayList<NotificationsResult>>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<ArrayList<NotificationsResult>>> call,
                                           Response<ResponseWrapper<ArrayList<NotificationsResult>>> response) {
                        loadingFinished();
                        if (NotificationsFragment.this.isDetached() || NotificationsFragment.this.isRemoving()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            updateUI(response.body().getResult());
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<ArrayList<NotificationsResult>>> call, Throwable t) {
                        loadingFinished();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    private void updateUI(ArrayList<NotificationsResult> result) {
        if (!NotificationsFragment.this.isVisible()) return;
        if (result == null || result.size() == 0) {
            alternateText.setVisibility(View.VISIBLE);
        } else {
            alternateText.setVisibility(View.GONE);
            adapter.addAll(result);
        }
    }

    @RestAPI
    private void getBanner(String type) {
        loadingStarted();
        String cityId = prefHelper.getUser().getSelectedCity();
        webService.getBanner(cityId, type)
                .enqueue(new Callback<ResponseWrapper<ArrayList<BannerResult>>>() {

                             @Override
                             public void onResponse(Call<ResponseWrapper<ArrayList<BannerResult>>> call,
                                                    Response<ResponseWrapper<ArrayList<BannerResult>>> response) {
                                 loadingFinished();
                                 if (NotificationsFragment.this.isDetached() || NotificationsFragment.this.isRemoving()) {
                                     return;
                                 }
                                 if (response == null || response.body() == null || response.body().getResponse() == null) {
                                     UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                                 } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                                     try {
                                         bannerList = response.body().getResult();
                                         if (bannerList != null && bannerList.size() > 0) {
                                             //for exclusive
                                             if (bannerList.get(0).getIs_exclusive() == 1) {
                                                 ImageLoaderHelper.loadImageWithPicasso(getDockActivity(), bannerList.get(0).getBannerImage(), img_BANNER, true);

                                                 img_BANNER.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View view) {
                                                         if (UIHelper.doubleClickCheck())
                                                             Util.openCustomChromeTabs(getDockActivity(),
                                                                     bannerList.get(0).getLink());
                                                     }
                                                 });
                                             } else {
                                                 usingAlarmLogic(bannerList);
                                                 handler.post(r);
                                             }
                                         }
                                     } catch (Exception e) {
                                         e.printStackTrace();
                                     }
                                 } else {
                                     UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                                 }
                             }

                             @Override
                             public void onFailure(Call<ResponseWrapper<ArrayList<BannerResult>>> call, Throwable t) {
                                 loadingFinished();
                                 UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                             }
                         }


                );
    }

    private void usingAlarmLogic(final List<BannerResult> bannerList) {
        final int[] i = {0};
        if (bannerList != null && bannerList.size() > 0)
            r = new Runnable() {
                @Override
                public void run() {
                    if (bannerList.size() > i[0]) {
                        final int clickCount = i[0];
                        int setTime;
                        setTime = (TextUtils.isEmpty(bannerList.get(i[0]).getTime())) ? DEFAULT_BANNER_REFRESH_RATE :
                                (Util.getParsedInteger(bannerList.get(i[0]).getTime()) * 1000);

                        handler.postDelayed(this, setTime); //run the runnable in a minute again
                        i[0]++;

                        ImageLoaderHelper.loadImageWithPicasso(getDockActivity(), bannerList.get(clickCount).getBannerImage(), img_BANNER, false);

                        img_BANNER.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (UIHelper.doubleClickCheck())
                                    Util.openCustomChromeTabs(getDockActivity(), bannerList.get(clickCount).getLink());
                            }
                        });

                    } else {
                        i[0] = 0;

//                        UIHelper.showShortToastInCenter(getDockActivity(), "removeCallbacks " + i[0]);

                        handler.removeCallbacks(this);
                        handler.post(r);
                    }
                }
            };
    }


//    private void setImagesWithTimer(final List<BannerResult> bannerList) {
//        long refreshRate = bannerList.get(timerIteration).getTime() == null ? DEFAULT_BANNER_REFRESH_RATE:
//                (Util.getParsedLong(bannerList.get(timerIteration).getTime())*1000);
//
//        timer.scheduleAtFixedRate(new TimerTask() {
//            @Override
//            public void run() {
//                if (timerIteration < bannerList.size()) {
//                    getDockActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            ImageLoaderHelper.loadImage(bannerList.get(timerIteration)
//                                    .getBannerImage(), img_BANNER);
//                            img_BANNER.setOnClickListener(new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    Util.openCustomChromeTabs(getDockActivity(),);
//                                }
//                            });
//                            timerIteration++;
//                        }
//                    });
//                }
//                else if (bannerList.size()>1){
//                    timerIteration = 0;
//                }
//            }
//        }, 0, refreshRate);
//    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof NotificationsAdapter.RecyclerviewViewHolder) {
            // get the removed item name to display it in snack bar
//            String name = cartList.get(viewHolder.getAdapterPosition()).getName();

            // backup of removed item for undo purpose
            if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity())) {
                deletedItem = (NotificationsResult) adapter.getItem(viewHolder.getAdapterPosition());
                deletedIndex = viewHolder.getAdapterPosition();

                // remove the item from recycler view
                adapter.removeItem(viewHolder.getAdapterPosition(), false);
                deleteNotificationAPI(deletedItem.getId());
            }
            // showing snack bar with Undo option
//            Snackbar snackbar = Snackbar
//                    .make(rootView, "Notification Removed", Snackbar.LENGTH_LONG);
//            snackbar.setAction("UNDO", new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    // undo is selected, restore the deleted item
//                    adapter.restoreItem(deletedItem, deletedIndex, false);
//                }
//            });
//            snackbar.addCallback(new Snackbar.Callback(){
//                @Override
//                public void onDismissed(Snackbar transientBottomBar, int event) {
//                    super.onDismissed(transientBottomBar, event);
//                    deleteNotificationAPI(deletedItem.getId());
//                }
//            });
//            snackbar.setActionTextColor(Color.YELLOW);
//            snackbar.show();
        }
    }

    private void restoreDeletedItem(NotificationsResult deletedItem, int deletedIndex) {
        adapter.restoreItem(deletedItem, deletedIndex, false);
    }

    @RestAPI
    private void deleteNotificationAPI(int id) {
        webService.deleteNotification(id).enqueue(new Callback<ResponseWrapper>() {
            @Override
            public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                if (NotificationsFragment.this.isVisible() && response.body() != null
                        && response.body().getResponse() != null && response.body().getMessage() != null
                        && response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    rvNotifications.postOnAnimation(new Runnable() {
                        @Override
                        public void run() {
                            if (adapter.getList().size() == 0) {
                                alternateText.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                } else if (NotificationsFragment.this.isVisible()) {
                    UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                    restoreDeletedItem(deletedItem, deletedIndex);
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                restoreDeletedItem(deletedItem, deletedIndex);
                UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
            }
        });
    }
}