package com.ingic.diningnightlife.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Switch;

import com.ingic.diningnightlife.BuildConfig;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.LoginResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.CameraHelper;
import com.ingic.diningnightlife.helpers.DateArrayAdapter;
import com.ingic.diningnightlife.helpers.DateHelper;
import com.ingic.diningnightlife.helpers.DateNumericAdapter;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.ProfileUpdateListener;
import com.ingic.diningnightlife.ui.views.AnyEditTextView;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;
import com.ingic.diningnightlife.wheel.OnWheelChangedListener;
import com.ingic.diningnightlife.wheel.WheelView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RuntimePermissions
public class ProfileFragment extends BaseFragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    private static final int CAMERA_REQUEST = 1001;
    static ProfileUpdateListener profileUpdateListener;
    private File fileUrl;
    @BindView(R.id.et_profile_fullName)
    AnyEditTextView etFullName;
    @BindView(R.id.et_profile_email)
    AnyEditTextView etEmail;
    @BindView(R.id.et_profile_phone)
    AnyEditTextView etPhone;
    @BindView(R.id.tv_profile_dob)
    AnyTextView tvDOB;
    @BindView(R.id.tv_profile_changePin)
    AnyTextView tvChangePIN;
    @BindView(R.id.tv_profile_notification)
    AnyTextView tvNotification;
    @BindView(R.id.switch_profile_notification)
    Switch switchNotification;
    @BindView(R.id.img_profile_changePicture)
    ImageView imgChangePicture;
    @BindView(R.id.img_profile_picture)
    ImageView imgProfilePicture;
    private String mCurrentPhotoPath;
    private Unbinder mBinder;
    private View.OnClickListener editButtonListener;
    private DatePickerDialog datePickerDialog;
    private String calenderDateForAPI = "";
    private Snackbar snackbar;
    private View rootView;
    private TitleBar titleBar;
    LoginResult userData;


    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(ProfileUpdateListener _profileUpdateListener) {
        profileUpdateListener = _profileUpdateListener;
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        mBinder = ButterKnife.bind(this, rootView);

        datePickerDialog = new DatePickerDialog(
                getDockActivity(), ProfileFragment.this, 2017, 0, 1);
        setUserData();
        lockViews();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListener();
//        setUserData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    private void lockViews() {
        etFullName.setEnabled(false);
        etPhone.setEnabled(false);
        tvDOB.setOnClickListener(null);
        tvChangePIN.setOnClickListener(null);
        tvNotification.setOnClickListener(null);
        imgChangePicture.setOnClickListener(null);

        switchNotification.setEnabled(false);
        if (titleBar != null)
            titleBar.setRightButton(R.drawable.ic_edit, editButtonListener);
        fullNamelock();

    }

    private void unlockViews() {
        etFullName.setEnabled(true);
        etPhone.setEnabled(true);
        tvDOB.setOnClickListener(this);
        tvChangePIN.setOnClickListener(this);
        tvNotification.setOnClickListener(this);
        imgChangePicture.setOnClickListener(this);

        switchNotification.setEnabled(true);

        if (titleBar != null)
            titleBar.setRightButton(R.drawable.tick, editButtonListener);
        etFullName.setText(userData.getFullName());
    }

    private void setListener() {
        editButtonListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkLoading())
                    if (etFullName.isEnabled() && fieldsValidated()) {
                        String vCode = prefHelper.getUser().getVerificationCode();
                        String userId = String.valueOf(prefHelper.getUser().getId());
                        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity())) {
                            if (isValidate()) {
                                updateProfile(etFullName.getText().toString(), etEmail.getText().toString(),
                                        etPhone.getText().toString(), calenderDateForAPI,
                                        vCode, switchNotification.isChecked() ? "1" : "0", userId, fileUrl);
//                            lockViews();
                            }
                        }
                    } else {
                        unlockViews();
                    }
            }
        };
    }

    private boolean isValidate() {
        if (etFullName.testValidity() && etEmail.testValidity()) {
            if (checkPhoneLength()) {
                return true;
            } else
                etPhone.setError("Please Enter valid phone number");

        }// && checkPhoneLength());
        return false;
    }

    private boolean checkPhoneLength() {
        return (etPhone.length() > 7 || etPhone.length() < 1);
    }

    private void setUserData() {
        userData = prefHelper.getUser();
        if (userData == null) return;

        ImageLoaderHelper.loadImage(userData.getProfilePicture(), imgProfilePicture);

        if (userData.getFullName() != null)
            etFullName.setText(userData.getFullName());
        if (userData.getEmail() != null)
            etEmail.setText(userData.getEmail());
        if (userData.getPhoneNo() != null)
            etPhone.setText(userData.getPhoneNo());
        if (userData.getDob() != null) {
            calenderDateForAPI = userData.getDob();
            tvDOB.setText(DateHelper.dateFormatGMT(userData.getDob(), "dd-MM-yy", AppConstants.DATE_FORMAT_DATE));
        }

        if (userData.getNotificationStatus() != null) {
            if (userData.getNotificationStatus().equals("1")) {
                switchNotification.setChecked(true);
            }
        }
    }

    private boolean fieldsValidated() {
        return (etFullName.testValidity() && etEmail.testValidity());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_profile_dob:
                if (checkLoading())
                    showWheelPicker(getDockActivity());
//                if (!datePickerDialog.isShowing()) {
//                    datePickerDialog.show();
//                }
                break;
            case R.id.tv_profile_changePin:
                if (checkLoading())
                    getDockActivity().replaceDockableFragment(ChangePinFragment.newInstance(), ChangePinFragment.class.getSimpleName());
                break;
            case R.id.img_profile_changePicture:
                if (checkLoading())
                    ProfileFragmentPermissionsDispatcher.getStoragePermissionWithCheck(ProfileFragment.this);
                break;
            case R.id.tv_profile_notification:
                if (checkLoading())
                    if (switchNotification != null) {
                        if (switchNotification.isChecked()) {
                            switchNotification.setChecked(false);
                        } else switchNotification.setChecked(true);
                    }
                break;
            default:
                break;
        }
    }


    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        this.titleBar = titleBar;
        titleBar.hideButtons();
        titleBar.showBackButton();
        if (!etFullName.isEnabled()) {
            titleBar.setRightButton(R.drawable.ic_edit, editButtonListener);
            fullNamelock();
        } else {
            titleBar.setRightButton(R.drawable.tick, editButtonListener);
            etFullName.setText(userData.getFullName());
        }
        titleBar.setSubHeading(getResources().getString(R.string.profile));
    }

    private void fullNamelock() {
        String name = userData.getFullName() + " (Public)";
        SpannableString spannable = new SpannableString(name);
//             here we set the color
        spannable.setSpan(new ForegroundColorSpan(Color.GRAY), userData.getFullName().length(), name.length(), 0);
        etFullName.setText(spannable);
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }


    private void showWheelPicker(final Context context) {

        Dialog dialog = null;
        if (dialog == null) {
            dialog = new Dialog(context, R.style.DialogCustomTheme);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.layout_date_wheel_picker);

            Calendar calendar = Calendar.getInstance();
            final WheelView day = dialog.findViewById(R.id.wheel_day);
            final WheelView month = dialog.findViewById(R.id.wheel_month);
            final WheelView year = dialog.findViewById(R.id.wheel_year);
            final Button btnOk = dialog.findViewById(R.id.wheel_ok);

            final Dialog finalDialog = dialog;
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finalDialog.dismiss();
                    String sYear, sMonth, sDay;
                    sYear = (String) year.getViewAdapter().getItemText(year.getCurrentItem());
                    sMonth = String.valueOf(month.getCurrentItem() + 1);
                    sDay = (String) day.getViewAdapter().getItemText(day.getCurrentItem());
//
//                    UIHelper.showShortToastInCenter(getDockActivity(), mDay
//                            +"-"+mMonth
//                            +"-"+mYear);

                    //DD MM YY
                    if (!DateHelper.isFutureDate(Util.getParsedInteger(sYear)
                            , Util.getParsedInteger(sMonth)
                            , Util.getParsedInteger(sDay))) {

                        calenderDateForAPI = sYear + "-" + sMonth + "-" + sDay;

//                        tvDD.setText(Util.getFormedInteger(Util.getParsedInteger(sDay)));
//                        tvMM.setText(Util.getFormedInteger(Util.getParsedInteger(sMonth)));

//                        tvYY.setText(Util.getParsedInteger(sYear) % 100 + "");.

                        //DD MM YY
                        String dateToShow = Util.getFormedInteger(Util.getParsedInteger(sDay))
                                + "-" + Util.getFormedInteger(Util.getParsedInteger(sMonth))
                                + "-" + (Util.getParsedInteger(sYear) % 100);
                        tvDOB.setText(dateToShow);
                    } else
                        UIHelper.showShortToastInCenter(getDockActivity(), "Please enter valid date.");


//                    getDockActivity().replaceDockableFragment(LoginFragment.newInstance(), "LoginFragment");

                }
            });

            OnWheelChangedListener listener = new OnWheelChangedListener() {
                public void onChanged(WheelView wheel, int oldValue, int newValue) {
                    updateDays(year, month, day);
                }
            };

            // month
            int curMonth = calendar.get(Calendar.MONTH);
            String months[] = new String[]{"January", "February", "March", "April", "May",
                    "June", "July", "August", "September", "October", "November", "December"};
            month.setViewAdapter(new DateArrayAdapter(getDockActivity(), months, curMonth));
            month.setCurrentItem(curMonth);
            month.addChangingListener(listener);

            // year
            int curYear = calendar.get(Calendar.YEAR);

            year.setViewAdapter(new DateNumericAdapter(getDockActivity(), curYear - 50, curYear, 50));
            year.setCurrentItem(50);
            year.addChangingListener(listener);

            //day
            updateDays(year, month, day);
            day.setCurrentItem(calendar.get(Calendar.DAY_OF_MONTH) - 1);
        }

        dialog.show();
    }

    /**
     * Updates day wheel. Sets max days according to selected month and year
     */
    void updateDays(WheelView year, WheelView month, WheelView day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + year.getCurrentItem());
        calendar.set(Calendar.MONTH, month.getCurrentItem());

        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        day.setViewAdapter(new DateNumericAdapter(getDockActivity(),
                1, maxDays, calendar.get(Calendar.DAY_OF_MONTH) - 1));
        int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
        day.setCurrentItem(curDay - 1, true);
    }


    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
//        String date = String.valueOf(dayOfMonth) + "-" + String.valueOf(month) + "-" + String.valueOf(year % 100);
//        tvDOB.setText(date);

        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        // and get that as a Date
        Date dateSpecified = c.getTime();
        if (dateSpecified.after(date)) {
            UIHelper.showShortToastInCenter(getDockActivity(), "Please enter valid date.");
            return;
        }


        month++;
        String sYear, sMonth, sDay;
        sYear = String.format(Locale.US, "%d", year);
        sMonth = String.format(Locale.US, "%02d", month);
        sDay = String.format(Locale.US, "%02d", dayOfMonth);

        calenderDateForAPI = sYear + "-" + sMonth + "-" + sDay;

        //DD MM YY
        String dateToShow = String.format(Locale.US, "%s %s %02d", sDay, sMonth, ((year) % 100));
        tvDOB.setText(dateToShow);
    }

    @RestAPI
    private void updateProfile(String _fullName, String _email, String _phone_no, String _dob,
                               String _verification_code, String _notification_status, String _user_id, final File fileUrl) {
        loadingStarted();
        MultipartBody.Part body = null;
        if (fileUrl != null) {
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"),
                            fileUrl
                    );
            //to send also the actual file name
            body = MultipartBody.Part.createFormData("profile_picture", fileUrl.getName(), requestFile);
        }

        RequestBody fullName = RequestBody.create(MediaType.parse("text/plain"), _fullName);
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), _email);
        RequestBody phone_no = RequestBody.create(MediaType.parse("text/plain"), _phone_no);

        RequestBody dob = null;
        if (Util.isNotNullEmpty(_dob))
            dob = RequestBody.create(MediaType.parse("text/plain"), _dob);

        RequestBody verification_code = RequestBody.create(MediaType.parse("text/plain"), _verification_code);
        RequestBody notification_status = RequestBody.create(MediaType.parse("text/plain"), _notification_status);
        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), _user_id);
        webService.updateProfile(fullName, email, phone_no, dob, verification_code,
                notification_status, body, user_id)
                .enqueue(new Callback<ResponseWrapper<LoginResult>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<LoginResult>> call, Response<ResponseWrapper<LoginResult>> response) {
                        loadingFinished();
                        if (ProfileFragment.this.isDetached() || ProfileFragment.this.isRemoving()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.details_updated_success));

                            //update old selectedCity by user
                            String selectedCity = prefHelper.getUser().getSelectedCity();
                            LoginResult newObject = response.body().getResult();
                            newObject.setSelectedCity(selectedCity);
                            prefHelper.putUser(newObject);
                            userData = newObject;
                            lockViews();

                            if (profileUpdateListener != null)
                                profileUpdateListener.profileUpdate();
                            ProfileFragment.this.fileUrl = null;
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<LoginResult>> call, Throwable t) {
                        loadingFinished();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    public void uploadFromCamera() {
        Intent pictureActionIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        Uri photoURI;
        try {
            photoURI = FileProvider.getUriForFile(getDockActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    createImageFile());
            if (photoURI != null) {
                pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                ProfileFragment.this.startActivityForResult(pictureActionIntent, CAMERA_REQUEST);
            }
        } catch (IOException e) {
            UIHelper.showShortToastInCenter(getDockActivity(), getString(R.string.failure_response));
            e.printStackTrace();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Dining&Nightlife");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fileUrl = CameraHelper.retrieveAndDisplayPicture(requestCode, resultCode, data, getDockActivity(),
                imgProfilePicture, mCurrentPhotoPath);
//        if (fileUrl == null) {
//            UIHelper.showLongToastInCenter(getDockActivity(), "null file");
//        }
    }

    /**
     * Permission Dispatcher work
     */
    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void getStoragePermission() {
        CameraHelper.uploadPhotoDialog(ProfileFragment.this, getDockActivity());
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showRationaleForStorage(final PermissionRequest request) {
        new AlertDialog.Builder(getDockActivity())
                .setMessage(R.string.permission_storage_rationale)
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForStorage() {
        showSnackBar(getResources().getString(R.string.permission_storage_denied));
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showNeverAskForStorage() {
        showSnackBar(getResources().getString(R.string.permission_storage_neverask));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        ProfileFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void showSnackBar(String msg) {
        snackbar = Snackbar.make(rootView, msg, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Settings", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startInstalledAppDetailsActivity(getDockActivity());
            }
        });
        snackbar.show();
    }

    private void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
}