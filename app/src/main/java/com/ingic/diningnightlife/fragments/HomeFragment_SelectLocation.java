package com.ingic.diningnightlife.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.LoginResult;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by developer on 7/26/17.
 */

public class HomeFragment_SelectLocation extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.img_selectLocation_AbuDhabi)
    ImageView imgAbuDhabi;
    @BindView(R.id.img_selectLocation_Dubai)
    ImageView imgDubai;
    private Unbinder mBinder;
    //    @BindView(R.id.rl_selectLocation_enter)
//    RelativeLayout rlBtnEnter;

    public HomeFragment_SelectLocation() {
    }

    public static HomeFragment_SelectLocation newInstance() {
        return new HomeFragment_SelectLocation();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().releaseDrawer();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home_select_location, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        setListener();
        return rootView;
    }

    private void setListener() {
        imgAbuDhabi.setOnClickListener(this);
        imgDubai.setOnClickListener(this);
//        rlBtnEnter.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showMenuButton();
        titleBar.setSubHeading(getResources().getString(R.string.home));

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_selectLocation_Dubai:
//                updateSelectedCity("1");
                UIHelper.showShortToast(getDockActivity(), "Coming soon!");
                break;
            case R.id.img_selectLocation_AbuDhabi:
                updateSelectedCity("2");
                break;
            default:
                break;
        }
    }

    private void updateSelectedCity(String val) {
        LoginResult userData = prefHelper.getUser();
        userData.setSelectedCity(val);
        prefHelper.putUser(userData);
        if (AppConstants.FETCH_LOCATION)
            AppConstants.FETCH_LOCATION = false;
        getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false), HomeFragment.class.getSimpleName());
    }

}