package com.ingic.diningnightlife.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.helpers.Funcs;
import com.ingic.diningnightlife.helpers.GPSHelper;
import com.ingic.diningnightlife.helpers.MapRoutes;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.RouteFailedListener;
import com.ingic.diningnightlife.ui.dialogs.DialogFactory;
import com.ingic.diningnightlife.ui.views.AnyEditTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;
import com.jota.autocompletelocation.AutoCompleteLocation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

@RuntimePermissions
public class DirectionMapFragment extends BaseFragment implements
        OnMapReadyCallback,
        GoogleMap.OnMapLoadedCallback, RouteFailedListener,
        AutoCompleteLocation.AutoCompleteLocationListener {

    private static final String TAG = DirectionMapFragment.class.getSimpleName();
    private static final long DELAY_FOR_LATE_GPS_RESPONSE = 3000;
    @BindView(R.id.et_direction_searchBar)
    AnyEditTextView etSearch;
    @BindView(R.id.autocomplete_location)
    AutoCompleteLocation autoCompleteLocation;
    private LatLng targetLatLng = new LatLng(0, 0);
    private LatLng selectedLatLng = null;
    private GoogleMap googleMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private Unbinder mBinder;
    private boolean isMapLoadComplete;
    private View rootView;
    private Snackbar snackbar, snackbarRoute;

    public DirectionMapFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_direction_map, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        if (getArguments() != null) {
            String lat = getArguments().getString(AppConstants.BUNDLE_LAT, null);
            String lng = getArguments().getString(AppConstants.BUNDLE_LNG, null);
            if (lat != null && lng != null)
                targetLatLng = new LatLng(Util.getParsedDouble(lat), Util.getParsedDouble(lng));
        }
        if (targetLatLng.latitude == 0) {
            DialogFactory.createMessageDialog(getDockActivity(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    getDockActivity().popFragment();
                }
            }, "Restaurant location is not available", "Location Error").show();
        } else {
            initMap();
        }
        initSearchListener();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initSearchListener() {
        autoCompleteLocation.setAutoCompleteTextListener(this);
//        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if ((actionId == EditorInfo.IME_ACTION_SEARCH)) {
//                    getDockActivity().notImplemented();
//                    etSearch.setText("");//clear text
//                }
//                return false;
//            }
//        });
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.googleMap);
        mapFragment.getMapAsync(this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getDockActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    @Override
    public void onStart() {
        super.onStart();
        //get Location Permission
        if (targetLatLng.latitude != 0)
            DirectionMapFragmentPermissionsDispatcher.showLocationWithCheck(DirectionMapFragment.this);
    }

    private void getLocation(LatLng latLng) {
        if (mFusedLocationClient != null && latLng == null) {
            selectedLatLng = null;
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getDockActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                Log.e(TAG, "onSuccess: lat->" + location.getLatitude());
                                updateMap(location.getLatitude(), location.getLongitude());
                            } else {
                                UIHelper.showShortToastInCenter(getDockActivity(), "Location is Null");
                                Log.e(TAG, "location = NULL");
                            }
                        }
                    });
        } else if (latLng != null) {
            updateMap(latLng.latitude, latLng.longitude);
        } else if (DirectionMapFragment.this.isVisible()) {
            UIHelper.showShortToastInCenter(getDockActivity(), getString(R.string.failure_response));
            //
        }
    }

    private void updateMap(double latitude, double longitude) {
        if (googleMap == null) return;
        googleMap.clear();

        LatLng currentLatLng = new LatLng(latitude, longitude);
        googleMap.addMarker(new MarkerOptions()
                .position(currentLatLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin)));
        googleMap.addMarker(new MarkerOptions()
                .position(targetLatLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin)));

        // Move the camera instantly to current Location with a zoom of 15.
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15));

        // Zoom in, animating the camera.
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());

        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);

        // Construct a CameraPosition focusing on Mountain View and animate the camera to that position.
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(targetLatLng)      // Sets the center of the map to Mountain View
                .zoom(17)                   // Sets the zoom
                .bearing(90)                // Sets the orientation of the camera to east
                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder

        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        builder.include(currentLatLng);
//        builder.include(targetLatLng);
//        LatLngBounds bound = builder.build();
//
//        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bound, (int) Util.convertDpToPixel(64, getDockActivity())));

        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);

        if (Funcs.checkInternetConnection(getDockActivity())) {
            //show Routes
            MapRoutes routes = new MapRoutes(googleMap, this, DirectionMapFragment.this);
            routes.showRoute(currentLatLng, targetLatLng);
        }

        isMapLoadComplete = true;
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.directions));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
        isMapLoadComplete = false;
        googleMap = null;
        mFusedLocationClient = null;

        if (snackbar != null && snackbar.isShownOrQueued()) snackbar.dismiss();
        if (snackbarRoute != null && snackbarRoute.isShownOrQueued()) snackbarRoute.dismiss();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapLoadedCallback(this);
    }

    @Override
    public void onMapLoaded() {

    }

    @Override
    public void onRouteCreationFailed() {
//        DialogFactory.createMessageDialog(getDockActivity(), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                getLocation(null);
//            }
//        }, "Map route creation failed, Please try again", "Map Route Failed").show();
        showRouteErrorSnackBar(getResources().getString(R.string.map_route_failed));
    }

    /**
     * Permission Dispatcher work
     */
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    void showLocation() {
        if (!isMapLoadComplete)
            if (GPSHelper.isGPSEnabled(getDockActivity())) {
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getLocation(null);
                    }
                }, DELAY_FOR_LATE_GPS_RESPONSE);
            } else
                GPSHelper.showGPSDisabledAlertToUser(getDockActivity());
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForLocation(final PermissionRequest request) {
        new AlertDialog.Builder(getDockActivity())
                .setMessage(R.string.permission_location_rationale)
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForLocation() {
        showSettingsSnackBar(getResources().getString(R.string.permission_location_denied));
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    void showNeverAskForLocation() {
        showSettingsSnackBar(getResources().getString(R.string.permission_location_neverask));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        DirectionMapFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void showSettingsSnackBar(String msg) {
        snackbar = Snackbar.make(rootView, msg, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Settings", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startInstalledAppDetailsActivity(getDockActivity());
            }
        });
        snackbar.show();
    }

    private void showRouteErrorSnackBar(String msg) {
        snackbarRoute = Snackbar.make(rootView, msg, Snackbar.LENGTH_INDEFINITE);
        snackbarRoute.setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation(selectedLatLng);
            }
        });
        snackbarRoute.show();
    }

    private void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    @Override
    public void onTextClear() {

    }

    @Override
    public void onItemSelected(Place selectedPlace) {
        selectedLatLng = selectedPlace.getLatLng();
        getLocation(selectedPlace.getLatLng());
    }
}