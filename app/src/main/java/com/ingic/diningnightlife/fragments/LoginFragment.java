package com.ingic.diningnightlife.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.RelativeLayout;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.LoginResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.DateArrayAdapter;
import com.ingic.diningnightlife.helpers.DateHelper;
import com.ingic.diningnightlife.helpers.DateNumericAdapter;
import com.ingic.diningnightlife.helpers.FacebookLoginHelper;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.ProfileUpdateListener;
import com.ingic.diningnightlife.ui.views.AnyEditTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;
import com.ingic.diningnightlife.wheel.OnWheelChangedListener;
import com.ingic.diningnightlife.wheel.WheelView;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends BaseFragment implements OnClickListener,
        DatePickerDialog.OnDateSetListener, ProfileUpdateListener,
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private static final String TAG = LoginFragment.class.getSimpleName();
    static ProfileUpdateListener profileUpdateListener;
    @BindView(R.id.et_login_fullName)
    AnyEditTextView etFullName;
    @BindView(R.id.et_login_email)
    AnyEditTextView etEmail;
    @BindView(R.id.et_login_phone)
    AnyEditTextView etPhone;
    @BindView(R.id.et_login_calender)
    AnyEditTextView etCalender;
    @BindView(R.id.rl_login_newUser)
    RelativeLayout rlNewUser;
    @BindView(R.id.rl_login_fb)
    RelativeLayout rlFbLogin;
    @BindView(R.id.rl_login_userLogin)
    RelativeLayout rlUserLogin;
    @BindView(R.id.cb_login_dontSendUpdates)
    CheckBox cbSendUpdates;
    @BindView(R.id.mFBLibraryLoginButton)
    LoginButton mFBLibraryLoginButton;
    @BindView(R.id.rl_login_google)
    RelativeLayout rlLoginGoogle;
    @BindView(R.id.btn_sign_in)
    SignInButton btnSignIn;
    Unbinder unbinder;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private String calenderDateForAPI = null;
    public static final int RC_SIGN_IN = 007;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileUpdateListener = this;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListeners();
        setFacebookLogin();
        if (mGoogleApiClient == null)
            initGooglePlus();
    }

    private void initGooglePlus() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestId()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getDockActivity())
                .enableAutoManage(getDockActivity(), this)
                .addConnectionCallbacks(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    private void setListeners() {
        rlNewUser.setOnClickListener(this);
        rlFbLogin.setOnClickListener(this);
        rlUserLogin.setOnClickListener(this);
        etCalender.setOnClickListener(this);

//        etPhone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
//                    etCalender.performClick();
//                }
//                return false;
//            }
//        });
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.showTitleBar();
        titleBar.hideButtons();
        titleBar.setSubHeading(getDockActivity().getResources().getString(R.string.sign_in));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onClick(View v) {
        if (checkLoading())
            switch (v.getId()) {
                case R.id.rl_login_newUser:
                    if (isValidate()) {
                        prefHelper.setIsUserSignUp(true);
                        //new user
                        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
                            registration(etFullName.getText().toString(), etEmail.getText().toString(),
                                    etPhone.getText().toString(),
                                    calenderDateForAPI, "", "", refreshedToken,
                                    cbSendUpdates.isChecked() ? 1 : 0);
                    }
                    break;
                case R.id.rl_login_userLogin:
                    //existing user
                    prefHelper.setIsUserSignUp(false);
                    launchEnterPinFragment(null, AppConstants.BUNDLE_EXITING_USER_LOGIN);

                    break;
                case R.id.rl_login_fb:
                    //facebook login
                    if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity())) {
                        LoginManager.getInstance().logOut();
                        mFBLibraryLoginButton.performClick();
                    }
                    break;
                case R.id.et_login_calender:
//                showDatePicker();
                    showWheelPicker(getDockActivity());
                    break;
                default:
                    break;
            }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    private boolean isValidate() {
        if (etFullName.testValidity() && etEmail.testValidity()) {
            if (checkPhoneLength()) {
                return true;
            } else
                etPhone.setError("Please Enter valid phone number"
                );

        }// && checkPhoneLength());
        return false;
    }

    private boolean checkPhoneLength() {
        //minimum 8
        return (etPhone.length() > 7 || etPhone.length() < 1);
    }

    @RestAPI
    private void registration(String fullName, String email, String phone, String dob,
                              final String fbId, String socialMediaType, String token, int state) {
        loadingStarted();
        webService.register(fullName, email, phone, dob, fbId,
                socialMediaType, WebServiceConstants.DEVICE, token, state)
                .enqueue(new Callback<ResponseWrapper<LoginResult>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<LoginResult>> call, Response<ResponseWrapper<LoginResult>> response) {
                        loadingFinished();
                        if (LoginFragment.this.isDetached() || LoginFragment.this.isRemoving()) {
                            //fragment is no longer available here, ignore response
                            return;
                        }

                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            if (fbId.isEmpty()) {
                                //launch enterPin Screen
                                launchEnterPinFragment(response.body().getResult(), AppConstants.BUNDLE_NEW_USER_SIGN_UP);
                            } else {
                                //incase Facebook login, launch home here
//                                launchEnterPinFragment(response.body().getResult(), AppConstants.BUNDLE_NEW_USER_SIGN_UP);
//                                launchHomeFragment(response.body().getResult());
                            }
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<LoginResult>> call, Throwable t) {
                        loadingFinished();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    private void setFacebookLogin() {
        try {
            mFBLibraryLoginButton.setFragment(this);
            mFBLibraryLoginButton.setReadPermissions(Arrays.asList("public_profile,email"));
            callbackManager = CallbackManager.Factory.create();
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookLoginHelper(getDockActivity(), this) {
                @Override
                public void getBundle(Bundle bFacebookData) {
                    if (bFacebookData != null) {
                        String email = bFacebookData.getString("email");
                        String fullName = bFacebookData.getString("first_name") + " " + bFacebookData.getString("last_name");
                        String fbId = bFacebookData.getString("idFacebook");

                        if (email == null || email.isEmpty()) {
                            //user did not provided his email address
                            etFullName.setText(fullName);
                            etEmail.testValidity();
                            UIHelper.showLongToastInCenter(getDockActivity(), "Email address is required");
                        } else {
                            //here fill form only
                            etFullName.setText(fullName);
                            etEmail.setText(email);
//                            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//                            registration(fullName, email, null, null, fbId, WebServiceConstants.FACEBOOK, refreshedToken,
//                                    cbSendUpdates.isChecked() ? 1 : 0);
                        }
                    } else {
                        UIHelper.showShortToastInCenter(getDockActivity(), "null bundle");
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
//        }
    }

    private void launchEnterPinFragment(LoginResult result, String userType) {
        EnterPinFragment fragment = new EnterPinFragment();
        Bundle bundle = new Bundle();
        if (result != null) {
            bundle.putString(AppConstants.BUNDLE_LOGIN_RESULT, new Gson().toJson(result));
        }

        bundle.putInt(AppConstants.BUNDLE_EMAIL_UPDATE_STATE, cbSendUpdates.isChecked() ? 1 : 0);
        bundle.putString(AppConstants.BUNDLE_LOGIN_USER_TYPE, userType);
        fragment.setArguments(bundle);


        getDockActivity().replaceDockableFragment(fragment, EnterPinFragment.class.getSimpleName());
    }

//    private void launchHomeFragment(LoginResult result) {
//        if (result == null) {
//            UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
//            return;
//        }
//        prefHelper.putUser(result);
//        prefHelper.setLoginStatus(true);
//        sendBroadCast();
////        if (profileUpdateListener != null)
////            profileUpdateListener.profileUpdate();
//        getDockActivity().popBackStackTillEntry(0);
//        if (prefHelper.getUser() != null && prefHelper.getUser().getIsSubscribe() == AppConstants.YES)
//            getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(), HomeFragment_SelectLocation.class.getSimpleName());
//        else
//            getDockActivity().replaceDockableFragment(SubscriptionFragment.newInstance(true), SubscriptionFragment.class.getSimpleName());
//    }

    private void showWheelPicker(final Context context) {
        Dialog dialog = null;
        if (dialog == null) {
            dialog = new Dialog(context, R.style.DialogCustomTheme);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.layout_date_wheel_picker);

            Calendar calendar = Calendar.getInstance();
            final WheelView day = dialog.findViewById(R.id.wheel_day);
            final WheelView month = dialog.findViewById(R.id.wheel_month);
            final WheelView year = dialog.findViewById(R.id.wheel_year);
            final Button btnOk = dialog.findViewById(R.id.wheel_ok);

            final Dialog finalDialog = dialog;
            btnOk.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    finalDialog.dismiss();
                    String sYear, sMonth, sDay;
                    sYear = (String) year.getViewAdapter().getItemText(year.getCurrentItem());
                    sMonth = String.valueOf(month.getCurrentItem() + 1);
                    sDay = (String) day.getViewAdapter().getItemText(day.getCurrentItem());
//
//                    UIHelper.showShortToastInCenter(getDockActivity(), mDay
//                            +"-"+mMonth
//                            +"-"+mYear);

                    //DD MM YY
                    if (!DateHelper.isFutureDate(Util.getParsedInteger(sYear)
                            , Util.getParsedInteger(sMonth)
                            , Util.getParsedInteger(sDay))) {

                        calenderDateForAPI = sYear + "-" + sMonth + "-" + sDay;

//                        tvDD.setText(Util.getFormedInteger(Util.getParsedInteger(sDay)));
//                        tvMM.setText(Util.getFormedInteger(Util.getParsedInteger(sMonth)));

//                        tvYY.setText(Util.getParsedInteger(sYear) % 100 + "");.

                        //DD MM YY
                        String dateToShow = Util.getFormedInteger(Util.getParsedInteger(sDay))
                                + "-" + Util.getFormedInteger(Util.getParsedInteger(sMonth))
                                + "-" + (Util.getParsedInteger(sYear) % 100);
                        etCalender.setText(dateToShow);
                    } else
                        UIHelper.showShortToastInCenter(getDockActivity(), "Please enter valid date.");


//                    getDockActivity().replaceDockableFragment(LoginFragment.newInstance(), "LoginFragment");

                }
            });

            OnWheelChangedListener listener = new OnWheelChangedListener() {
                public void onChanged(WheelView wheel, int oldValue, int newValue) {
                    updateDays(year, month, day);
                }
            };

            // month
            int curMonth = calendar.get(Calendar.MONTH);
            String months[] = new String[]{"January", "February", "March", "April", "May",
                    "June", "July", "August", "September", "October", "November", "December"};
            month.setViewAdapter(new DateArrayAdapter(getDockActivity(), months, curMonth));
            month.setCurrentItem(curMonth);
            month.addChangingListener(listener);

            // year
            int curYear = calendar.get(Calendar.YEAR);

            year.setViewAdapter(new DateNumericAdapter(getDockActivity(), curYear - 50, curYear, 50));
            year.setCurrentItem(50);
            year.addChangingListener(listener);

            //day
            updateDays(year, month, day);
            day.setCurrentItem(calendar.get(Calendar.DAY_OF_MONTH) - 1);
        }

        dialog.show();
    }

    /**
     * Updates day wheel. Sets max days according to selected month and year
     */
    void updateDays(WheelView year, WheelView month, WheelView day) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + year.getCurrentItem());
        calendar.set(Calendar.MONTH, month.getCurrentItem());

        int maxDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        day.setViewAdapter(new DateNumericAdapter(getDockActivity(),
                1, maxDays, calendar.get(Calendar.DAY_OF_MONTH) - 1));
        int curDay = Math.min(maxDays, day.getCurrentItem() + 1);
        day.setCurrentItem(curDay - 1, true);
    }

    private void showDatePicker() {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(getDockActivity(), this, year, month, day).show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, day);

        // and get that as a Date
        Date dateSpecified = c.getTime();
        if (dateSpecified.after(date)) {
            UIHelper.showShortToastInCenter(getDockActivity(), "Please enter valid date.");
            return;
        }


        month++;
        String sYear, sMonth, sDay;
        sYear = String.format(Locale.US, "%d", year);
        sMonth = String.format(Locale.US, "%02d", month);
        sDay = String.format(Locale.US, "%02d", day);

        calenderDateForAPI = sYear + "-" + sMonth + "-" + sDay;

        //DD MM YY
        String dateToShow = String.format(Locale.US, "%s %s %02d", sDay, sMonth, ((year) % 100));
        etCalender.setText(dateToShow);
    }

    @Override
    public void profileUpdate() {
        //ignore this
    }

    private void sendBroadCast() {
        Intent intent = new Intent(AppConstants.SIDE_MENU_UPDATOR);
//        LocalBroadcastManager.getInstance(getDockActivity()).sendBroadcast(intent);
        getDockActivity().sendBroadcast(intent);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        UIHelper.showShortToastInCenter(getDockActivity(), connectionResult.getErrorMessage());
    }

    @OnClick(R.id.rl_login_google)
    public void onViewClicked() {
        if (checkLoading())
            signIn();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Log.e(TAG, "signOut onResult: " + status);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String fullName = acct.getDisplayName();
            String email = acct.getEmail();
            String id = acct.getId();
            Log.e(TAG, "Name: " + fullName + ", email: " + email + ", id: " + id);
            TextViewHelper.setText(etFullName, fullName);
            TextViewHelper.setText(etEmail, email);
//            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//            registration(fullName, email, null, null, id, WebServiceConstants.Google, refreshedToken,
//                    cbSendUpdates.isChecked() ? 1 : 0);
        } else {
            // Signed out, show unauthenticated UI.
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e(TAG, "onConnected: ");
        signOut();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e(TAG, "onConnectionSuspended: ");
    }
}