package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.SimpleDividerItemDecoration;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.retrofit.WebService;
import com.ingic.diningnightlife.retrofit.WebServiceFactory;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.wheel.adapters.AdapterAutoComplete;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AutoCompleteSearchFragment extends BaseFragment implements OnViewHolderClick {


    @BindView(R.id.backImage)
    ImageView backImage;
    @BindView(R.id.searchView)
    EditText searchView;
    Unbinder unbinder;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private RecyclerViewListAdapter<String> adapter;

    public AutoCompleteSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new AdapterAutoComplete(getDockActivity(), this);
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideTitleBar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_auto_complete_search, container, false);
        unbinder = ButterKnife.bind(this, view);
        init();
        return view;
    }

    private void init() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getDockActivity()));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getDockActivity()));
        recyclerView.setAdapter(adapter);

        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 0) {//clear
                    adapter.removeAll();
                } else {
                    getSearchedData(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.backImage)
    public void onViewClicked() {
        getDockActivity().popFragment();
    }

    @Override
    public void onItemClick(View view, int position) {
        //navigate to filter search
        StoreListingFiterableFragment fragment = new StoreListingFiterableFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_SEARCH_RESULT, adapter.getItem(position));
        fragment.setArguments(bundle);
        getDockActivity().replaceDockableFragment(fragment, StoreListingFiterableFragment.class.getSimpleName());
    }

    @RestAPI
    private void getSearchedData(String key) {
        webService.autoCompleteSearch(key).enqueue(new Callback<ResponseWrapper<ArrayList<String>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<String>>> call, Response<ResponseWrapper<ArrayList<String>>> response) {
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    notifyUpdate(response.body().getResult());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<String>>> call, Throwable t) {
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    private void notifyUpdate(ArrayList<String> result) {
        adapter.addAll(result);
    }

}
