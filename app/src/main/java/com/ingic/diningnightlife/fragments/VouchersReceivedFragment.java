package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.ParentEntity;
import com.ingic.diningnightlife.entities.Received;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.interfaces.ChildItemClickListener;
import com.ingic.diningnightlife.ui.adapters.MyVouchersAdapter;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class VouchersReceivedFragment extends Fragment {
    private static ChildItemClickListener listener;
    @BindView(R.id.rv_vouchersReceived)
    RecyclerView rvVouchersReceived;
    Unbinder unbinder;

    public VouchersReceivedFragment() {
        // Required empty public constructor
    }

    public static VouchersReceivedFragment newInstance(ChildItemClickListener childItemClickListener) {
        listener = childItemClickListener;
        return new VouchersReceivedFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vouchers_received, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getArguments() != null) {
            String receivedVouchersString = getArguments().getString(AppConstants.BUNDLE_RECEIVED_VOUCHERS, null);
            if (receivedVouchersString != null) {
                Received obj = new Gson().fromJson(receivedVouchersString, Received.class);
                init(obj);
            }
        }
    }

    private void init(Received obj) {
        rvVouchersReceived.setLayoutManager(new LinearLayoutManager(getActivity()));
        ParentEntity dining = new ParentEntity("Dining Deals", obj.getDinning());
        ParentEntity brunch = new ParentEntity("Brunch Bargains", obj.getBrunch());
        ParentEntity lastMinute = new ParentEntity("Last Minute Offers", obj.getLastMinute());
        ParentEntity exclusive = new ParentEntity("Exclusive Experiences", obj.getExclusive());
        ParentEntity nightlife = new ParentEntity("Nightlife", obj.getNightlife());

        List<ParentEntity> parentEntityList = Arrays.asList(dining, brunch, lastMinute, exclusive, nightlife);

        // RecyclerView has some built in animations to it, using the DefaultItemAnimator.
        // Specifically when you call notifyItemChanged() it does a fade animation for the changing
        // of the data in the ViewHolder. If you would like to disable this you can use the following:
        RecyclerView.ItemAnimator animator = rvVouchersReceived.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        MyVouchersAdapter adapter = new MyVouchersAdapter(getActivity(), parentEntityList, true, listener);
        rvVouchersReceived.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

//    @Override
//    public void onClickListener(SharedVoucher entity) {
//        RedeemVoucherFragment fragment = RedeemVoucherFragment.newInstance();
//        Bundle bundle = new Bundle();
//        bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(entity.getStoreId()));
//        bundle.putString(AppConstants.BUNDLE_STORE_VOUCHER_ID, String.valueOf(entity.getId()));
//        bundle.putString(AppConstants.BUNDLE_VOUCHER_NAME, entity.getVoucher());
//        bundle.putString(AppConstants.BUNDLE_VOUCHER_STORE_TYPE, entity.getType());
//        bundle.putString(AppConstants.BUNDLE_STORE_NAME, entity.getFullName());
//        bundle.putString(AppConstants.BUNDLE_FREE_VOUCHER_ID, String.valueOf(entity.getFree_voucher_id()));
//        fragment.setArguments(bundle);
//        getDockActivity().replaceDockableFragment(fragment,
//                RedeemVoucherFragment.class.getSimpleName());
//    }
}