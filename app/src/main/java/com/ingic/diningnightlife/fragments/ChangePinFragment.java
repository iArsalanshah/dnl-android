package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.LoginResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.ui.views.AnyEditTextView;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePinFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.rl_changePIN_btn)
    RelativeLayout rlBtnChangePIN;
    @BindView(R.id.et_chanePIN_old)
    AnyEditTextView etOld;
    @BindView(R.id.et_chanePIN_new)
    AnyEditTextView etNew;
    @BindView(R.id.et_chanePIN_confirm)
    AnyEditTextView etConfirm;
    @BindView(R.id.container_oldPIN)
    RelativeLayout containerOldPIN;
    @BindView(R.id.tv_changePin)
    AnyTextView tvChangePin;
    private Unbinder mBinder;
    private boolean createPIN;
    private boolean gotoChangeCityScreen;

    public ChangePinFragment() {
        // Required empty public constructor
    }

    public static ChangePinFragment newInstance() {
        return new ChangePinFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_change_pin, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        if (getArguments() != null) {
            createPIN = getArguments().getBoolean(AppConstants.BUNDLE_CREATE_NEW_PIN, false);
        }
        if (createPIN) {
            tvChangePin.setText(getResources().getString(R.string.create_pin));
            etOld.setText(prefHelper.getUser().getVerificationCode());
            containerOldPIN.setVisibility(View.GONE);
        }
        setListener();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
        if (gotoChangeCityScreen) {
            navigateToHomeOrChangeCity();
        }
    }

    private void setListener() {
        rlBtnChangePIN.setOnClickListener(this);
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        if (createPIN) {
            titleBar.showSkipText(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkLoading())
                        navigateToHomeOrChangeCity();
                }
            });
            titleBar.setSubHeading(getResources().getString(R.string.create_pin));
        } else {
            titleBar.showBackButton();
            titleBar.setSubHeading(getResources().getString(R.string.change_pin));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @Override
    public void onClick(View view) {
        if (checkLoading())
            if (view.getId() == R.id.rl_changePIN_btn) {
                if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()) && checkLoading())
                    if (createPIN && validate() && isMatched()) {
                        changePIN(etOld.getText().toString(), etNew.getText().toString());
                    } else if (validate() && isMatched())
                        changePIN(etOld.getText().toString(), etNew.getText().toString());
            }
    }

    private boolean isMatched() {
        if (!(etNew.getText().toString().equals(etConfirm.getText().toString()))) {
            etConfirm.setError(getResources().getString(R.string.error_passwords_do_not_match));
            return false;
        }
        if (etOld.getText().length() < 4 || etNew.getText().length() < 4 || etConfirm.getText().length() < 4) {
            UIHelper.showShortToastInCenter(getDockActivity(), "4 characters required");
            return false;
        }
        return true;
    }

    private boolean validate() {
        return etOld.testValidity() && etNew.testValidity() && etConfirm.testValidity();
    }

    @RestAPI
    private void changePIN(String oldPin, String newPin) {
        loadingStarted();
        webService.changePin(prefHelper.getUser().getId(), oldPin, newPin).enqueue(new Callback<ResponseWrapper<LoginResult>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<LoginResult>> call, Response<ResponseWrapper<LoginResult>> response) {
                if (!ChangePinFragment.this.isVisible()) {
                    //fragment is no longer available
                    return;
                }
                if (response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                    String selectedCity = prefHelper.getUser().getSelectedCity();
                    LoginResult newObject = response.body().getResult();
                    newObject.setSelectedCity(selectedCity);
                    prefHelper.putUser(newObject);
                    if (ChangePinFragment.this.isResumed())
                        navigateToHomeOrChangeCity();
                    else {
                        gotoChangeCityScreen = true;
                    }
//                    if (createPIN) {
//                        if (ChangePinFragment.this.isResumed())
//                            navigateToHomeOrChangeCity();
//                        else {
//                            gotoChangeCityScreen = true;
//                        }
//                    } else {
//                        navigateToHomeOrChangeCity();
//                    }
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
                loadingFinished();
            }

            @Override
            public void onFailure(Call<ResponseWrapper<LoginResult>> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    private void navigateToHomeOrChangeCity() {
        if (createPIN && prefHelper.getIsUserSignUp()) {
            prefHelper.setIsUserSignUp(false);
            getDockActivity().popBackStackTillEntry(0);
            getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(),
                    HomeFragment_SelectLocation.class.getSimpleName());
        } else {
//            etOld.setText("");
//            etNew.setText("");
//            etConfirm.setText("");
            getDockActivity().popFragment();
        }


        //TODO Abu Dhabi Only
//        if (prefHelper.getIsUserSignUp()) {
//            prefHelper.setIsUserSignUp(false);
////            getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(),
////                    HomeFragment_SelectLocation.class.getSimpleName());
//            getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false),
//                    HomeFragment.class.getSimpleName());
//        } else {
//            getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false),
//                    HomeFragment.class.getSimpleName());
//        }
    }

}
