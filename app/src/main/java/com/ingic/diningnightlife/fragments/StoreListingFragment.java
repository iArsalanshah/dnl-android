package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.AllStore;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.SimpleDividerItemDecoration;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.OnOffersClick;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.AutocompleteSearchAdapter;
import com.ingic.diningnightlife.ui.adapters.HomeOffersAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class StoreListingFragment extends BaseFragment implements OnViewHolderClick, OnOffersClick {
    @BindView(R.id.rv_diningDeals)
    RecyclerView rvDiningDeals;
    @BindView(R.id.tv_diningDeals_Title)
    AnyTextView tvTitle;
    String storeType = "";
    private RecyclerViewListAdapter adapter;
    private Unbinder mBinder;

    public StoreListingFragment() {
        // Required empty public constructor
    }


    public static StoreListingFragment newInstance() {
        return new StoreListingFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_store_listing, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        initRecyclerView();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        boolean isAdvanceSearch = false;
        if (getArguments() != null) {
            String type = getArguments().getString(AppConstants.BUNDLE_STORE_TYPE);
            isAdvanceSearch = getArguments().getBoolean(AppConstants.BUNDLE_IS_ADVANCE_SEARCH, false);
            if (Util.isNotNullEmpty(type))
                storeType = type;
        }
        String title = "";
        switch (storeType) {
            case "dinning":
                title = "Dining Deals";
                break;
            case "brunch":
                title = "Brunch Bargains";
                break;
            case "last_minute":
                title = "Last Minute Offers";
                break;
            case "exclusive":
                title = "Exclusive Experiences";
                break;
            case "nightlife":
                title = "Nightlife";
                break;
            default:
                break;
        }

        TextViewHelper.setText(tvTitle, title);

        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
            if (isAdvanceSearch) {
                String cityId = getArguments().getString(AppConstants.BUNDLE_CITY_ID);
                String cuisine = getArguments().getString(AppConstants.BUNDLE_CUISINE);
                String venue = getArguments().getString(AppConstants.BUNDLE_VENUE);
                String dietary = getArguments().getString(AppConstants.BUNDLE_DIETARY);
                String quickSearch = getArguments().getString(AppConstants.BUNDLE_QUICK_SEARCH);
                int isNowOpen = getArguments().getInt(AppConstants.BUNDLE_QUICK_SEARCH_NOW_OPEN, 0);
//                getAdvanceSearchResult(cityId, storeType, cuisine, venue, dietary, quickSearch, isNowOpen);
            } else if (!storeType.isEmpty()) {
                String cityId = prefHelper.getUser().getSelectedCity();
                getStoresList(cityId, storeType);
            }
    }

    private void initRecyclerView() {
        adapter = new HomeOffersAdapter(getDockActivity(), this, this);
        rvDiningDeals.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.VERTICAL, false));
        rvDiningDeals.addItemDecoration(new SimpleDividerItemDecoration(getDockActivity()));
        rvDiningDeals.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
        String cityVal = prefHelper.getUser().getSelectedCity();
        if (cityVal == null) {
            cityVal = "1";
        }
        String city = cityVal.equals("1") ? "Dubai" : "Abu Dhabi";
        titleBar.hideButtons();
        titleBar.showBackButton();
        final AutocompleteSearchAdapter adapter = new AutocompleteSearchAdapter(getDockActivity(), android.R.layout.simple_dropdown_item_1line, cityVal);
        titleBar.getAutoCompleteTextView().setAdapter(adapter);
//        titleBar.showAutoComplete(cityVal, city, getDockActivity(), new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                AllStore item = adapter.getItem(i);
//                DetailPageFragment fragment = new DetailPageFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(item.getId()));
//                bundle.putString(AppConstants.BUNDLE_STORE_TYPE, "");
//                fragment.setArguments(bundle);
//                getDockActivity().replaceDockableFragment(fragment, DetailPageFragment.class.getSimpleName());
//            }
//        });
        titleBar.setRightButton(R.drawable.ic_search_add, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdvanceSearchFragment fragment = AdvanceSearchFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_STORE_TYPE, storeType);
                fragment.setArguments(bundle);
                getDockActivity().replaceDockableFragment(fragment,
                        AdvanceSearchFragment.class.getSimpleName());
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @RestAPI
    private void getStoresList(String cityId, String storeType) {
        loadingStarted();
        webService.getStoresList(cityId, storeType).enqueue(new Callback<ResponseWrapper<ArrayList<AllStore>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<AllStore>>> call, Response<ResponseWrapper<ArrayList<AllStore>>> response) {
                loadingFinished();
                if (StoreListingFragment.this.isDetached() || StoreListingFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    //home data
                    UpdateUI(response.body().getResult());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<AllStore>>> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    @RestAPI
    private void generalSearch(String cityId, String search) {
        loadingStarted();
        webService.generalSearch(cityId, search).enqueue(new Callback<ResponseWrapper<ArrayList<AllStore>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<AllStore>>> call, Response<ResponseWrapper<ArrayList<AllStore>>> response) {
                loadingFinished();
                if (StoreListingFragment.this.isDetached() || StoreListingFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    //home data
                    UpdateUI(response.body().getResult());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<AllStore>>> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

//    @RestAPI
//    private void getAdvanceSearchResult(String cityId, String storeType, String cuisine, String venue, String dietary, String quickSearch, int isNowOpen) {
//        loadingStarted();
//        webService.advancedSearchResult(cityId, storeType, cuisine, venue, dietary, quickSearch, isNowOpen)
//                .enqueue(new Callback<ResponseWrapper<ArrayList<AllStore>>>() {
//                    @Override
//                    public void onResponse(Call<ResponseWrapper<ArrayList<AllStore>>> call, Response<ResponseWrapper<ArrayList<AllStore>>> response) {
//                        loadingFinished();
//                        if (StoreListingFragment.this.isDetached() || StoreListingFragment.this.isRemoving()) {
//                            //fragment is no longer available
//                            return;
//                        }
//                        if (response == null || response.body() == null || response.body().getResponse() == null) {
//                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
//                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
//                            UpdateUI(response.body().getResult());
//                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
//                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
//                        } else {
//                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResponseWrapper<ArrayList<AllStore>>> call, Throwable t) {
//                        loadingFinished();
//                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
//                    }
//                });
//    }

    private void UpdateUI(ArrayList<AllStore> result) {
        adapter.addAll(result);
    }

    @Override
    public void onOfferItemClicked(int position, int storeId) {
        DetailPageFragment fragment = new DetailPageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(storeId));
        bundle.putString(AppConstants.BUNDLE_STORE_TYPE, storeType);
        fragment.setArguments(bundle);
        getDockActivity().replaceDockableFragment(fragment, DetailPageFragment.class.getSimpleName());
    }
}