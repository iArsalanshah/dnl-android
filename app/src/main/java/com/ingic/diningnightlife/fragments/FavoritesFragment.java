package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.FavoritesListResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.SimpleDividerItemDecoration;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.interfaces.UnFavoriteListener;
import com.ingic.diningnightlife.ui.adapters.FavoritesAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritesFragment extends BaseFragment implements OnViewHolderClick, UnFavoriteListener {
    @BindView(R.id.rv_favorites)
    RecyclerView rvFavorites;
    @BindView(R.id.alternateText)
    AnyTextView alternateText;
    private RecyclerViewListAdapter adapter;
    private Unbinder mBinder;

    public FavoritesFragment() {
        // Required empty public constructor
    }

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_favorites, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        initRecyclerView();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        bindData();
        if (prefHelper.getUser() != null && InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
            getFavoritesList(prefHelper.getUser().getId());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    private void initRecyclerView() {
        adapter = new FavoritesAdapter(getDockActivity(), this, this);
        rvFavorites.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.VERTICAL, false));
        rvFavorites.addItemDecoration(new SimpleDividerItemDecoration(getDockActivity()));
        rvFavorites.setAdapter(adapter);
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.favorites));
    }

    @Override
    public void onItemClick(View view, int position) {
        if (UIHelper.doubleClickCheck()) {
            long itemId = adapter.getItemId(position);
            DetailPageFragment fragment = new DetailPageFragment();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(itemId));
            bundle.putString(AppConstants.BUNDLE_STORE_TYPE, "");
            fragment.setArguments(bundle);
            getDockActivity().replaceDockableFragment(fragment, DetailPageFragment.class.getSimpleName());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @RestAPI
    private void getFavoritesList(int userId) {
        loadingStarted();
        webService.favouriteListing(userId).enqueue(new Callback<ResponseWrapper<ArrayList<FavoritesListResult>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<FavoritesListResult>>> call, Response<ResponseWrapper<ArrayList<FavoritesListResult>>> response) {
                loadingFinished();
                if (FavoritesFragment.this.isDetached() || FavoritesFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    updateUI(response.body().getResult());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<FavoritesListResult>>> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    private void updateUI(ArrayList<FavoritesListResult> result) {
        if (!FavoritesFragment.this.isVisible()) return;

        if (result == null || result.size() == 0) alternateText.setVisibility(View.VISIBLE);
        else {
            alternateText.setVisibility(View.GONE);
            adapter.addAll(result);
        }
    }

    @Override
    public void unFavorite(int position, int storeId) {
        if (UIHelper.doubleClickCheck())
            if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
                UnFavoriteAPI(prefHelper.getUser().getId(), storeId, position);
    }

    @RestAPI
    private void UnFavoriteAPI(int userId, int storeId, final int position) {
        loadingStarted();
        webService.addOrRemoveFavourite(userId, storeId).enqueue(new Callback<ResponseWrapper<FavoritesListResult>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<FavoritesListResult>> call, Response<ResponseWrapper<FavoritesListResult>> response) {
                loadingFinished();
                if (FavoritesFragment.this.isDetached() || FavoritesFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    rvFavorites.postOnAnimation(new Runnable() {
                        @Override
                        public void run() {
                            if (adapter.getList().size() > 0 || adapter.getItem(position) != null) {
                                if (adapter.getList().size() == 1)
                                    alternateText.setVisibility(View.VISIBLE);
                                adapter.removeItem(position, false);
                            }
                        }
                    });
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<FavoritesListResult>> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }
}
