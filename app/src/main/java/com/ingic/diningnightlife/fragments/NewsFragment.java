package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.NewsResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.StoreBanners;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.ui.adapters.ImageSliderAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends BaseFragment implements View.OnClickListener, ViewPager.OnPageChangeListener {
    @BindView(R.id.img_news_image)
    ImageView imgMain;
    @BindView(R.id.img_news_btnNext)
    ImageView imgBtnNext;
    @BindView(R.id.img_news_btnBack)
    ImageView imgBtnBack;
    @BindView(R.id.tv_news)
    TextView tvNews;
    @BindView(R.id.vp_slider)
    ViewPager vpSlider;
    @BindView(R.id.ink_indicator)
    InkPageIndicator inkPageIndicator;
    @BindView(R.id.alternateText)
    AnyTextView alternateText;
    Unbinder unbinder;
    private ImageSliderAdapter sliderAdapter;
    private List<NewsResult> images = new ArrayList<>();
    private int loadedImagePosition;

    public NewsFragment() {
        // Required empty public constructor
    }

    public static NewsFragment newInstance() {
        return new NewsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAdapter();
        setListener();
        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
            getNews();
    }

    private void initAdapter() {
        sliderAdapter = new ImageSliderAdapter(getDockActivity(), null);
        vpSlider.setAdapter(sliderAdapter);
    }

    private void setListener() {
        imgBtnNext.setOnClickListener(this);
        imgBtnBack.setOnClickListener(this);
        vpSlider.addOnPageChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.news));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void loadImage(final int position) {
        if (position < images.size()) {
            vpSlider.setCurrentItem(position);
//            tvNews.setText(images.get(position).getText());
            TextViewHelper.setText(tvNews,images.get(position).getText());
//            imgMain.post(new Runnable() {
//                @Override
//                public void run() {
//                    Animation animation = AnimationUtils.loadAnimation(getDockActivity(), android.R.anim.fade_in);
//                    imgMain.startAnimation(animation);
//                    ImageLoaderHelper.loadImage(images.get(position).getNewsImage(), imgMain);
//                    tvNews.setText(images.get(position).getText());
//                    vpSlider.setCurrentItem(position);
//                }
//            });
        }
    }

    private void getImagePosition(boolean isNext) {
        if (isNext) {
            if (loadedImagePosition < images.size() - 1) {
                loadImage(++loadedImagePosition);
            }
        } else {
            if (loadedImagePosition > 0) {
                loadImage(--loadedImagePosition);
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_news_btnNext:
                getImagePosition(true);
                break;
            case R.id.img_news_btnBack:
                getImagePosition(false);
                break;
            default:
                break;
        }
    }

    @RestAPI
    private void getNews() {
        loadingStarted();
        webService.getNews().enqueue(new Callback<ResponseWrapper<ArrayList<NewsResult>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<NewsResult>>> call, Response<ResponseWrapper<ArrayList<NewsResult>>> response) {
                loadingFinished();
                if (NewsFragment.this.isDetached() || NewsFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    updateUI(response.body().getResult());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<NewsResult>>> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    private void updateUI(ArrayList<NewsResult> result) {
        if (result == null || result.size() == 0) {
            alternateText.setVisibility(View.VISIBLE);
        } else alternateText.setVisibility(View.GONE);

        Collections.reverse(result);
        images = result;
        loadedImagePosition = 0;
        getDockActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadImage(loadedImagePosition);
            }
        });

        List<StoreBanners> imageList = new ArrayList<>();
        for (NewsResult obj :
                result) {
            imageList.add(new StoreBanners(obj.getNewsImage()));
        }

        sliderAdapter.addAll(imageList);
        try {
            inkPageIndicator.setViewPager(vpSlider);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        loadedImagePosition = position;
        TextViewHelper.setText(tvNews,images.get(position).getText());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}