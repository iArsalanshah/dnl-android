package com.ingic.diningnightlife.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.share.widget.ShareDialog;
import com.google.gson.Gson;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.activities.FullScreenSliderActivity;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.DetailPageResult;
import com.ingic.diningnightlife.entities.FavoritesListResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.ReviewResult;
import com.ingic.diningnightlife.entities.ShareVoucherEmailObj;
import com.ingic.diningnightlife.entities.StoreBanners;
import com.ingic.diningnightlife.entities.StoreHour;
import com.ingic.diningnightlife.entities.StoreMenu;
import com.ingic.diningnightlife.entities.StorePhoto;
import com.ingic.diningnightlife.entities.StoreTag;
import com.ingic.diningnightlife.entities.StoreVoucher;
import com.ingic.diningnightlife.entities.SuggestedStore;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.DateHelper;
import com.ingic.diningnightlife.helpers.GridSpacingItemDecoration;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.SelectShareIntent;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.OnRedeemVoucherClicked;
import com.ingic.diningnightlife.interfaces.OnSuggestedClicked;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.interfaces.OnVoucherCheckBoxCL;
import com.ingic.diningnightlife.ui.adapters.DetailsMediaFilesAdapter;
import com.ingic.diningnightlife.ui.adapters.DetailsMenuAdapter;
import com.ingic.diningnightlife.ui.adapters.DetailsRatingsAdapter;
import com.ingic.diningnightlife.ui.adapters.DetailsRedeemVoucherAdapter;
import com.ingic.diningnightlife.ui.adapters.DetailsSuggestedOffersAdapter;
import com.ingic.diningnightlife.ui.adapters.ImageSliderAdapter;
import com.ingic.diningnightlife.ui.adapters.ShareVoucherAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyEditTextView;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.CustomRatingBar;
import com.ingic.diningnightlife.ui.views.FlowLayout;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.pixelcan.inkpageindicator.InkPageIndicator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ingic.diningnightlife.global.AppConstants.INTENT_FULLSCREEN_SLIDER;
import static com.ingic.diningnightlife.global.AppConstants.INTENT_FULLSCREEN_SLIDER_POSITION;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailPageFragment extends BaseFragment implements View.OnClickListener,
        OnViewHolderClick, OnSuggestedClicked, OnRedeemVoucherClicked, OnVoucherCheckBoxCL {
    @BindView(R.id.tv_detailPage_name)
    AnyTextView tvName;
    @BindView(R.id.tv_detailPage_address)
    AnyTextView tvAddress;
    @BindView(R.id.tv_detailPage_status)
    AnyTextView tvStatus;
    @BindView(R.id.tv_detailPage_statusOnTime)
    AnyTextView tvStatusOnTime;
    @BindView(R.id.alternateText_Vouchers)
    AnyTextView alternateTextVouchers;
    @BindView(R.id.alternateText_Reviews)
    AnyTextView alternateTextReviews;
    @BindView(R.id.tv_detailPage_call)
    FrameLayout tvCall;
    @BindView(R.id.tv_detailPage_email)
    FrameLayout tvEmail;
    @BindView(R.id.tv_detailPage_directions)
    FrameLayout tvDirections;
    @BindView(R.id.container_favorite)
    FrameLayout containerFavorite;
    @BindView(R.id.tv_detailPage_favorite)
    AnyTextView tvFavorite;
    @BindView(R.id.tv_detailPage_share)
    FrameLayout tvShare;
    @BindView(R.id.tv_detailPage_openingHours_fridayTime)
    AnyTextView openingHoursFridayTime;
    @BindView(R.id.tv_detailPage_openingHours_saturdayTime)
    AnyTextView openingHoursSaturdayTime;
    @BindView(R.id.tv_detailPage_openingHours_sundayTime)
    AnyTextView openingHoursSundayTime;
    @BindView(R.id.tv_detailPage_openingHours_mondayTime)
    AnyTextView openingHoursMondayTime;
    @BindView(R.id.tv_detailPage_openingHours_tuesdayTime)
    AnyTextView openingHoursTuesdayTime;
    @BindView(R.id.tv_detailPage_openingHours_wednesdayTime)
    AnyTextView openingHoursWednesdayTime;
    @BindView(R.id.tv_detailPage_openingHours_thursdayTime)
    AnyTextView openingHoursThursdayTime;
    @BindView(R.id.fl_detailPage_tags)
    FlowLayout flowLayoutTags;
    @BindView(R.id.containerReview)
    RelativeLayout containerReview;
    @BindView(R.id.img_detailPage_backBtn)
    ImageView imgBackBtn;
    @BindView(R.id.img_detailPage_favorite)
    ImageView img_detailPage_favorite;
    @BindView(R.id.rv_detailPage_menu)
    RecyclerView rvMenu;
    @BindView(R.id.rv_detailPage_rating)
    RecyclerView rvRatings;
    @BindView(R.id.rv_detailPage_redeemVoucher)
    RecyclerView rvRedeemVoucher;
    @BindView(R.id.tv_detailPage_shareVoucher)
    TextView tvShareVoucher;
    @BindView(R.id.rv_detailPage_files)
    RecyclerView rvFiles;
    @BindView(R.id.rv_detailPage_suggestedOffers)
    RecyclerView rvSuggestedOffers;
    @BindView(R.id.img_detailPage_reviewProfileImage)
    ImageView imgReviewProfileImage;
    @BindView(R.id.tv_detailPage_reviewProfileName)
    AnyTextView tvReviewProfileName;
    @BindView(R.id.tv_detailPage_profileReviews)
    AnyTextView tvTextReviews;
    @BindView(R.id.tv_detailPage_viewAll)
    AnyTextView tvViewAll;
    @BindView(R.id.rl_detailPage_writeReview)
    RelativeLayout rlBtnWriteReview;
    @BindView(R.id.vp_slider)
    ViewPager vpSlider;
    @BindView(R.id.ink_indicator)
    InkPageIndicator inkPageIndicator;
    String storeType = "", storeName = "", storeId = "";
    RecyclerViewListAdapter adapterMenu, adapterRatings, adapterRedeemVoucher, adapterFiles, adapterSuggestedOffers;
    @BindView(R.id.rb_writeReview_quality)
    CustomRatingBar rbQuality;
    @BindView(R.id.rb_writeReview_service)
    CustomRatingBar rbService;
    @BindView(R.id.rb_writeReview_ambiance)
    CustomRatingBar rbAmbiance;
    @BindView(R.id.rb_writeReview_price)
    CustomRatingBar rbPrice;
    @BindView(R.id.rb_writeReview_overAllExperience)
    CustomRatingBar rbOverAllExperience;
    ShareDialog shareDialog;
    private Unbinder mBinder;
    private ImageSliderAdapter sliderAdapter;
    private int isFavoriteMarked = 0;
    private int apiCount;
    private List<StoreVoucher> selectedSharedStoreVouchersList;
    private String profileImage;

    public DetailPageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shareDialog = new ShareDialog(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail_page, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initAdapters();
        initRecyclerViews();
        setListener();

        int userId = prefHelper.getUser().getId();
        String cityId = prefHelper.getUser().getSelectedCity();
        if (getArguments() != null) {
            storeId = getArguments().getString(AppConstants.BUNDLE_STORE_ID, "");
            storeType = getArguments().getString(AppConstants.BUNDLE_STORE_TYPE, "all");
            if (TextUtils.isEmpty(storeType)) {
                storeType = "all";
            }
        }
        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity())) {
            apiCount = 0;
            getStoreDetails(userId, storeId, storeType);
            getSuggestedStores(cityId, storeId, storeType);
            getReviews(userId, storeId);
        }
    }

    private void bindMenuData(List<StoreMenu> storeMenus) {
        if (storeMenus == null) return;
        adapterMenu.addAll(storeMenus);
    }

    private void bindTagsData(List<StoreTag> storeTags) {
        if (storeTags == null) return;

        FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(
                FlowLayout.LayoutParams.WRAP_CONTENT,
                FlowLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 10, 10, 10);

        flowLayoutTags.removeAllViews();
        for (StoreTag tag :
                storeTags) {
            TextView mTagView = new TextView(getDockActivity());
            mTagView.setTextColor(ContextCompat.getColor(getDockActivity(), R.color.white));
            mTagView.setTextSize(14);
            mTagView.setCompoundDrawablePadding(16);
            mTagView.setLayoutParams(params);
            setBackground(mTagView, tag.getColorCode());

            if (tag.getTagType() != null)
                switch (tag.getTagType()) {
                    case "icon":
                        mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                ContextCompat.getDrawable(getDockActivity(), R.drawable.ic_wifi),
                                null, null, null);
                        mTagView.setCompoundDrawablePadding(0);
                        break;
                    case "default":
                        TextViewHelper.setText(mTagView, tag.getTagName());
                        break;
                    case "holiday":
                        TextViewHelper.setText(mTagView, tag.getTagName());
                        if (tag.getIsOpen() != null) {
                            if (tag.getIsOpen().equals("1")) {
                                mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                        ContextCompat.getDrawable(getDockActivity(), R.drawable.ic_tick_white),
                                        null, null, null);
                            } else {
                                mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                        ContextCompat.getDrawable(getDockActivity(), R.drawable.ic_close_white),
                                        null, null, null);
                            }
                        }
                        break;
                    case "user":
                        TextViewHelper.setText(mTagView, tag.getTagName());
                        mTagView.setCompoundDrawablesWithIntrinsicBounds(
                                ContextCompat.getDrawable(getDockActivity(), R.drawable.ic_user_white),
                                null, null, null);
                        break;
                    default:
                        break;
                }
            flowLayoutTags.addView(mTagView);
        }
    }

    private void bindRatingData(DetailPageResult result) {
        float ratingAmbiance = result.getAmbianceRating();
        float ratingPrice = result.getPriceRating();
        float ratingQuality = result.getQualityRating();
        float ratingOverAll = result.getOverallRating();
        float ratingService = result.getServiceRating();

        rbAmbiance.setScore(ratingAmbiance);
        rbPrice.setScore(ratingPrice);
        rbQuality.setScore(ratingQuality);
        rbOverAllExperience.setScore(ratingOverAll);
        rbService.setScore(ratingService);

//        List<DetailsPageRatingEntity> ratingEntityList = new ArrayList<>();
//        ratingEntityList.add(new DetailsPageRatingEntity("Quality", ratingQuality));
//        ratingEntityList.add(new DetailsPageRatingEntity("Service", ratingService));
//        ratingEntityList.add(new DetailsPageRatingEntity("Ambiance", ratingAmbiance));
//        ratingEntityList.add(new DetailsPageRatingEntity("Price", ratingPrice));
//        ratingEntityList.add(new DetailsPageRatingEntity("Overall Experience", ratingOverAll));
//
//        adapterRatings.addAll(ratingEntityList);
    }

    private void bindRedeemVoucherData(List<StoreVoucher> storeVouchers) {
        if (storeVouchers == null || storeVouchers.size() == 0) {
            alternateTextVouchers.setVisibility(View.VISIBLE);
            tvShareVoucher.setVisibility(View.INVISIBLE);
            adapterRedeemVoucher.addAll(new ArrayList());
            return;
        }
        selectedSharedStoreVouchersList = new ArrayList<>();
        alternateTextVouchers.setVisibility(View.GONE);
        tvShareVoucher.setVisibility(View.VISIBLE);
        tvShareVoucher.setEnabled(false);
        tvShareVoucher.setBackgroundResource(R.drawable.drawable_share_voucher_disabled);
        adapterRedeemVoucher.addAll(storeVouchers);

    }

    private void bindFilesData(List<StorePhoto> storePhotos) {
        if (storePhotos == null) return;
        adapterFiles.addAll(storePhotos);
    }

    private void bindSuggestedOffersData(ArrayList<SuggestedStore> result) {
        adapterSuggestedOffers.addAll(result);
    }

    private void setBackground(TextView textView, String color) {
        Drawable tempDrawable = ContextCompat.getDrawable(getDockActivity(), R.drawable.drawable_capsule);
        LayerDrawable bubble = (LayerDrawable) tempDrawable; //(cast to root element in xml)
        GradientDrawable solidColor = (GradientDrawable) bubble.findDrawableByLayerId(R.id.outerRectangle);
        solidColor.setColor(getColor(color));
        textView.setBackground(tempDrawable);
    }

    private int getColor(String color) {
        try {
            return Color.parseColor(color);
        } catch (Exception ex) {
            Log.e("PARSE COLOR", "getColor: ");
            return ContextCompat.getColor(getDockActivity(), R.color.colorOrange);
        }
    }

    private void initAdapters() {
        adapterMenu = new DetailsMenuAdapter(getDockActivity(), this);
        adapterRatings = new DetailsRatingsAdapter(getDockActivity(), this);
        adapterRedeemVoucher = new DetailsRedeemVoucherAdapter(getDockActivity(), this, this, this);
        adapterFiles = new DetailsMediaFilesAdapter(getDockActivity(), this);
        adapterSuggestedOffers = new DetailsSuggestedOffersAdapter(getDockActivity(), this, this);

        sliderAdapter = new ImageSliderAdapter(getDockActivity(), null);
    }

    private void initRecyclerViews() {
        //Menu
        rvMenu.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvMenu.setAdapter(adapterMenu);

        //Ratings
        rvRatings.setLayoutManager(new GridLayoutManager(getDockActivity(), 3));
        int spanCount = 3; // default 3 columns
        int spacing = 50; // default 50px
        rvRatings.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, false));
        rvRatings.setAdapter(adapterRatings);

        //Redeem Vouchers
        rvRedeemVoucher.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.VERTICAL, false));
        rvRedeemVoucher.setAdapter(adapterRedeemVoucher);

        //Media Files
        rvFiles.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvFiles.setAdapter(adapterFiles);

        //Suggested Offers
        rvSuggestedOffers.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvSuggestedOffers.setAdapter(adapterSuggestedOffers);

        //viewpager slider
        vpSlider.setAdapter(sliderAdapter);
    }

    private void setListener() {
        tvDirections.setEnabled(true);
        rlBtnWriteReview.setEnabled(true);
        tvViewAll.setEnabled(true);

        imgBackBtn.setOnClickListener(this);
        tvShareVoucher.setOnClickListener(this);
//        tvDirections.setOnClickListener(this);
//        tvViewAll.setOnClickListener(this);
//        rlBtnWriteReview.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
        UIHelper.hideSoftKeyboard(getDockActivity(), getView());
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideTitleBar();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_detailPage_backBtn:
                getDockActivity().onBackPressed();
                break;
            case R.id.tv_detailPage_shareVoucher:
                //share voucher dialog
                if (checkLoading() || UIHelper.doubleClickCheck()) {
                    showShareVoucherDialog(selectedSharedStoreVouchersList);
                }
                break;
            default:
                break;
        }
    }

    private void showShareVoucherDialog(final List<StoreVoucher> storeVouchersList) {
        final RecyclerViewListAdapter adapter = new ShareVoucherAdapter(getDockActivity());
        final Dialog dialog = new Dialog(getDockActivity(), R.style.DialogCustomTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.voucher_share_dialog);
        RecyclerView rvShare = dialog.findViewById(R.id.rv_shareVoucher);
        rvShare.setLayoutManager(new LinearLayoutManager(getDockActivity()));
        rvShare.setAdapter(adapter);
        adapter.addAll(storeVouchersList);
        final AnyEditTextView email = dialog.findViewById(R.id.et_share_email);
        RelativeLayout send = dialog.findViewById(R.id.rl_share_send);
        dialog.findViewById(R.id.img_contactUs_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIHelper.hideSoftKeyboard(getDockActivity(), view);
                dialog.dismiss();
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //api send data
                if (checkLoading() && UIHelper.doubleClickCheck()) {
                    String userEmail = prefHelper.getUser().getEmail();
                    if (email.testValidity() && checkUserEmail(userEmail, email)) {
                        UIHelper.hideSoftKeyboard(getDockActivity(), v);
                        dialog.dismiss();
                        shareVoucher(email.getText().toString(), getShareVoucherObj(storeVouchersList));
                    }
                }
            }
        });
        dialog.show();
    }

    private boolean checkUserEmail(String userEmail, AnyEditTextView enteredEmail) {
        if (userEmail.equalsIgnoreCase(enteredEmail.getText().toString())) {
            enteredEmail.setError(getResources().getString(R.string.error_email_address_not_valid));
            return false;
        }
        return true;
    }

    private String getShareVoucherObj(List<StoreVoucher> storeVouchersList) {
        ArrayList<ShareVoucherEmailObj> objs = new ArrayList<>();
        for (StoreVoucher voucher :
                storeVouchersList) {
            ShareVoucherEmailObj obj = new ShareVoucherEmailObj();
            obj.setStoreId(storeId);
            obj.setStoreName(storeName);
            obj.setVoucherId(voucher.getId());
            objs.add(obj);
        }
        return new Gson().toJson(objs);
    }

    @RestAPI
    private void shareVoucher(String email, String obj) {
        loadingStarted();
        final int userId = prefHelper.getUser().getId();
        webService.shareVoucher(userId, email, obj).enqueue(new Callback<ResponseWrapper>() {
            @Override
            public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                loadingFinished();
                if (DetailPageFragment.this.isResumed()) {
                    if (response.body() != null && response.body().getResponse() != null
                            && response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
//                        UIHelper.showShortToastInCenter(getDockActivity(), response.body().getMessage());
                        UIHelper.showShortToastInCenter(getDockActivity(), "Voucher shared successfully!");
                        //delete vouchers
                        getStoreDetails(userId, storeId, storeType);
                    } else if (response.body() != null) {
                        UIHelper.showShortToastInCenter(getDockActivity(), response.body().getMessage());
                    } else
                        UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                loadingFinished();
                UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getTag() != null)
            switch (view.getTag().toString()) {
                case AppConstants.MENU_ADAPTER:
                    List<StoreMenu> listStorePhotos = (List<StoreMenu>) adapterMenu.getList();
                    getDockActivity().startActivity(new Intent(getDockActivity(), FullScreenSliderActivity.class)
                            .putExtra(INTENT_FULLSCREEN_SLIDER, new Gson().toJson(listStorePhotos))
                            .putExtra(INTENT_FULLSCREEN_SLIDER_POSITION, position));
                    break;
                case AppConstants.FILE_ADAPTER:
                    List<StorePhoto> list = (List<StorePhoto>) adapterFiles.getList();
                    List<StoreMenu> menus = new ArrayList<>();
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getFileType() != null && list.get(i).getFileType().equals("image"))
                            menus.add(new StoreMenu(list.get(i).getId(), list.get(i).getStoreId(),
                                    list.get(i).getPhotoImage()));
                    }
                    getDockActivity().startActivity(new Intent(getDockActivity(), FullScreenSliderActivity.class)
                            .putExtra(INTENT_FULLSCREEN_SLIDER, new Gson().toJson(menus))
                            .putExtra(INTENT_FULLSCREEN_SLIDER_POSITION, position));
                    break;
            }
    }

    @RestAPI
    private void getStoreDetails(int userId, String storeId, String storeType) {
        loadingStarted();
        webService.getStoreDetails(userId, storeId, storeType).enqueue(new Callback<ResponseWrapper<DetailPageResult>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<DetailPageResult>> call, Response<ResponseWrapper<DetailPageResult>> response) {
                if (DetailPageFragment.this.isDetached() || DetailPageFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    bindData(response.body().getResult());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
                loadingFinish(++apiCount);
            }

            @Override
            public void onFailure(Call<ResponseWrapper<DetailPageResult>> call, Throwable t) {
                loadingFinish(++apiCount);
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    @RestAPI
    private void FavoriteAPI(int userId, int storeId) {
        loadingStarted();
        webService.addOrRemoveFavourite(userId, storeId).enqueue(new Callback<ResponseWrapper<FavoritesListResult>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<FavoritesListResult>> call, Response<ResponseWrapper<FavoritesListResult>> response) {
                loadingFinished();
                if (DetailPageFragment.this.isDetached() || DetailPageFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    toggleImage();
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<FavoritesListResult>> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    @RestAPI
    private void getSuggestedStores(String cityId, String storeId, String type) {
        webService.getSuggestedStores(cityId, storeId, type).enqueue(new Callback<ResponseWrapper<ArrayList<SuggestedStore>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<SuggestedStore>>> call, Response<ResponseWrapper<ArrayList<SuggestedStore>>> response) {
                if (!DetailPageFragment.this.isVisible()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    bindSuggestedOffersData(response.body().getResult());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
                loadingFinish(++apiCount);
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<SuggestedStore>>> call, Throwable t) {
                loadingFinish(++apiCount);
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    @RestAPI
    private void getReviews(int user_id, final String store_id) {
        webService.getReviews(user_id, store_id)
                .enqueue(new Callback<ResponseWrapper<ArrayList<ReviewResult>>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<ArrayList<ReviewResult>>> call,
                                           Response<ResponseWrapper<ArrayList<ReviewResult>>> response) {
                        if (!DetailPageFragment.this.isVisible()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            updateReview(response.body().getResult(), store_id);
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                        loadingFinish(++apiCount);
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<ArrayList<ReviewResult>>> call, Throwable t) {
                        loadingFinish(++apiCount);
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    private void updateReview(ArrayList<ReviewResult> result, final String store_id) {
        //write reviews
        rlBtnWriteReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlBtnWriteReview.setEnabled(false);
                WriteReviewFragment fragment = WriteReviewFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_STORE_ID, store_id);
                bundle.putString(AppConstants.BUNDLE_STORE_NAME, storeName);
                fragment.setArguments(bundle);
                getDockActivity().replaceDockableFragment(fragment,
                        WriteReviewFragment.class.getSimpleName());
            }
        });

        if (result == null || result.size() == 0) {
            alternateTextReviews.setVisibility(View.VISIBLE);
            return;
        }
        alternateTextReviews.setVisibility(View.GONE);

        ReviewResult obj = result.get(0);
        String profileImage = obj.getProfilePicture();
        String profileName = obj.getUserName();
        String reviewMsg = obj.getReview();

        imgReviewProfileImage.setVisibility(View.VISIBLE);
        tvReviewProfileName.setVisibility(View.VISIBLE);

        ImageLoaderHelper.loadImage(profileImage, imgReviewProfileImage);
        TextViewHelper.setText(tvReviewProfileName, profileName);
        TextViewHelper.setText(tvTextReviews, reviewMsg);

        containerReview.setVisibility(View.VISIBLE);
        tvViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //read All reviews
                tvViewAll.setEnabled(false);
                ReviewsFragment fragment = ReviewsFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_STORE_ID, store_id);
                fragment.setArguments(bundle);
                getDockActivity().replaceDockableFragment(fragment,
                        ReviewsFragment.class.getSimpleName());
            }
        });
    }

    private void toggleImage() {
        if (isFavoriteMarked == 0) {
            isFavoriteMarked = 1;
            img_detailPage_favorite.setImageResource(R.drawable.fav_icon);
            tvFavorite.setText(getString(R.string.unfavorite));
        } else {
            isFavoriteMarked = 0;
            img_detailPage_favorite.setImageResource(R.drawable.favorite);
//            tvFavorite.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.favorite, 0, 0);
            tvFavorite.setText(getString(R.string.favorite));
        }
    }

    private void bindData(DetailPageResult result) {
        if (result == null || !DetailPageFragment.this.isResumed()) return;
        profileImage = result.getProfileImage();
        bindViews(result);
        bindTagsData(result.getStoreTags());
        bindMenuData(result.getStoreMenus());
        bindRatingData(result);
        bindRedeemVoucherData(result.getStoreVouchers());
        bindFilesData(result.getStorePhotos());
        bindOpeningHours(result.getStoreHours());
    }

    private void bindViews(final DetailPageResult result) {
        if (result == null || !DetailPageFragment.this.isResumed()) return;

        List<StoreBanners> banners = result.getStoreBanners();
        sliderAdapter.addAll(banners);
        try {
            inkPageIndicator.setViewPager(vpSlider);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        storeName = result.getFullName();
        TextViewHelper.setText(tvName, result.getFullName());
        TextViewHelper.setText(tvAddress, result.getLocation());
        isOpenNow(result);

        //Call
        final String phone = result.getPhoneNo();
        tvCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });

        //Email
        final String email = result.getEmail();
        tvEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", email, null));
                getDockActivity().startActivity(Intent.createChooser(emailIntent, "Subject"));
            }
        });

        //favorite
        final int storeId = result.getId();
        isFavoriteMarked = result.getIsFavourite();
        if (isFavoriteMarked == 0) {
            img_detailPage_favorite.setImageResource(R.drawable.favorite);
            tvFavorite.setText(getString(R.string.favorite));
        } else {
            img_detailPage_favorite.setImageResource(R.drawable.fav_icon);
            tvFavorite.setText(getString(R.string.unfavorite));
        }
        containerFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FavoriteAPI(prefHelper.getUser().getId(), storeId);
            }
        });

        //Direction map
        final String lat = result.getLatitude();
        final String lng = result.getLongitude();
        tvDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvDirections.setEnabled(false);
                DirectionMapFragment fragment = new DirectionMapFragment();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_LAT, lat);
                bundle.putString(AppConstants.BUNDLE_LNG, lng);
                fragment.setArguments(bundle);
                getDockActivity().replaceDockableFragment(fragment,
                        DirectionMapFragment.class.getSimpleName());
            }
        });

        //Share
        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String restuarantName = result.getFullName().trim();
                String textToShare = String.format(Locale.US, "Check out %s on Dining & Nightlife Discounts!", restuarantName);
                SelectShareIntent.selectIntent(getDockActivity(), getResources().getString(R.string.share), restuarantName, textToShare);
            }
        });

        //should how Write Review Button
        if (result.getIs_show_review_button() == 0) {
            rlBtnWriteReview.setVisibility(View.GONE);
        } else rlBtnWriteReview.setVisibility(View.VISIBLE);
    }

    private void isOpenNow(DetailPageResult result) {
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = calendar.getTime();
            String currentDay = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()).toLowerCase();
            String currentDaySuffix = new SimpleDateFormat("EE", Locale.ENGLISH).format(date.getTime()).toLowerCase();

            List<StoreHour> hoursList = result.getStoreHours();
            if (hoursList == null || hoursList.size() == 0) {
                String text = "<font color='#E94B45'>Closed Now</font>";
                tvStatus.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
            }

            if (currentDay.equals("monday")) {
                StoreHour obj = hoursList.get(0);
                setIfOpenNow(obj, currentDaySuffix);
            } else if (currentDay.equals("tuesday")) {
                StoreHour obj = hoursList.get(1);
                setIfOpenNow(obj, currentDaySuffix);
            } else if (currentDay.equals("wednesday")) {
                StoreHour obj = hoursList.get(2);
                setIfOpenNow(obj, currentDaySuffix);
            } else if (currentDay.equals("thursday")) {
                StoreHour obj = hoursList.get(3);
                setIfOpenNow(obj, currentDaySuffix);
            } else if (currentDay.equals("friday")) {
                StoreHour obj = hoursList.get(4);
                setIfOpenNow(obj, currentDaySuffix);
            } else if (currentDay.equals("saturday")) {
                StoreHour obj = hoursList.get(5);
                setIfOpenNow(obj, currentDaySuffix);
            } else if (currentDay.equals("sunday")) {
                StoreHour obj = hoursList.get(6);
                setIfOpenNow(obj, currentDaySuffix);
            }
        } catch (Exception e) {
            e.printStackTrace();
            String text = "<font color='#E94B45'>Closed Now</font>";
            if (tvStatus != null)
                tvStatus.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
        }
    }

    private void setIfOpenNow(StoreHour obj, String currentDaySuffix) throws ParseException {
        String upperCaseSuffix = currentDaySuffix.substring(0, 1).toUpperCase() + currentDaySuffix.substring(1);

        String from = obj.getFrom();
        Date dFrom = new SimpleDateFormat("HH:mm:ss", Locale.US).parse(from);
        Calendar cFrom = Calendar.getInstance();
        cFrom.setTime(dFrom);
        cFrom.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));
        cFrom.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        cFrom.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));

        String to = obj.getTo();
        Date dTo = new SimpleDateFormat("HH:mm:ss", Locale.US).parse(to);
        Calendar cTo = Calendar.getInstance();
        cTo.setTime(dTo);
        cTo.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));
        cTo.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        cTo.set(Calendar.MONTH, Calendar.getInstance().get(Calendar.MONTH));

        Calendar calendar3 = Calendar.getInstance();
        Date x = calendar3.getTime();
        if ((obj.getIsOpen() == 1) && x.after(cFrom.getTime()) && (x.before(cTo.getTime()) || cFrom.getTime().after(cTo.getTime()))) {
            //checkes whether the current time is between
            String text = "<font color='#85B954'>Open Now</font> <font color='#000000'>-</font> ";
            tvStatus.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
            TextViewHelper.setText(tvStatusOnTime,
                    String.format(Locale.US, "%s to %s (%s)",
                            getTime(obj.getFrom(), false),
                            getTime(obj.getTo(), false),
                            upperCaseSuffix
                    ));
        } else {
            String text = "<font color='#E94B45'>Closed Now</font>";
            tvStatus.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
        }
    }

    private void bindOpeningHours(List<StoreHour> storeHours) {
        if (storeHours == null) return;
        for (int i = 0; i < storeHours.size(); i++) {
            AnyTextView openingHoursTime;
            switch (i) {
                case 0:
                    openingHoursTime = openingHoursMondayTime;
                    break;
                case 1:
                    openingHoursTime = openingHoursTuesdayTime;
                    break;
                case 2:
                    openingHoursTime = openingHoursWednesdayTime;
                    break;
                case 3:
                    openingHoursTime = openingHoursThursdayTime;
                    break;
                case 4:
                    openingHoursTime = openingHoursFridayTime;
                    break;
                case 5:
                    openingHoursTime = openingHoursSaturdayTime;
                    break;
                case 6:
                    openingHoursTime = openingHoursSundayTime;
                    break;
                default:
                    openingHoursTime = openingHoursMondayTime;
                    break;
            }
            String from = "", to = "";
            if (storeHours.get(i).getIsOpen() == 1) {
                String fri = String.format(Locale.US, "%s to %s",
                        getTime(storeHours.get(i).getFrom(), true),
                        getTime(storeHours.get(i).getTo(), true));
                TextViewHelper.setText(openingHoursTime, fri);
            } else
                openingHoursTime.setTextColor(ContextCompat.getColor(getDockActivity(), R.color.colorRed));
        }
    }

    private String getTime(@NonNull String val, boolean isTable) {
        int index = val.indexOf(":");
        if (index != -1) {
            String minutes = val.substring(index + 1, val.lastIndexOf(":"));
            if (minutes.equals("00")) {
                if (isTable)
                    return DateHelper.dateFormatUTC(val, "h:mm a", AppConstants.DATE_FORMAT_TIME).toLowerCase();
                else
                    return DateHelper.dateFormatUTC(val, "h:mm a", AppConstants.DATE_FORMAT_TIME);
            } else {
                if (isTable)
                    return DateHelper.dateFormatUTC(val, "h:mm a", AppConstants.DATE_FORMAT_TIME).toLowerCase();
                else
                    return DateHelper.dateFormatUTC(val, "h:mm a", AppConstants.DATE_FORMAT_TIME);
            }
        }
        if (isTable)
            return DateHelper.dateFormatUTC(val, "h a", AppConstants.DATE_FORMAT_TIME).toLowerCase();
        else
            return DateHelper.dateFormatUTC(val, "h a", AppConstants.DATE_FORMAT_TIME);
    }

    @Override
    public void onSuggestedStoreClicked(int storeId) {
        //open new detail page here and add in stack
        DetailPageFragment fragment = new DetailPageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(storeId));
        bundle.putString(AppConstants.BUNDLE_STORE_TYPE, storeType);
        fragment.setArguments(bundle);
        getDockActivity().replaceDockableFragment(fragment, DetailPageFragment.class.getSimpleName());
    }

    @Override
    public void onRedeemItemClicked(String voucherName, String voucherId, String storeType) {
        RedeemVoucherFragment fragment = RedeemVoucherFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_STORE_ID, storeId);
        bundle.putString(AppConstants.BUNDLE_STORE_VOUCHER_ID, voucherId);
        bundle.putString(AppConstants.BUNDLE_VOUCHER_NAME, voucherName);
        bundle.putString(AppConstants.BUNDLE_VOUCHER_STORE_TYPE, storeType);
        bundle.putString(AppConstants.BUNDLE_STORE_NAME, storeName); // restaurant name
        fragment.setArguments(bundle);
        getDockActivity().replaceDockableFragment(fragment,
                RedeemVoucherFragment.class.getSimpleName());
    }

    private void loadingFinish(int count) {
        if (count > 2)
            loadingFinished();
    }

    @Override
    public void onCheckBoxClick(StoreVoucher item, int position, boolean isChecked) {
        if (isChecked) {
            //add to list
            item.setPosition(position);
            item.setProfilePicture(profileImage);
            selectedSharedStoreVouchersList.add(item);
        } else {
            //remove to list
            for (int i = 0; i < selectedSharedStoreVouchersList.size(); i++) {
                if (selectedSharedStoreVouchersList.get(i).getPosition() == position)
                    selectedSharedStoreVouchersList.remove(i);
            }
        }
        if (selectedSharedStoreVouchersList.size() == 1) {
            tvShareVoucher.setEnabled(true);
            tvShareVoucher.setBackgroundResource(R.drawable.drawable_share_voucher_enabled);
        } else if (selectedSharedStoreVouchersList.size() == 0) {
            tvShareVoucher.setEnabled(false);
            tvShareVoucher.setBackgroundResource(R.drawable.drawable_share_voucher_disabled);
        }
    }
}