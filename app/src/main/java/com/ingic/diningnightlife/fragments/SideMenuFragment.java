package com.ingic.diningnightlife.fragments;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.flyco.animation.BounceEnter.BounceEnter;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.activities.MainActivity;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.interfaces.ProfileUpdateListener;
import com.ingic.diningnightlife.ui.adapters.SideMenuAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyEditTextView;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SideMenuFragment extends BaseFragment implements OnViewHolderClick, ProfileUpdateListener {
    @BindView(R.id.img_sideMenu_profileImage)
    ImageView imgProfile;
    @BindView(R.id.tv_sideMenu_profileName)
    AnyTextView tvProfileName;
    @BindView(R.id.rv_sideMenu)
    RecyclerView rvSideMenu;
    private Unbinder mBinder;
    private List<String> sideMenuList = new ArrayList<>();

    private RecyclerViewListAdapter adapter;
    private BroadcastReceiver sideMenuUpdator;

    public static SideMenuFragment newInstance() {
        return new SideMenuFragment();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new SideMenuAdapter(getDockActivity(), this);
        sideMenuUpdator = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                profileUpdate();
            }
        };
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sidemenu, container, false);
        mBinder = ButterKnife.bind(this, view);
        rvSideMenu.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.VERTICAL, false));
        rvSideMenu.setAdapter(adapter);
        bindData();
        profileUpdate();
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDockActivity().replaceDockableFragment(ProfileFragment.newInstance(SideMenuFragment.this), ProfileFragment.class.getSimpleName());

            }
        });
        return view;
    }

    private void bindData() {
//        sideMenuList.add("Change City");//TODO Abu Dhabi Only
        sideMenuList.add("Home");
        sideMenuList.add("Profile");
        sideMenuList.add("My Vouchers");
        sideMenuList.add("News");
        sideMenuList.add("Notifications");
        sideMenuList.add("Savings");
        sideMenuList.add("Subscription");
        sideMenuList.add("Favorites");
        sideMenuList.add("Terms & Conditions");
        sideMenuList.add("Contact Us");
        sideMenuList.add("Logout");
        adapter.addAll(sideMenuList);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideTitleBar();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @Override
    public void onItemClick(View view, int position) {
        //sideMenu click events
        switch (position) {
            //TODO Abu Dhabi Only
//            case 0:
//                //change city
//                getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(), HomeFragment_SelectLocation.class.getSimpleName());
//                break;
            case 0:
                //home
                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(AppConstants.FETCH_LOCATION), HomeFragment.class.getSimpleName());
                break;
            case 1:
                //profile
                getDockActivity().replaceDockableFragment(ProfileFragment.newInstance(this), ProfileFragment.class.getSimpleName());
                break;
            case 2: //My Vouchers
                getDockActivity().replaceDockableFragment(MyVouchersFragment.newInstance(), MyVouchersFragment.class.getSimpleName());
                break;
            case 3: //News
                getDockActivity().replaceDockableFragment(NewsFragment.newInstance(), NewsFragment.class.getSimpleName());
                break;
            case 4: //Notifications
                getDockActivity().replaceDockableFragment(NotificationsFragment.newInstance(), NotificationsFragment.class.getSimpleName());
                break;
            case 5: //Savings
                getDockActivity().replaceDockableFragment(SavingsFragment.newInstance(), SavingsFragment.class.getSimpleName());
                break;
            case 6: //Subscription
                if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
                    getDockActivity().replaceDockableFragment(SubscriptionFragment.newInstance(false), SubscriptionFragment.class.getSimpleName());
                break;
            case 7: ///Favorites
                getDockActivity().replaceDockableFragment(FavoritesFragment.newInstance(), FavoritesFragment.class.getSimpleName());
                break;
            case 8: //T&C
                getDockActivity().replaceDockableFragment(TnCFragment.newInstance(), TnCFragment.class.getSimpleName());
                break;
            case 9: //contact Us
                getDockActivity().closeDrawer();
                contactUs(getDockActivity());
                break;
            case 10:  //logout
                logoutDialog();
                break;
            default:
                break;
        }
    }

    private void logoutDialog() {
        final NormalDialog dialog = new NormalDialog(getDockActivity());
        dialog.content(getResources().getString(R.string.logout_msg))
                .style(NormalDialog.STYLE_TWO)
                .title(getResources().getString(android.R.string.dialog_alert_title))
                .titleTextSize(23)
                .contentGravity(Gravity.CENTER)
                .showAnim(new BounceEnter())
                .btnText(getResources().getString(android.R.string.yes), getResources().getString(android.R.string.no))
                .show();

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                        navigateToLogin();
                    }
                },
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                    }
                });
    }

    private void navigateToLogin(){
        prefHelper.setLoginStatus(false);
        prefHelper.clearData();
        imgProfile.setImageResource(R.drawable.placeholder_error);
        tvProfileName.setText("");
        getDockActivity().startActivity(new Intent(getDockActivity(), MainActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    private void contactUs(Context context) {
        final Dialog dialog = new Dialog(context, R.style.DialogCustomTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_contact_us);
        final AnyEditTextView name = (AnyEditTextView) dialog.findViewById(R.id.et_contactUs_fullName);
        final AnyEditTextView email = (AnyEditTextView) dialog.findViewById(R.id.et_contactUs_email);
        final AnyEditTextView phone = (AnyEditTextView) dialog.findViewById(R.id.et_contactUs_phone);
        final AnyEditTextView desc = (AnyEditTextView) dialog.findViewById(R.id.et_contactUs_description);
        RelativeLayout submit = (RelativeLayout) dialog.findViewById(R.id.rl_contactUs_submit);
        final ImageView imageClose = (ImageView) dialog.findViewById(R.id.img_contactUs_close);
        if (prefHelper != null && prefHelper.getUser() != null) {
            String _name = prefHelper.getUser().getFullName();
            String _email = prefHelper.getUser().getEmail();
            String _phone = prefHelper.getUser().getPhoneNo();

            if (!_name.isEmpty()) {
                name.setEnabled(false);
            }
            if (!_email.isEmpty()) {
                email.setEnabled(false);
            }

            TextViewHelper.setText(name, _name);
            TextViewHelper.setText(email, _email);
            TextViewHelper.setText(phone, _phone);
        }
        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidate(name, email, desc, phone)) {
                    dialog.dismiss();
                    if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
                        sendContactDetails(prefHelper.getUser().getId(),
                                name.getText().toString(), email.getText().toString(), phone.getText().toString(), desc.getText().toString());
                }
            }
        });
        dialog.show();
    }

    private boolean isValidate(AnyEditTextView name, AnyEditTextView email, AnyEditTextView desc, AnyEditTextView phone) {
        if (name.testValidity() && email.testValidity() && desc.testValidity()) {
            if (phone.length() > 7 || phone.length() < 1) {
                return true;
            } else
                phone.setError("Please Enter valid phone number");

        }// && checkPhoneLength());
        return false;
    }
/*    private boolean isValidate(AnyEditTextView name, AnyEditTextView email, AnyEditTextView desc, AnyEditTextView phone) {
        if (name.testValidity() && email.testValidity() && desc.testValidity()) {
            if (phone.length() > 9 || phone.length() < 1) {
                return true;
            } else
                phone.setError("Please Enter valid phone number");

        }// && checkPhoneLength());
        return false;
    }*/

    @Override
    public void profileUpdate() {
        if (prefHelper != null && prefHelper.getUser() != null) {
            String profileImage = prefHelper.getUser().getProfilePicture();
            String profileName = prefHelper.getUser().getFullName();

            ImageLoaderHelper.loadImage(profileImage, imgProfile);
            TextViewHelper.setText(tvProfileName, profileName);
        }
    }

    @RestAPI
    private void sendContactDetails(int user_id, String full_name, String email_address, String phone_no, String description) {
        loadingStarted();
        webService.sendContactDetails(user_id, full_name, email_address, phone_no, description)
                .enqueue(new Callback<ResponseWrapper>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                        loadingFinished();
                        if (SideMenuFragment.this.isDetached() || SideMenuFragment.this.isRemoving()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.thankyou_for_your_enquiry));
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                        loadingFinished();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        getDockActivity().registerReceiver(sideMenuUpdator, new IntentFilter(AppConstants.SIDE_MENU_UPDATOR));
    }

    @Override
    public void onPause() {
        super.onPause();
        getDockActivity().unregisterReceiver(sideMenuUpdator);
    }
}