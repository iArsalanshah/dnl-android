package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.ParentEntity;
import com.ingic.diningnightlife.entities.Sent;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.ui.adapters.MyVouchersAdapter;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class VouchersSentFragment extends Fragment {


    @BindView(R.id.rv_vouchersSent)
    RecyclerView rvVouchersSent;
    Unbinder unbinder;
    private MyVouchersAdapter adapter;

    public VouchersSentFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vouchers_sent, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (getArguments() != null) {
            String sentVouchersString = getArguments().getString(AppConstants.BUNDLE_SENT_VOUCHERS, null);
            if (sentVouchersString != null) {
                Sent obj = new Gson().fromJson(sentVouchersString, Sent.class);
                init(obj);
            }
        }
        return view;
    }

    private void init(Sent obj) {
        rvVouchersSent.setLayoutManager(new LinearLayoutManager(getActivity()));
        ParentEntity dining = new ParentEntity("Dining Deals", obj.getDinning());
        ParentEntity brunch = new ParentEntity("Brunch Bargains", obj.getBrunch());
        ParentEntity lastMinute = new ParentEntity("Last Minute Offers", obj.getLastMinute());
        ParentEntity exclusive = new ParentEntity("Exclusive Experiences", obj.getExclusive());
        ParentEntity nightlife = new ParentEntity("Nightlife", obj.getNightlife());

        List<ParentEntity> parentEntityList = Arrays.asList(dining, brunch, lastMinute, exclusive, nightlife);

        // RecyclerView has some built in animations to it, using the DefaultItemAnimator.
        // Specifically when you call notifyItemChanged() it does a fade animation for the changing
        // of the data in the ViewHolder. If you would like to disable this you can use the following:
        RecyclerView.ItemAnimator animator = rvVouchersSent.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        adapter = new MyVouchersAdapter(getActivity(), parentEntityList, false, null);
        rvVouchersSent.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
