package com.ingic.diningnightlife.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.LoginResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.ProfileUpdateListener;
import com.ingic.diningnightlife.ui.views.AnyEditTextView;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnterPinFragment extends BaseFragment implements View.OnClickListener, ProfileUpdateListener {
    @BindView(R.id.tv_enterPin_welcome)
    AnyTextView pinEntryWelcomeText;
    @BindView(R.id.layout_et2)
    RelativeLayout rl_pinEntry_email;
    @BindView(R.id.et_pinEntry)
    PinEntryEditText pinEntryEditText;
    @BindView(R.id.rl_pinEnter_proceed)
    RelativeLayout btnProceed;
    @BindView(R.id.txt_pinEntry_resend)
    AnyTextView txtResend;
    @BindView(R.id.et_enterPin_email)
    AnyEditTextView etEmail;
    private int emailUpdateState;

    private Unbinder mBinder;
    private ProfileUpdateListener profileUpdateListener;

    public EnterPinFragment() {
        // Required empty public constructor
    }

    public static EnterPinFragment newInstance() {
        return new EnterPinFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_enter_pin, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        profileUpdateListener = this;
        setListener();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            //new user or existing ....
            String userType = getArguments().getString(AppConstants.BUNDLE_LOGIN_USER_TYPE);
            if (userType.equals(AppConstants.BUNDLE_NEW_USER_SIGN_UP)) {
                rl_pinEntry_email.setVisibility(View.GONE);
                pinEntryWelcomeText.setVisibility(View.VISIBLE);
            } else if (userType.equals(AppConstants.BUNDLE_EXITING_USER_LOGIN)) {
                pinEntryWelcomeText.setVisibility(View.GONE);
                rl_pinEntry_email.setVisibility(View.VISIBLE);
            }
            //-------------------------------//

            String bundleData = getArguments().getString(AppConstants.BUNDLE_LOGIN_RESULT);
            emailUpdateState = getArguments().getInt(AppConstants.BUNDLE_EMAIL_UPDATE_STATE, 0);
            if (bundleData != null) {
                LoginResult result = new Gson().fromJson(bundleData, LoginResult.class);
                etEmail.setText(result.getEmail());
                etEmail.setCursorVisible(false);
                etEmail.setFocusable(false);
            } else if (!prefHelper.getLastEnteredEmail().isEmpty()) {
                etEmail.setText(prefHelper.getLastEnteredEmail());
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    private void setListener() {
        btnProceed.setOnClickListener(this);
        txtResend.setOnClickListener(this);
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.showTitleBar();
        titleBar.showBackButton();
        titleBar.setSubHeading(getDockActivity().getResources().getString(R.string.enter_pin));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @Override
    public void onClick(View view) {
        if (checkLoading())
            switch (view.getId()) {
                case R.id.rl_pinEnter_proceed:
                    if (UIHelper.doubleClickCheck())
                        if (pinEntryEditText.getText().toString().length() < 4) {
                            pinEntryEditText.setError("Please add PIN");
                            pinEntryEditText.requestFocus();
                        } else if (etEmail.testValidity()) {
                            if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity())) {
                                String refreshedToken = FirebaseInstanceId.getInstance().getToken();
                                login(etEmail.getText().toString(), pinEntryEditText.getText().toString(), refreshedToken, emailUpdateState);
                            }
                        }
                    break;
                case R.id.txt_pinEntry_resend:
                    if (UIHelper.doubleClickCheck())
                        if (etEmail.testValidity()) {
                            if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
                                resendCode(etEmail.getText().toString());
                        }
                    break;
                default:
                    break;
            }
    }

    private void launchHomeFragment(LoginResult result) {
        if (result == null) {
            UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
            return;
        }
        prefHelper.setLoginStatus(true);
        prefHelper.putUser(result);
        prefHelper.setLastEnteredEmail(result.getEmail());
        sendBroadCast();
//        if (profileUpdateListener != null)
//            profileUpdateListener.profileUpdate();
        getDockActivity().popBackStackTillEntry(0);

        if (prefHelper.getIsUserSignUp()) {
            getDockActivity().replaceDockableFragment(SubscriptionFragment.newInstance(true), SubscriptionFragment.class.getSimpleName());
        } else if (prefHelper.getUser() != null && prefHelper.getUser().getIsSubscribe() == AppConstants.YES) {
            getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(), HomeFragment_SelectLocation.class.getSimpleName());
        } else {
            getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(), HomeFragment_SelectLocation.class.getSimpleName());
        }

//
//        if (prefHelper.getUser() != null && prefHelper.getUser().getIsSubscribe() == AppConstants.YES) {
//            if (prefHelper.getIsUserSignUp()) {
//                //TODO Abu Dhabi Only
////                getDockActivity().replaceDockableFragment(HomeFragment_SelectLocation.newInstance(), HomeFragment_SelectLocation.class.getSimpleName());
//                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false), HomeFragment.class.getSimpleName());
//            } else
//                //TODO Abu Dhabi Only
////                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(true), HomeFragment.class.getSimpleName());
//                getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false), HomeFragment.class.getSimpleName());
//        } else
//            getDockActivity().replaceDockableFragment(SubscriptionFragment.newInstance(true), SubscriptionFragment.class.getSimpleName());

//        getDockActivity().replaceDockableFragment(SubscriptionFragment.newInstance(false), SubscriptionFragment.class.getSimpleName());
        //pinEntryEditText.setText(""); //if back stack would be available then clear text
    }

    @RestAPI
    private void login(String email, String verificationCode, String token, int _state) {
        loadingStarted();
        webService.login(email, verificationCode, WebServiceConstants.DEVICE, token, _state)
                .enqueue(new Callback<ResponseWrapper<LoginResult>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<LoginResult>> call, Response<ResponseWrapper<LoginResult>> response) {
                        loadingFinished();
                        if (EnterPinFragment.this.isDetached() || EnterPinFragment.this.isRemoving()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            launchHomeFragment(response.body().getResult());
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<LoginResult>> call, Throwable t) {
                        loadingFinished();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    @RestAPI
    private void resendCode(String email) {
        loadingStarted();
        webService.resendCode(email).enqueue(new Callback<ResponseWrapper>() {
            @Override
            public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                loadingFinished();
                if (EnterPinFragment.this.isDetached() || EnterPinFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    if (EnterPinFragment.this.isVisible())
                        UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.resend_pin_success));
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    @Override
    public void profileUpdate() {
    }

    private void sendBroadCast() {
        Intent intent = new Intent(AppConstants.SIDE_MENU_UPDATOR);
//        LocalBroadcastManager.getInstance(getDockActivity()).sendBroadcast(intent);
        getDockActivity().sendBroadcast(intent);
    }

}