package com.ingic.diningnightlife.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.flyco.animation.BounceEnter.BounceEnter;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.NormalDialog;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.BannerResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.SubscribeResult;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ingic.diningnightlife.global.AppConstants.DEFAULT_BANNER_REFRESH_RATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class RedeemVoucherFragment extends BaseFragment implements View.OnClickListener {
    @BindView(R.id.img_redeem_voucher_BANNER)
    ImageView img_BANNER;
    @BindView(R.id.txt_redeemVoucher_label)
    AnyTextView txtRedeemVoucherLabel;
    @BindView(R.id.et_pinEntry)
    PinEntryEditText pinEntryEditText;
    @BindView(R.id.rl_redeemVoucher_redeem)
    RelativeLayout btnRedeem;
    @BindView(R.id.txt_redeemVoucher_forgot)
    AnyTextView txtForgot;
    @BindView(R.id.txt_redeemVoucher_tnc)
    AnyTextView txtTnC;
    private Unbinder mBinder;
    private String storeId = "", voucherName = "", storeName = "";
    private String voucherId = "", freeVoucherId = "";

    //for banner
    List<BannerResult> bannerList;
    Handler handler = new Handler();
    Runnable r;
    private String voucherStoreType;

    public RedeemVoucherFragment() {
        // Required empty public constructor
    }

    public static RedeemVoucherFragment newInstance() {
        return new RedeemVoucherFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_redeem_voucher, container, false);
        storeId = getArguments().getString(AppConstants.BUNDLE_STORE_ID, "");
        voucherId = getArguments().getString(AppConstants.BUNDLE_STORE_VOUCHER_ID, "");
        voucherName = (getArguments().getString(AppConstants.BUNDLE_VOUCHER_NAME, ""));
        voucherStoreType = (getArguments().getString(AppConstants.BUNDLE_VOUCHER_STORE_TYPE, ""));
        storeName = getArguments().getString(AppConstants.BUNDLE_STORE_NAME, ""); //restaurant name , for write review
        freeVoucherId = getArguments().getString(AppConstants.BUNDLE_FREE_VOUCHER_ID, ""); //restaurant name , for write review

//        if (storeVoucherId != null || !storeVoucherId.isEmpty())
//            txtRedeemVoucherLabel.setText(storeVoucherId);
        mBinder = ButterKnife.bind(this, rootView);
        setHeader();
        setListener();
        if (InternetHelper.CheckInternetConectivity(getDockActivity()))
            getBanner(AppConstants.REDEEM);
        return rootView;
    }

    private void setHeader() {
        String titleSuffix = "Voucher";
        if (Util.isNotNullEmpty(voucherStoreType)) {
            if (voucherStoreType.equals("exclusive")) {
                titleSuffix = "Exclusive Voucher";
            } else if (voucherStoreType.equals("last_minute")) {
                titleSuffix = "Last Minute Voucher";
            }
        }
        if (titleSuffix.startsWith("E")) {
            TextViewHelper.setText(txtRedeemVoucherLabel, String.format(Locale.US, "%s", titleSuffix));
        } else if (titleSuffix.startsWith("L")) {
            TextViewHelper.setText(txtRedeemVoucherLabel, String.format(Locale.US, "%s", titleSuffix));
        } else {
            TextViewHelper.setText(txtRedeemVoucherLabel, String.format(Locale.US, "%s %s", voucherName, titleSuffix));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        handler.post(r);
    }

    private void setListener() {
        btnRedeem.setOnClickListener(this);
        txtForgot.setOnClickListener(this);
        txtTnC.setOnClickListener(this);
    }

    private void dialogLeaveReview(Context context) {
        final Dialog dialog = new Dialog(context, R.style.DialogCustomTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_city);
        AnyTextView title = dialog.findViewById(R.id.tv_dialog_title);
        AnyTextView message = dialog.findViewById(R.id.tv_dialog_message);
        AnyTextView btnNotNow = dialog.findViewById(R.id.btn_left);
        AnyTextView btnLeaveReview = dialog.findViewById(R.id.btn_right);
//        title.setVisibility(View.GONE);
        title.setText("Success");
        btnNotNow.setText("Not Now");
        btnLeaveReview.setText("Write Review");
        message.setText(getResources().getString(R.string.leave_review));
        btnNotNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getDockActivity().popFragment();
            }
        });
        btnLeaveReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getDockActivity().popFragment();
                WriteReviewFragment fragment = WriteReviewFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_STORE_ID, storeId);
                bundle.putString(AppConstants.BUNDLE_STORE_NAME, storeName); // restaurant name
                fragment.setArguments(bundle);
                getDockActivity().replaceDockableFragment(fragment,
                        WriteReviewFragment.class.getSimpleName());
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void LeaveReviewDialog(String uniqueCode) {
        String msg = String.format(Locale.US, "%s\nYour voucher code is: %s", getString(R.string.leave_review), uniqueCode);

        final NormalDialog dialog = new NormalDialog(getDockActivity());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.content(msg)
                .style(NormalDialog.STYLE_TWO)
                .title(getResources().getString(R.string.success))
                .titleTextSize(23)
                .contentGravity(Gravity.CENTER)
                .showAnim(new BounceEnter())
                .btnText(getResources().getString(R.string.not_now), getResources().getString(R.string.write_review))
                .show();

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        getDockActivity().popFragment();
                        dialog.dismiss();
                    }
                },
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            @Override
                            public void onDismiss(DialogInterface dialog) {
                                //write review
                                getDockActivity().popFragment();
                                WriteReviewFragment fragment = WriteReviewFragment.newInstance();
                                Bundle bundle = new Bundle();
                                bundle.putString(AppConstants.BUNDLE_STORE_ID, storeId);
                                bundle.putString(AppConstants.BUNDLE_STORE_NAME, storeName); // restaurant name
//                                bundle.putString(AppConstants.BUNDLE_FREE_VOUCHER_ID, freeVoucherId); // restaurant name
                                fragment.setArguments(bundle);
                                getDockActivity().replaceDockableFragment(fragment,
                                        WriteReviewFragment.class.getSimpleName());
                            }
                        });
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.redeem_discount));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
        handler.removeCallbacks(r);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_redeemVoucher_redeem:
                if (UIHelper.doubleClickCheck())
                    if (pinEntryEditText.getText().toString().length() > 3) {
                        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
                            if (isValidatePinCode(pinEntryEditText.getText().toString()))
                                sendRedeemVoucher(prefHelper.getUser().getId(), storeId, voucherId);
                            else
                                UIHelper.showShortToastInCenter(getDockActivity(), "Invalid pin code");
                    } else {
                        pinEntryEditText.setError("Please add PIN");
                        pinEntryEditText.requestFocus();
                    }
                break;
            case R.id.txt_redeemVoucher_forgot:
                if (UIHelper.doubleClickCheck())
                    if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
                        resendCode(prefHelper.getUser().getEmail());
                break;
            case R.id.txt_redeemVoucher_tnc:
                if (UIHelper.doubleClickCheck())
                    getDockActivity().replaceDockableFragment(TnCFragment.newInstance(), TnCFragment.class.getSimpleName());
                break;
            default:
                break;
        }
    }

    private boolean isValidatePinCode(String pinCode) {
        return (prefHelper.getUser() != null && prefHelper.getUser().getVerificationCode().equals(pinCode));
    }

    @RestAPI
    private void sendRedeemVoucher(int user_id, String store_id, String store_voucher_id) {
        String type = "";
        if (freeVoucherId.isEmpty()) {
            type = "paid";
        } else type = "free";
        loadingStarted();
        btnRedeem.setEnabled(false);
        webService.sendRedeemVoucher(user_id, store_id, store_voucher_id, type, freeVoucherId)
                .enqueue(new Callback<ResponseWrapper<SubscribeResult>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<SubscribeResult>> call, Response<ResponseWrapper<SubscribeResult>> response) {
                        loadingFinished();
                        if (!RedeemVoucherFragment.this.isVisible()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            navigateToParticularFragment(response.body().getResult(), response.body().getMessage());
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                        btnRedeem.setEnabled(true);
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<SubscribeResult>> call, Throwable t) {
                        btnRedeem.setEnabled(true);
                        loadingFinished();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    private void navigateToParticularFragment(SubscribeResult result, String message) {
        if (result.getIsSubscribe() == AppConstants.YES) {

            UIHelper.showShortToast(getDockActivity(), getResources().getString(R.string.voucher_redeemed_success));
            LeaveReviewDialog(result.getUniqueCode());
        } else {
            UIHelper.showShortToastInCenter(getDockActivity(), message);
            getDockActivity().replaceDockableFragment(SubscriptionFragment.newInstance(false),
                    SubscriptionFragment.class.getSimpleName());
        }
    }

    @RestAPI
    private void resendCode(String email) {
        loadingStarted();
        webService.resendCode(email).enqueue(new Callback<ResponseWrapper>() {
            @Override
            public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                loadingFinished();
                if (RedeemVoucherFragment.this.isDetached() || RedeemVoucherFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    if (RedeemVoucherFragment.this.isVisible())
                        UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.resend_pin_success));
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    @RestAPI
    private void getBanner(String type) {
        loadingStarted();
        String cityId = prefHelper.getUser().getSelectedCity();
        webService.getBanner(cityId, type)
                .enqueue(new Callback<ResponseWrapper<ArrayList<BannerResult>>>() {

                             @Override
                             public void onResponse(Call<ResponseWrapper<ArrayList<BannerResult>>> call,
                                                    Response<ResponseWrapper<ArrayList<BannerResult>>> response) {
                                 loadingFinished();
                                 if (RedeemVoucherFragment.this.isDetached() || RedeemVoucherFragment.this.isRemoving()) {
                                     return;
                                 }
                                 if (response == null || response.body() == null || response.body().getResponse() == null) {
                                     UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                                 } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                                     try {
                                         bannerList = response.body().getResult();
                                         if (bannerList != null && bannerList.size() > 0) {
                                             //for exclusive
                                             if (bannerList.get(0).getIs_exclusive() == 1) {
                                                 ImageLoaderHelper.loadImageWithPicasso(getDockActivity(), bannerList.get(0).getBannerImage(), img_BANNER, true);

                                                 img_BANNER.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View view) {
                                                         if (UIHelper.doubleClickCheck())
                                                             Util.openCustomChromeTabs(getDockActivity(),
                                                                     bannerList.get(0).getLink());
                                                     }
                                                 });
                                             } else {
                                                 usingAlarmLogic(bannerList);
                                                 handler.post(r);
                                             }
                                         }
                                     } catch (Exception e) {
                                         e.printStackTrace();
                                     }
                                 } else {
                                     UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                                 }
                             }

                             @Override
                             public void onFailure(Call<ResponseWrapper<ArrayList<BannerResult>>> call, Throwable t) {
                                 loadingFinished();
                                 UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                             }
                         }
                );
    }

    private void usingAlarmLogic(final List<BannerResult> bannerList) {
        final int[] i = {0};
        if (bannerList != null && bannerList.size() > 0)
            r = new Runnable() {
                @Override
                public void run() {
                    if (bannerList.size() > i[0]) {
                        final int clickCount = i[0];
                        int setTime;
                        setTime = (TextUtils.isEmpty(bannerList.get(i[0]).getTime())) ? DEFAULT_BANNER_REFRESH_RATE :
                                (Util.getParsedInteger(bannerList.get(i[0]).getTime()) * 1000);

                        handler.postDelayed(this, setTime); //run the runnable in a minute again
                        i[0]++;

                        ImageLoaderHelper.loadImageWithPicasso(getDockActivity(), bannerList.get(clickCount).getBannerImage(), img_BANNER, false);

                        img_BANNER.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (UIHelper.doubleClickCheck())
                                    Util.openCustomChromeTabs(getDockActivity(), bannerList.get(clickCount).getLink());
                            }
                        });

                    } else {
                        i[0] = 0;

//                        UIHelper.showShortToastInCenter(getDockActivity(), "removeCallbacks " + i[0]);

                        handler.removeCallbacks(this);
                        handler.post(r);
                    }
                }
            };
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}