package com.ingic.diningnightlife.fragments;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.ingic.diningnightlife.BuildConfig;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.CameraHelper;
import com.ingic.diningnightlife.helpers.DateHelper;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.SnackbarUtils;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.ui.views.AnyEditTextView;
import com.ingic.diningnightlife.ui.views.CustomRatingBar;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.makeramen.roundedimageview.RoundedImageView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.ingic.diningnightlife.helpers.CameraHelper.mCurrentPhotoPath;

@RuntimePermissions
public class WriteReviewFragment extends BaseFragment implements View.OnClickListener {
    private static final int CAMERA_REQUEST = 1001;
    private final String TAG = WriteReviewFragment.class.getSimpleName();
    public View rootView;
    @BindView(R.id.et_writeReview_name)
    AnyEditTextView etName;
    @BindView(R.id.et_writeReview_description)
    AnyEditTextView etDesc;
    @BindView(R.id.img_writeReview_galleryPic1)
    RoundedImageView imgAddImage1;
    @BindView(R.id.img_writeReview_galleryPic2)
    RoundedImageView imgAddImage2;
    @BindView(R.id.img_writeReview_galleryPic3)
    RoundedImageView imgAddImage3;
    @BindView(R.id.img_writeReview_addRemove1)
    ImageView imgRemove1;
    @BindView(R.id.img_writeReview_addRemove2)
    ImageView imgRemove2;
    @BindView(R.id.img_writeReview_addRemove3)
    ImageView imgRemove3;
    @BindView(R.id.rb_writeReview_quality)
    CustomRatingBar rbQuality;
    @BindView(R.id.rb_writeReview_service)
    CustomRatingBar rbService;
    @BindView(R.id.rb_writeReview_ambiance)
    CustomRatingBar rbAmbiance;
    @BindView(R.id.rb_writeReview_price)
    CustomRatingBar rbPrice;
    @BindView(R.id.rb_writeReview_overAllExperience)
    CustomRatingBar rbOverAllExperience;
    @BindView(R.id.rl_writeReview_rate)
    RelativeLayout rlRate;
    File image1Url, image2Url, image3Url;
    boolean image1 = false, image2 = false, image3 = false;
    private Unbinder mBinder;
    private File fileUrl;
    private Snackbar snackbar;
    private Uri uriImage;
    private String storeId = "", storeName = "";
    private String filePathArray[] = new String[3];
    private String freeVoucherId = "";


    public WriteReviewFragment() {
        // Required empty public constructor
    }

    public static WriteReviewFragment newInstance() {
        return new WriteReviewFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_write_review, container, false);
        if (getArguments() != null) {
            storeId = getArguments().getString(AppConstants.BUNDLE_STORE_ID, "");
            storeName = getArguments().getString(AppConstants.BUNDLE_STORE_NAME, "");
            freeVoucherId = getArguments().getString(AppConstants.BUNDLE_FREE_VOUCHER_ID, "");
        }
        mBinder = ButterKnife.bind(this, rootView);
        setListener();
        etName.setText(storeName);
        return rootView;
    }

    private void setListener() {
        imgAddImage1.setOnClickListener(this);
        imgAddImage2.setOnClickListener(this);
        imgAddImage3.setOnClickListener(this);
        rlRate.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.write_review));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_writeReview_rate://submit btn
                if (UIHelper.doubleClickCheck())
                    if (etName.testValidity() && etDesc.testValidity()) {
                        if (checkRating()) {
                            if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
                                sendReviews(prefHelper.getUser().getId(),
                                        storeId,
                                        etDesc.getText().toString().trim(),
                                        (int) Math.ceil(rbQuality.getScore()),
                                        (int) Math.ceil(rbService.getScore()),
                                        (int) Math.ceil(rbAmbiance.getScore()),
                                        (int) Math.ceil(rbPrice.getScore()),
                                        (int) Math.ceil(rbOverAllExperience.getScore()),
                                        image1Url, image2Url, image3Url
                                        //getFile(filePathArray);
                                        //getByteArray(filePathArray);
                                );
                        } else {
                            UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.provide_rating));
                        }
                    }
                break;
            case R.id.img_writeReview_galleryPic1:
                if (image1Url == null || image1Url.equals("")) {
                    image1 = true;
                    WriteReviewFragmentPermissionsDispatcher.getStoragePermissionWithCheck(WriteReviewFragment.this);
                } else {
                    image1Url = null;
                    enableAddPhoto(imgAddImage1, imgRemove1);
//                    filePathArray[0] = null;
                }

                break;
            case R.id.img_writeReview_galleryPic2:
                if (image2Url == null || image2Url.equals("")) {
                    image2 = true;
                    WriteReviewFragmentPermissionsDispatcher.getStoragePermissionWithCheck(WriteReviewFragment.this);
                } else {
                    image2Url = null;
                    enableAddPhoto(imgAddImage2, imgRemove2);
//                    filePathArray[1] = null;
                }
                break;
            case R.id.img_writeReview_galleryPic3:
                if (image3Url == null || image3Url.equals("")) {
                    image3 = true;
                    WriteReviewFragmentPermissionsDispatcher.getStoragePermissionWithCheck(WriteReviewFragment.this);
                } else {
                    image3Url = null;
                    enableAddPhoto(imgAddImage3, imgRemove3);
//                    filePathArray[2]
// = null;
                }
                break;
            default:
                break;
        }
    }

    private boolean checkRating() {
        return rbAmbiance.getScore() > 0 && rbOverAllExperience.getScore() > 0 && rbQuality.getScore() > 0 &&
                rbPrice.getScore() > 0 && rbService.getScore() > 0;
    }

    @RestAPI
    private void sendReviews(int _id, String _storeId, String _desc, int _quality, int _service, int _ambiance, int _price,
                             int _overAllExperience, File image1Url, File image2Url, File image3Url) {
        rlRate.setEnabled(false);
        loadingStarted();
        Log.d(TAG + ", Data \n ", _id + " \n  " + _storeId + " \n " + _desc + " \n  " + _quality + " \n " + _service
                + " \n  " + _ambiance + " \n " + _price + " \n  " + _overAllExperience);

        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_id));
        RequestBody storeId = RequestBody.create(MediaType.parse("text/plain"), _storeId);
        RequestBody desc = RequestBody.create(MediaType.parse("text/plain"), _desc);
        RequestBody quality = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_quality));
        RequestBody service = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_service));
        RequestBody ambiance = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_ambiance));
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_price));
        RequestBody overAllExperience = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_overAllExperience));
        RequestBody writeReviewLocalTime = RequestBody.create(MediaType.parse("text/plain"), DateHelper.getCurrentTimeStamp());
        MultipartBody.Part image1 = null;
        MultipartBody.Part image2 = null;
        MultipartBody.Part image3 = null;

        if (image1Url != null) {
            RequestBody requestFile1 =
                    RequestBody.create(MediaType.parse("multipart/form-data"),
                            image1Url
                    );
            //to send also the actual file name
            image1 = MultipartBody.Part.createFormData("image1", image1Url.getName(), requestFile1);
        }
        if (image2Url != null) {
            RequestBody requestFile2 =
                    RequestBody.create(MediaType.parse("multipart/form-data"),
                            image2Url
                    );
            //to send also the actual file name
            image2 = MultipartBody.Part.createFormData("image2", image2Url.getName(), requestFile2);
        }
        if (image3Url != null) {
            RequestBody requestFile3 =
                    RequestBody.create(MediaType.parse("multipart/form-data"),
                            image3Url
                    );
            //to send also the actual file name
            image3 = MultipartBody.Part.createFormData("image3", image3Url.getName(), requestFile3);
        }

        webService.sendReviews(id, storeId, desc, quality, service,
                ambiance, price, overAllExperience, writeReviewLocalTime, image1, image2, image3)
                .enqueue(new Callback<ResponseWrapper>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                        loadingFinished();
                        if (!WriteReviewFragment.this.isVisible()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            if (WriteReviewFragment.this.isVisible()) {
                                UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.thankyou_for_your_review));
//                                if (freeVoucherId.isEmpty())
                                getDockActivity().popFragment();
//                                else {
//                                    getDockActivity().popFragment();
//                                    getDockActivity().replaceDockableFragment(MyVouchersFragment.newInstance(), MyVouchersFragment.class.getSimpleName());
//                                }
                            }
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                        rlRate.setEnabled(true);
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                        rlRate.setEnabled(true);
                        loadingFinished();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    public void uploadFromCamera() {
        Intent pictureActionIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        Uri photoURI;
        try {
            photoURI = FileProvider.getUriForFile(getDockActivity(),
                    BuildConfig.APPLICATION_ID + ".provider",
                    createImageFile());
            if (photoURI != null) {
                pictureActionIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                WriteReviewFragment.this.startActivityForResult(pictureActionIntent, CAMERA_REQUEST);
            }
        } catch (IOException e) {
            UIHelper.showShortToastInCenter(getDockActivity(), getString(R.string.failure_response));
            e.printStackTrace();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Dining&Nightlife");
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK)
            if (image1) {
                image1Url = CameraHelper.retrieveAndDisplayPicture(requestCode, resultCode, data, getDockActivity(),
                        imgAddImage1, mCurrentPhotoPath);
//                addOrSetFIleUrl(filePathArray, 0, image1Url);

                enableCancel(imgRemove1);
                image1 = false;

            } else if (image2) {
                image2Url = CameraHelper.retrieveAndDisplayPicture(requestCode, resultCode, data, getDockActivity(),
                        imgAddImage2, mCurrentPhotoPath);
//                addOrSetFIleUrl(filePathArray, 1, image2Url);
                enableCancel(imgRemove2);
                image2 = false;

            } else if (image3) {
                image3Url = CameraHelper.retrieveAndDisplayPicture(requestCode, resultCode, data, getDockActivity(),
                        imgAddImage3, mCurrentPhotoPath);
//                filePathArray.add(image3Url);

//                addOrSetFIleUrl(filePathArray, 2, image3Url);
                enableCancel(imgRemove3);
                image3 = false;
            }


//
//        fileUrl = CameraHelper.retrieveAndDisplayPicture(requestCode, resultCode, data, getDockActivity(), imgAddImage1,
//                mCurrentPhotoPath);
//        if (fileUrl == null) {
//            UIHelper.showLongToastInCenter(getDockActivity(), "null file");
//        }
    }

//    private void addOrSetFIleUrl(String[] filePathArray, int position, File imageUrl) {
//        try {
//            if (filePathArray[position] == null)
//                filePathArray[position] = imageUrl.getAbsolutePath();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }

    private void enableCancel(ImageView imgRemove) {
        imgRemove.setBackground(getResources().getDrawable(R.drawable.drawable_circle_solid_alphagray));
        imgRemove.setImageDrawable(ContextCompat.getDrawable(getDockActivity(), R.drawable.ic_remove_pic));
    }

    private void enableAddPhoto(RoundedImageView imgAddImage, ImageView imgRemoveChangeToAddPhoto) {
        imgAddImage.setImageResource(android.R.color.transparent);
        imgRemoveChangeToAddPhoto.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        imgRemoveChangeToAddPhoto.setImageDrawable(ContextCompat.getDrawable(getDockActivity(), R.drawable.ic_add_gallery));
    }

    /**
     * Permission Dispatcher work
     */
    @NeedsPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void getStoragePermission() {
        CameraHelper.uploadPhotoDialogForWriteReview(WriteReviewFragment.this, getDockActivity());
    }

    @OnShowRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showRationaleForStorage(final PermissionRequest request) {
        new AlertDialog.Builder(getDockActivity())
                .setMessage(R.string.permission_storage_rationale)
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showDeniedForStorage() {
        SnackbarUtils.showCameraSettingSnackBar(getDockActivity(), rootView,
                getResources().getString(R.string.permission_storage_denied));
    }

    @OnNeverAskAgain(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    void showNeverAskForStorage() {
        SnackbarUtils.showCameraSettingSnackBar(getDockActivity(), rootView,
                getResources().getString(R.string.permission_storage_neverask));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        WriteReviewFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

//    private Map<String, RequestBody> getByteArray(String[] _fileUrl) {
//        Map<String, RequestBody> map = new HashMap<>();
//        for (int i = 0; i < _fileUrl.length; i++) {
//            if (_fileUrl[i] != null) {
//                RequestBody requestFile =
//                        RequestBody.create(MediaType.parse("multipart/form-data"),
//                                _fileUrl[i]
//                        );
//                //to send also the actual file name
//                map.put(fileNames[i], requestFile);
//            }
//        }
//        return map;
//    }
//
//    private void getFile(String[] filePathArray) {
//        MultipartBody.Part image1 = null;
//        MultipartBody.Part image2 = null;
//        MultipartBody.Part image3 = null;
//        RequestBody requestFile1 = null;
//        RequestBody requestFile2 = null;
//        RequestBody requestFile3 = null;
//        if (filePathArray != null) {
//
//            //to send also the actual file name
//            if (filePathArray[0] !=null) {
//                requestFile1 =
//                        RequestBody.create(MediaType.parse("multipart/form-data"),
//                                filePathArray[0]);
//                image1 = MultipartBody.Part.createFormData("image1", filePathArray[0], requestFile1);
//            }
//            if (filePathArray[1] !=null) {
//                requestFile2 =
//                        RequestBody.create(MediaType.parse("multipart/form-data"),
//                                filePathArray[1]);
//                image2 = MultipartBody.Part.createFormData("image2", filePathArray[1], requestFile2);
//            }
//            if (filePathArray[2] !=null) {
//                requestFile3 =
//                        RequestBody.create(MediaType.parse("multipart/form-data"),
//                                filePathArray[2]);
//                image3 = MultipartBody.Part.createFormData("image3", filePathArray[2], requestFile3);
//            }
//        }
//    }

}