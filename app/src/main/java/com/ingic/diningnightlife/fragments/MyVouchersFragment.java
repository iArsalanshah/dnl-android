package com.ingic.diningnightlife.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.entities.Received;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.Sent;
import com.ingic.diningnightlife.entities.SharedVoucher;
import com.ingic.diningnightlife.entities.SharedVoucherList;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.ChildItemClickListener;
import com.ingic.diningnightlife.ui.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyVouchersFragment extends BaseFragment implements ChildItemClickListener {
    TabLayout tabMyVouchers;
    ViewPager vpMyVouchers;
    private ViewPagerAdapter adapter;

    public MyVouchersFragment() {
        // Required empty public constructor
    }

    public static MyVouchersFragment newInstance() {
        return new MyVouchersFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_myvouchers, container, false);
        init(view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getSharedVouchers(prefHelper.getUser().getId());
    }

    private void init(View view) {
        vpMyVouchers = view.findViewById(R.id.vp_myVouchers);
        tabMyVouchers = view.findViewById(R.id.tab_myVouchers);
    }

    @Override
    public void setTitleBar(TitleBar titleBar) {
        super.setTitleBar(titleBar);
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.setSubHeading(getResources().getString(R.string.my_vouchers));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    private void setupViewPager(Received receivedObj, Sent sentObj) {
        VouchersReceivedFragment receivedFragment = VouchersReceivedFragment.newInstance(this);
        VouchersSentFragment sentFragment = new VouchersSentFragment();
        Bundle receiveBundle = new Bundle();
        receiveBundle.putString(AppConstants.BUNDLE_RECEIVED_VOUCHERS, new Gson().toJson(receivedObj));
        Bundle sentBundle = new Bundle();
        sentBundle.putString(AppConstants.BUNDLE_SENT_VOUCHERS, new Gson().toJson(sentObj));
        receivedFragment.setArguments(receiveBundle);
        sentFragment.setArguments(sentBundle);

        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(receivedFragment, getResources().getString(R.string.vouchers_reveived));
        adapter.addFragment(sentFragment, getResources().getString(R.string.vouchers_sent));

        vpMyVouchers.setAdapter(adapter);
        tabMyVouchers.setupWithViewPager(vpMyVouchers);
    }

    private void getSharedVouchers(int userId) {
        loadingStarted();
        webService.getSharedVoucherList(userId).enqueue(new Callback<ResponseWrapper<SharedVoucherList>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<SharedVoucherList>> call, Response<ResponseWrapper<SharedVoucherList>> response) {
                loadingFinished();
                if (MyVouchersFragment.this.isResumed()) {
                    if (response.body() != null && response.body().getResponse() != null
                            && response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                        updateUi(response.body().getResult());
                    } else if (response.body() != null && response.body().getMessage() != null)
                        UIHelper.showShortToastInCenter(getDockActivity(), response.body().getMessage());
                    else {
                        UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<SharedVoucherList>> call, Throwable t) {
                loadingFinished();
                if (MyVouchersFragment.this.isResumed())
                    UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
            }
        });
    }

    private void updateUi(SharedVoucherList result) {
        Sent sentObj = result.getSent();
        Received receivedObj = result.getReceived();
        setupViewPager(receivedObj, sentObj);
    }

    @Override
    public void onClickListener(SharedVoucher entity) {
        RedeemVoucherFragment fragment = RedeemVoucherFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(entity.getStoreId()));
        bundle.putString(AppConstants.BUNDLE_STORE_VOUCHER_ID, String.valueOf(entity.getId()));
        bundle.putString(AppConstants.BUNDLE_VOUCHER_NAME, entity.getVoucher());
        bundle.putString(AppConstants.BUNDLE_VOUCHER_STORE_TYPE, entity.getType());
        bundle.putString(AppConstants.BUNDLE_STORE_NAME, entity.getFullName());
        bundle.putString(AppConstants.BUNDLE_FREE_VOUCHER_ID, String.valueOf(entity.getFree_voucher_id()));
        fragment.setArguments(bundle);
        getDockActivity().popFragment();
        getDockActivity().replaceDockableFragment(fragment,
                RedeemVoucherFragment.class.getSimpleName());
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}