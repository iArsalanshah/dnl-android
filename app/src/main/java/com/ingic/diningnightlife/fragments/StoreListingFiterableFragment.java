package com.ingic.diningnightlife.fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.base.Function;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.AllStore;
import com.ingic.diningnightlife.entities.LoginResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.DateHelper;
import com.ingic.diningnightlife.helpers.GPSHelper;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.SimpleDividerItemDecoration;
import com.ingic.diningnightlife.helpers.TextViewHelper;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.OnOffersClick;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.StoreListFilterableAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.MatchableRVArrayAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
@RuntimePermissions
public class StoreListingFiterableFragment extends BaseFragment implements OnViewHolderClick, OnOffersClick, SearchView.OnQueryTextListener, android.support.v7.widget.PopupMenu.OnMenuItemClickListener {
    private static final long DELAY_FOR_LATE_GPS_RESPONSE = 100;
    @BindView(R.id.rv_diningDeals)
    RecyclerView rvDiningDeals;
    @BindView(R.id.tv_diningDeals_Title)
    AnyTextView tvTitle;
    String storeType = "";
    private MatchableRVArrayAdapter<AllStore> adapter;
    private Unbinder mBinder;
    private PopupMenu filterMenu;
    private String cityVal;
    private View rootView;
    private FusedLocationProviderClient mFusedLocationClient;
    boolean isAdvanceSearch = false;
    private String isSearchResult;
    @BindView(R.id.alternateText)
    TextView alternateText;

    public StoreListingFiterableFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_store_listing_fiterable, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rootView = view;
        mBinder = ButterKnife.bind(this, view);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getDockActivity());
        initRecyclerView();
        if (getArguments() != null) {
            String type = getArguments().getString(AppConstants.BUNDLE_STORE_TYPE);
            isAdvanceSearch = getArguments().getBoolean(AppConstants.BUNDLE_IS_ADVANCE_SEARCH, false);
            isSearchResult = getArguments().getString(AppConstants.BUNDLE_SEARCH_RESULT, null);
            if (Util.isNotNullEmpty(type))
                storeType = type;
        }

        //title set
        String title = "";
        switch (storeType) {
            case "dinning":
                title = "Dining Deals";
                break;
            case "brunch":
                title = "Brunch Bargains";
                break;
            case "last_minute":
                title = "Last Minute Offers";
                break;
            case "exclusive":
                title = "Exclusive Experiences";
                break;
            case "nightlife":
                title = "Nightlife";
                break;
            default:
                title = "Search Result";
                break;
        }

        TextViewHelper.setText(tvTitle, title);

        if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity()))
            if (isAdvanceSearch) {
                String cityId = getArguments().getString(AppConstants.BUNDLE_CITY_ID);
                String cuisine = getArguments().getString(AppConstants.BUNDLE_CUISINE);
                String venue = getArguments().getString(AppConstants.BUNDLE_VENUE);
                String dietary = getArguments().getString(AppConstants.BUNDLE_DIETARY);
                String quickSearch = getArguments().getString(AppConstants.BUNDLE_QUICK_SEARCH);
                int isNowOpen = getArguments().getInt(AppConstants.BUNDLE_QUICK_SEARCH_NOW_OPEN, 0);

                getAdvanceSearchResult(cityId, storeType, cuisine, venue, dietary, quickSearch, isNowOpen, DateHelper.getCurrentTimeStamp(AppConstants.DATE_FORMAT_TIME));
            } else if (isSearchResult != null) {
                //get Search result
                String cityId = prefHelper.getUser().getSelectedCity();
                getSearchedData(cityId, isSearchResult);
            } else if (!storeType.isEmpty()) {
                String cityId = prefHelper.getUser().getSelectedCity();
                getStoresList(cityId, storeType);
            }
//        if (Util.isNotNullEmpty(storeType)) {
//            String cityId = prefHelper.getUser().getSelectedCity();
//            getStoresList(cityId, storeType);
//        }
    }

    private void initRecyclerView() {
        adapter = new StoreListFilterableAdapter(getDockActivity(), this, this, getFunction());
        rvDiningDeals.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.VERTICAL, false));
        rvDiningDeals.addItemDecoration(new SimpleDividerItemDecoration(getDockActivity()));
        rvDiningDeals.setAdapter(adapter);
    }

    private Function<AllStore, String> getFunction() {
        return new Function<AllStore, String>() {
            @Override
            public String apply(AllStore input) {
                if (input == null || input.getFullName() == null) return "";
                else
                    return input.getFullName();
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().lockDrawer();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
//        final String cityVal = prefHelper.getUser().getSelectedCity();
//        String city = (cityVal == null || cityVal.equals("1")) ? "Dubai" : "Abu Dhabi";
//        titleBar.hideButtons();
//        titleBar.showBackButton();
//        titleBar.showSearchBarForListing(city, new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                if (view.getId() == R.id.img_titleBar_search) {
////                    generalSearch(cityVal,titleBar.getSearchText());
////                }
//            }
//        }, false);
//
//        titleBar.setRightButton(R.drawable.ic_search_add, new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AdvanceSearchFragment fragment = AdvanceSearchFragment.newInstance();
//                Bundle bundle = new Bundle();
//                bundle.putString(AppConstants.BUNDLE_STORE_TYPE, storeType);
//                fragment.setArguments(bundle);
//                getDockActivity().replaceDockableFragment(fragment,
//                        AdvanceSearchFragment.class.getSimpleName());
//            }
//        });
//        titleBar.getSearchView().setOnQueryTextListener(this);
//        titleBar.setCityButton(null);
//        titleBar.setCityButton(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (StoreListingFiterableFragment.this.isVisible())
//                    dialogChangeCity(StoreListingFiterableFragment.this.getContext());
//            }
//        });

        cityVal = prefHelper.getUser().getSelectedCity();
        if (cityVal == null) {
            cityVal = "1";
        }
        String cityName = cityVal.equals("2") ? "Abu Dhabi" : "Dubai";
        titleBar.hideButtons();
        titleBar.showBackButton();
        titleBar.showFilter(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show dropDown
                if (filterMenu == null) {
                    filterMenu = new PopupMenu(getDockActivity(), v);
                    MenuInflater inflater = filterMenu.getMenuInflater();

                    inflater.inflate(R.menu.filter_option, filterMenu.getMenu());
                    filterMenu.setOnMenuItemClickListener(StoreListingFiterableFragment.this);
                }
                if (adapter == null || adapter.getList() == null || adapter.getList().size() == 0) {
                    UIHelper.showShortToastInCenter(getDockActivity(), "No records available to sort");
                } else
                    filterMenu.show();
            }
        });
        if (isAdvanceSearch || isSearchResult != null) { //only filter
            titleBar.setSearchBarForSearchResult(cityName);
            titleBar.getSearchView().setOnQueryTextListener(this);
        } else {
//            final AutocompleteSearchAdapter adapter = new AutocompleteSearchAdapter(getDockActivity(), android.R.layout.simple_dropdown_item_1line, cityVal);
//            titleBar.getAutoCompleteTextView().setAdapter(adapter);
//            titleBar.showAutoComplete(cityVal, city, getDockActivity(), new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                    AllStore item = adapter.getItem(i);
//                    if (item == null) {
//                        UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.error_null));
//                        return;
//                    }
//                    DetailPageFragment fragment = new DetailPageFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(item.getId()));
//                    bundle.putString(AppConstants.BUNDLE_STORE_TYPE, "");
//                    fragment.setArguments(bundle);
//                    getDockActivity().replaceDockableFragment(fragment, DetailPageFragment.class.getSimpleName());
//                }
//            }, new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    adapter.getCustomFilterResult(titleBar.getAutoCompleteTextView().getText().toString());
//                }
//            });
            titleBar.showSearchBarForListing(cityName, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //move to Search Result
                    getDockActivity().replaceDockableFragment(new AutoCompleteSearchFragment());
//                    if (titleBar.getSearchText().isEmpty()) {
//                        //ignore
//                    } else {
//                        StoreListingFiterableFragment fragment = new StoreListingFiterableFragment();
//                        Bundle bundle = new Bundle();
//                        bundle.putString(AppConstants.BUNDLE_SEARCH_RESULT, titleBar.getSearchText());
//                        fragment.setArguments(bundle);
//                        getDockActivity().replaceDockableFragment(fragment, StoreListingFiterableFragment.class.getSimpleName());
//                    }
                }
            });
            titleBar.setRightButton(R.drawable.ic_search_add, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AdvanceSearchFragment fragment = AdvanceSearchFragment.newInstance();
                    Bundle bundle = new Bundle();
                    bundle.putString(AppConstants.BUNDLE_STORE_TYPE, "");
                    fragment.setArguments(bundle);
                    getDockActivity().replaceDockableFragment(fragment,
                            AdvanceSearchFragment.class.getSimpleName());
                }
            });

            //TODO Abu Dhabi Only
//            titleBar.setCityButton(null);
//            titleBar.setCityButton(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if (StoreListingFiterableFragment.this.isVisible())
//                        ChangeCityActionSheetDialog();
//                }
//            });
        }
    }

    private void ChangeCityActionSheetDialog() {
        final String[] stringItems = {getResources().getString(R.string.city_abu_dhabi),
                getResources().getString(R.string.city_dubai)};
        final ActionSheetDialog dialog = new ActionSheetDialog(getDockActivity(), stringItems, null);
        dialog.title(getResources().getString(R.string.change_city))
                .titleTextSize_SP(14.5f)
                .cancelText(getResources().getString(android.R.string.cancel))
                .show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) updateSelectedCity("2");
                else updateSelectedCity("1");
                dialog.dismiss();
            }
        });
    }

//    private void dialogChangeCity(Context context) {
//        final Dialog dialog = new Dialog(context, R.style.DialogCustomTheme);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.dialog_city);
//        AnyTextView message = dialog.findViewById(R.id.tv_dialog_message);
//        AnyTextView btnAbuDhabi = dialog.findViewById(R.id.btn_left);
//        AnyTextView btnDubai = dialog.findViewById(R.id.btn_right);
//        message.setText(getResources().getString(R.string.select_city));
//        btnAbuDhabi.setText("Abu Dhabi");
//        btnDubai.setText("Dubai");
//        btnAbuDhabi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                updateSelectedCity("2");
//                dialog.dismiss();
//            }
//        });
//        btnDubai.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                updateSelectedCity("1");
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//    }

    private void updateSelectedCity(String val) {
        LoginResult userData = prefHelper.getUser();
        String cityVal = prefHelper.getUser().getSelectedCity();
        if (Util.isNotNullEmpty(cityVal) && !cityVal.equals(val)) {
            userData.setSelectedCity(val);
            prefHelper.putUser(userData);
            StoreListingFiterableFragment fragment = new StoreListingFiterableFragment();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.BUNDLE_STORE_TYPE, storeType);
            fragment.setArguments(bundle);
            getDockActivity().popFragment();
            getDockActivity().replaceDockableFragment(fragment, StoreListingFiterableFragment.class.getSimpleName());
        }
    }

    @Override
    public void onOfferItemClicked(int position, int storeId) {
        DetailPageFragment fragment = new DetailPageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(storeId));
        bundle.putString(AppConstants.BUNDLE_STORE_TYPE, storeType);
        fragment.setArguments(bundle);
        getDockActivity().replaceDockableFragment(fragment, DetailPageFragment.class.getSimpleName());
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @RestAPI
    private void getStoresList(String cityId, String storeType) {
        loadingStarted();
        webService.getStoresList(cityId, storeType).enqueue(new Callback<ResponseWrapper<ArrayList<AllStore>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<AllStore>>> call, Response<ResponseWrapper<ArrayList<AllStore>>> response) {
                loadingFinished();
                if (StoreListingFiterableFragment.this.isDetached() || StoreListingFiterableFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    //home data
                    updateUI(response.body().getResult());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<AllStore>>> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    @RestAPI
    private void getAdvanceSearchResult(String cityId, String storeType, String cuisine,
                                        String venue, String dietary,
                                        String quickSearch, int isNowOpen, String currentTimeStamp) {
        loadingStarted();
        webService.advancedSearchResult(cityId, storeType, cuisine, venue, dietary, quickSearch, isNowOpen, currentTimeStamp)
                .enqueue(new Callback<ResponseWrapper<ArrayList<AllStore>>>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseWrapper<ArrayList<AllStore>>> call, @NonNull Response<ResponseWrapper<ArrayList<AllStore>>> response) {
                        loadingFinished();
                        if (StoreListingFiterableFragment.this.isDetached() || StoreListingFiterableFragment.this.isRemoving()) {
                            //fragment is no longer available
                            return;
                        }
                        if (response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            updateUI(response.body().getResult());
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseWrapper<ArrayList<AllStore>>> call, @NonNull Throwable t) {
                        loadingFinished();
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    @RestAPI
    private void getSearchedData(String cityId, String key) {
        loadingStarted();
        webService.generalSearch(cityId, key).enqueue(new Callback<ResponseWrapper<ArrayList<AllStore>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<AllStore>>> call, Response<ResponseWrapper<ArrayList<AllStore>>> response) {
                if (!StoreListingFiterableFragment.this.isResumed()) return;
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    //home data
                    updateUI(response.body().getResult());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
                loadingFinished();
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<AllStore>>> call, Throwable t) {
                loadingFinished();
                if (StoreListingFiterableFragment.this.isResumed())
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
            }
        });
    }


    private void updateUI(final ArrayList<AllStore> result) {
        if (!StoreListingFiterableFragment.this.isResumed()) return;
        if (result == null || result.size() == 0) {
            alternateText.setVisibility(View.VISIBLE);
        } else {
            alternateText.setVisibility(View.GONE);
            adapter.addAll(result);
        }

    }

    private void filterAndUpdateUI(final ArrayList<AllStore> result) {
        if (!StoreListingFiterableFragment.this.isResumed()) return;
        if (result == null || result.size() == 0) {
            alternateText.setVisibility(View.VISIBLE);
        } else {
            alternateText.setVisibility(View.GONE);
            List<AllStore> newList = new ArrayList<>();
            List<AllStore> oldList = adapter.getList();
            for (AllStore item :
                    result) {
                for (AllStore oldItem :
                        oldList) {
                    if (item.getId() == oldItem.getId()) {
                        newList.add(item);
                    }
                }
            }
            adapter.addAll(newList);
        }

    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            adapter.getFilter().filter("");
        } else {
            adapter.getFilter().filter(newText);
        }
        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuOptionAtoZ:
                item.setChecked(!item.isChecked());
                //sort by A to Z
                filterListByName(adapter.getList());
                break;
            case R.id.menuOptionNearest:
                item.setChecked(!item.isChecked());
                //sort by nearest with api response
                StoreListingFiterableFragmentPermissionsDispatcher.showLocationWithCheck(StoreListingFiterableFragment.this);
                break;
            case R.id.menuOptionNew:
                item.setChecked(!item.isChecked());
                //sort by newest with date
                filterListByDate(adapter.getList());
                break;
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private void filterListByDate(List adapterList) {
        if (adapterList == null) return;
        try {
            ArrayList<AllStore> result = (ArrayList<AllStore>) adapterList;
            Collections.sort(result, new Comparator<AllStore>() {
                @Override
                public int compare(AllStore o1, AllStore o2) {
                    return DateHelper.parseDate(o1.getCreatedAt()).compareTo(DateHelper.parseDate(o2.getCreatedAt()));
                }
            });
            Collections.reverse(result);
            adapter.addAll(result);
        } catch (Exception ex) {
            UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
        }
    }

    @RestAPI
    private void filterListByDistance(String search, double lat, double _long) {
        loadingStarted();
        webService.getStoresFilter(cityVal, storeType, search, lat, _long).enqueue(new Callback<ResponseWrapper<ArrayList<AllStore>>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<ArrayList<AllStore>>> call, Response<ResponseWrapper<ArrayList<AllStore>>> response) {
                if (StoreListingFiterableFragment.this.isVisible()) {
                    if (response.body() == null || response.body().getResponse() == null ||
                            response.body().getResult() == null) {
                        UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.error_null));
                    } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                        filterAndUpdateUI(response.body().getResult());
                    } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                        UIHelper.showShortToastInCenter(getDockActivity(), response.body().getMessage());
                    } else {
                        UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                    }
                }
                loadingFinished();
            }

            @Override
            public void onFailure(Call<ResponseWrapper<ArrayList<AllStore>>> call, Throwable t) {
                loadingFinished();
                UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void filterListByName(List adapterList) {
        if (adapterList == null) return;
        try {
            ArrayList<AllStore> result = (ArrayList<AllStore>) adapterList;
            Collections.sort(result, new Comparator<AllStore>() {
                @Override
                public int compare(AllStore o1, AllStore o2) {
                    return o1.getFullName().compareToIgnoreCase(o2.getFullName());
                }
            });
            adapter.addAll(result);
        } catch (Exception ex) {
            UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
        }
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    void showLocation() {
        if (GPSHelper.isGPSEnabled(getDockActivity())) {
            loadingStarted();
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    getLocation();
                }
            }, DELAY_FOR_LATE_GPS_RESPONSE);
        } else
            GPSHelper.showGPSDisabledSnackBarToUser(getDockActivity(), rootView, null);
    }

    @SuppressLint("MissingPermission")
    private void getLocation() {
        if (mFusedLocationClient != null) {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getDockActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                filterListByDistance(isSearchResult, location.getLatitude(), location.getLongitude());
                            } else {
                                UIHelper.showShortToastInCenter(getDockActivity(), "Location not found");
                            }
                        }
                    });
        }
        loadingFinished();
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForLocation(final PermissionRequest request) {
        new AlertDialog.Builder(getDockActivity())
                .setMessage(R.string.permission_location_rationale)
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForLocation() {
        showSettingsSnackBar(getResources().getString(R.string.permission_location_denied));
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    void showNeverAskForLocation() {
        showSettingsSnackBar(getResources().getString(R.string.permission_location_neverask));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        StoreListingFiterableFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void showSettingsSnackBar(String msg) {
        Snackbar snackbar = Snackbar.make(rootView, msg, Snackbar.LENGTH_LONG);
        snackbar.setAction("Settings", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startInstalledAppDetailsActivity(getDockActivity());
            }
        });
        snackbar.show();
    }

    private void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }
}
