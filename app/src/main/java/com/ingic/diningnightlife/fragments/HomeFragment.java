package com.ingic.diningnightlife.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.flyco.animation.BounceEnter.BounceEnter;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.listener.OnOperItemClickL;
import com.flyco.dialog.widget.ActionSheetDialog;
import com.flyco.dialog.widget.NormalDialog;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.ingic.diningnightlife.R;
import com.ingic.diningnightlife.annotations.RestAPI;
import com.ingic.diningnightlife.entities.BannerResult;
import com.ingic.diningnightlife.entities.FeaturedStore;
import com.ingic.diningnightlife.entities.HomeGridEntity;
import com.ingic.diningnightlife.entities.HomeResult;
import com.ingic.diningnightlife.entities.LoginResult;
import com.ingic.diningnightlife.entities.MagazineResult;
import com.ingic.diningnightlife.entities.ResponseWrapper;
import com.ingic.diningnightlife.entities.SavingResult;
import com.ingic.diningnightlife.fragments.abstracts.BaseFragment;
import com.ingic.diningnightlife.global.AppConstants;
import com.ingic.diningnightlife.global.WebServiceConstants;
import com.ingic.diningnightlife.helpers.GPSHelper;
import com.ingic.diningnightlife.helpers.GridSpacingItemDecoration;
import com.ingic.diningnightlife.helpers.ImageLoaderHelper;
import com.ingic.diningnightlife.helpers.InternetHelper;
import com.ingic.diningnightlife.helpers.SimpleDividerItemDecoration;
import com.ingic.diningnightlife.helpers.UIHelper;
import com.ingic.diningnightlife.interfaces.OnHotDealsClicked;
import com.ingic.diningnightlife.interfaces.OnOffersClick;
import com.ingic.diningnightlife.interfaces.OnViewHolderClick;
import com.ingic.diningnightlife.ui.adapters.HomeGridAdapter;
import com.ingic.diningnightlife.ui.adapters.HomeHotDealsAdapter;
import com.ingic.diningnightlife.ui.adapters.HomeOffersAdapter;
import com.ingic.diningnightlife.ui.adapters.abstracts.RecyclerViewListAdapter;
import com.ingic.diningnightlife.ui.views.AnyTextView;
import com.ingic.diningnightlife.ui.views.TitleBar;
import com.ingic.diningnightlife.ui.views.Util;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ingic.diningnightlife.global.AppConstants.DEFAULT_BANNER_REFRESH_RATE;

@RuntimePermissions
public class HomeFragment extends BaseFragment implements OnViewHolderClick, OnHotDealsClicked, OnOffersClick {
    private static final String TAG = HomeFragment.class.getSimpleName();
    private static boolean fetchLocation;
    @BindView(R.id.img_home_BANNER_TOP)
    ImageView img_BANNER;
    @BindView(R.id.rv_home_gridOptions)
    RecyclerView rvGridOptions;
    @BindView(R.id.rv_home_hotDeals)
    RecyclerView rvHotDeals;
    @BindView(R.id.rv_home_offers)
    RecyclerView rvOffers;
    @BindView(R.id.img_home_swipeHotDeals)
    ImageView imageSwipeHotDeals;
    @BindView(R.id.tv_home_savedAmountText)
    AnyTextView tvSavedAmountText;
    @BindView(R.id.view_rightcon)
    View viewRightIcon;
    @BindView(R.id.line_home)
    View lineBlueColor;
    RecyclerViewListAdapter adapterGridOptions, adapterHotDeals, adapterOffers;
    @BindView(R.id.dummySize)
    View dummySize;
    private Unbinder mBinder;
    private List<HomeGridEntity> gridEntityList = new ArrayList<>();
    private int apiCount;
    private static final long DELAY_FOR_LATE_GPS_RESPONSE = 100;
    private FusedLocationProviderClient mFusedLocationClient;
    private View rootView;

    //For banner list:
    List<BannerResult> bannerList;
    Handler handler = new Handler();
    Runnable r;
    private final String userAvailedAnyVoucher = "Congrats, so far you\'ve saved AED %.2f! ";
    private final String userNotAvailedAnyVoucher = "So far you\'ve saved AED %.2f! ";

    public static HomeFragment newInstance(boolean fetchCurrentLocation) {
        fetchLocation = fetchCurrentLocation;
        if (AppConstants.FETCH_LOCATION == fetchCurrentLocation) {
            AppConstants.FETCH_LOCATION = false;
        }
        return new HomeFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: Hash key : " + printKeyHash(getDockActivity()));
        adapterGridOptions = new HomeGridAdapter(getDockActivity(), this);
        adapterHotDeals = new HomeHotDealsAdapter(getDockActivity(), this, this);
        adapterOffers = new HomeOffersAdapter(getDockActivity(), this, this);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getDockActivity());
        //schedule alarm here and post runnable as soon as scheduled
//        handler.post(r);
//        usingAlarmLogic(bannerList);
//        handler.post(r);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
        mBinder = ButterKnife.bind(this, rootView);
        if (prefHelper.getUser() == null) {
            prefHelper.setLoginStatus(false);
            getDockActivity().popBackStackTillEntry(0);
            getDockActivity().replaceDockableFragment(LoginFragment.newInstance(), LoginFragment.class.getSimpleName());
        } else {
            if (InternetHelper.CheckInternetConectivity(getDockActivity())) {
                apiCount = 0;
                getSavings(prefHelper.getUser().getId());
                getBanner(AppConstants.HOME);
            }
            initRecyclerViews();
        }
        return rootView;
    }

    private void initRecyclerViews() {
        //Grid Options
        rvGridOptions.setLayoutManager(new GridLayoutManager(getDockActivity(), 3));
        int spanCount = 3; // default 3 columns
        int spacing = 0; // default 50px
        rvGridOptions.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, true));
        rvGridOptions.setAdapter(adapterGridOptions);

        //Hot Deals
        rvHotDeals.setLayoutManager(new LinearLayoutManager(getDockActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvHotDeals.setAdapter(adapterHotDeals);

        //Offers
        LinearLayoutManager llm = new LinearLayoutManager(getDockActivity(),
                LinearLayoutManager.VERTICAL, false);
        llm.setAutoMeasureEnabled(true);
        rvOffers.setLayoutManager(llm);
        rvOffers.addItemDecoration(new SimpleDividerItemDecoration(getDockActivity()));
        rvOffers.setAdapter(adapterOffers);

        imageSwipeHotDeals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    rvHotDeals.smoothScrollBy((int) Util.convertDpToPixel(150, getDockActivity()), 0);
                } catch (Exception ex) {
                    Log.e(TAG, "onClick: " + ex.toString());
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
//        if (prefHelper.getUser() != null && fetchLocation) TODO Abu Dhabi Only
//            HomeFragmentPermissionsDispatcher.showLocationWithCheck(HomeFragment.this);
        handler.post(r);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getDockActivity().getDrawerLayout() != null)
            getDockActivity().releaseDrawer();

//        if (bannerList != null && bannerList.size() > 0) {
//            try {
//                TimerUtilHelper.setImagesWithTimer(getDockActivity(),
//                        bannerList,
//                        img_BANNER);
//            } catch (Exception ex) {
//            }
//        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (prefHelper.getUser() != null) {
            String cityId = prefHelper.getUser().getSelectedCity();
            if (InternetHelper.CheckInternetConectivityandShowToast(getDockActivity())) {
                getHomeData(cityId);
                bindDataGrid();
            } else {
                loadingFinish(++apiCount);
            }
        }
    }

    private void bindDataGrid() {
        List<Integer> imgResources = new ArrayList<>();
        imgResources.add(R.drawable.icon_dine);
        imgResources.add(R.drawable.icon_brunch);
        imgResources.add(R.drawable.icon_offer);
        imgResources.add(R.drawable.icon_latest);
        imgResources.add(R.drawable.icon_experience);
        imgResources.add(R.drawable.icon_nightlife);

        List<Integer> imgColors = new ArrayList<>();
        imgColors.add(ContextCompat.getColor(getDockActivity(), R.color.colorOrangeDarkGrid));
        imgColors.add(ContextCompat.getColor(getDockActivity(), R.color.colorBlueGrid));
        imgColors.add(ContextCompat.getColor(getDockActivity(), R.color.colorPurpleGrid));
        imgColors.add(ContextCompat.getColor(getDockActivity(), R.color.colorPinkGrid));
        imgColors.add(ContextCompat.getColor(getDockActivity(), R.color.colorGreenGrid));
        imgColors.add(ContextCompat.getColor(getDockActivity(), R.color.colorOrangeLightGrid));

        List<String> titleNames = new ArrayList<>();
        titleNames.add(getResources().getString(R.string.dining_deals));
        titleNames.add(getResources().getString(R.string.brunch_bargains));
        titleNames.add(getResources().getString(R.string.last_minute_offer));
        titleNames.add(getResources().getString(R.string.latest_edition));
        titleNames.add(getResources().getString(R.string.exclusive_experiences));
        titleNames.add(getResources().getString(R.string.night_life));

        gridEntityList.clear();
        for (int i = 0; i < 6; i++) {
            gridEntityList.add(new HomeGridEntity(imgColors.get(i), imgResources.get(i), titleNames.get(i)));
        }


        adapterGridOptions.addAll(gridEntityList);
    }

    @Override
    public void setTitleBar(final TitleBar titleBar) {
        super.setTitleBar(titleBar);
//        String cityVal = prefHelper.getUser().getSelectedCity();
//        String city = (cityVal == null || cityVal.equals("1")) ? "Dubai" : "Abu Dhabi";
//        titleBar.hideButtons();
//        titleBar.showMenuButton();
//        titleBar.showSearchBarForListing(city, new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getDockActivity().replaceDockableFragment(StoreListingFragment.newInstance(),
//                        StoreListingFragment.class.getSimpleName());
//            }
//        }, true);
//        titleBar.setRightButton(R.drawable.ic_search_add, new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getDockActivity().replaceDockableFragment(AdvanceSearchFragment.newInstance(),
//                        AdvanceSearchFragment.class.getSimpleName());
//            }
//        });
        String cityVal = prefHelper.getUser().getSelectedCity();

        //TODO for Abu Dhabi Only
        cityVal = "2";

//        if (cityVal == null) {
//            cityVal = "1";
//        }
        String cityName = cityVal.equals("2") ? "Abu Dhabi" : "Dubai";
        titleBar.hideButtons();
        titleBar.showMenuButton();
        titleBar.showSearchBarForListing(cityName, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //move to listing
                getDockActivity().replaceDockableFragment(new AutoCompleteSearchFragment());
//                if (titleBar.getSearchText().isEmpty()) {
//                    //ignore
//                } else {
//                    StoreListingFiterableFragment fragment = new StoreListingFiterableFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putString(AppConstants.BUNDLE_SEARCH_RESULT, titleBar.getSearchText());
//                    fragment.setArguments(bundle);
//                    getDockActivity().replaceDockableFragment(fragment, StoreListingFiterableFragment.class.getSimpleName());
//                }
            }
        });
//        final AutocompleteSearchAdapter adapter = new AutocompleteSearchAdapter(getDockActivity(), android.R.layout.simple_dropdown_item_1line, cityVal);
//        titleBar.getAutoCompleteTextView().setAdapter(adapter);
//        titleBar.showAutoComplete(cityVal, city, getDockActivity(), new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                AllStore item = adapter.getItem(i);
//                if (item == null) {
//                    UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.error_null));
//                    return;
//                }
//                DetailPageFragment fragment = new DetailPageFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(item.getId()));
//                bundle.putString(AppConstants.BUNDLE_STORE_TYPE, "");
//                fragment.setArguments(bundle);
//                getDockActivity().replaceDockableFragment(fragment, DetailPageFragment.class.getSimpleName());
//            }
//        }, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                adapter.getCustomFilterResult(titleBar.getAutoCompleteTextView().getText().toString());
//            }
//        });
        //Advance Search
        titleBar.setRightButton(R.drawable.ic_search_add, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdvanceSearchFragment fragment = AdvanceSearchFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString(AppConstants.BUNDLE_STORE_TYPE, "");
                fragment.setArguments(bundle);
                getDockActivity().replaceDockableFragment(fragment,
                        AdvanceSearchFragment.class.getSimpleName());
            }
        });

        //TODO Abu Dhabi Only
//        titleBar.setCityButton(null);
//        titleBar.setCityButton(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (HomeFragment.this.isResumed())
//                    ChangeCityActionSheetDialog();
//            }
//        });
    }

//    public void showDialog(Activity activity, String title, CharSequence message) {
//        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//
//        if (title != null) builder.setTitle(title);
//
//        builder.setMessage(message);
//        builder.setPositiveButton("Dubai", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                updateSelectedCity("1");
//                dialogInterface.dismiss();
//            }
//        });
//        builder.setNegativeButton("Abu Dhabi", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                updateSelectedCity("2");
//                dialogInterface.dismiss();
//
//
//            }
//        });
//        builder.show();
//    }

    private void updateSelectedCity(String val) {
        LoginResult userData = prefHelper.getUser();
        String cityVal = prefHelper.getUser().getSelectedCity();
        if (Util.isNotNullEmpty(cityVal) && !cityVal.equals(val)) {
            userData.setSelectedCity(val);
            prefHelper.putUser(userData);
            getDockActivity().replaceDockableFragment(HomeFragment.newInstance(false), HomeFragment.class.getSimpleName());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mBinder.unbind();
        handler.removeCallbacks(r);

    }

    @Override
    public void onItemClick(View view, int position) {
        if (!InternetHelper.CheckInternetConectivityandShowToast(getDockActivity())) {
            return;
        }

        if (view.getTag() != null && view.getTag().equals(AppConstants.GRID_TAG_CLICKED)) {
            //Store Listing
            String storeType = "";
            switch (position) {
                case 0://dining
                    storeType = "dinning";
                    gridFragments(storeType);

                    break;
                case 1://brunch
                    storeType = "brunch";
                    gridFragments(storeType);
                    break;
                case 2://last minute offer
                    storeType = "last_minute";
                    gridFragments(storeType);
                    break;
                case 4://exclusive
                    storeType = "exclusive";
                    gridFragments(storeType);
                    break;
                case 5://nightlife
                    storeType = "nightlife";
                    if (prefHelper.getUser().getOver_21() == 0) {
                        Confirmation21Years(storeType);
                    } else
                        gridFragments(storeType);
                    break;
                default:
                    storeType = "dinning";
                    gridFragments(storeType);
                    break;
            }
//            gridFragments(storeType);
        } else if (view.getTag() != null && view.getTag().equals(AppConstants.GRID_TAG_LATEST_EDITION)) {
            //@latest_edition
            getMagazine();
//            String cityId = prefHelper.getUser().getSelectedCity();
//            getDockActivity().startActivity(new Intent(getDockActivity(), PdfActivity.class)
//                    .putExtra(AppConstants.INTENT_CITY_ID, cityId));
        } else {
            //ignore
        }
    }

//    private void dialogConfirmAge(Context context, final String storeType) {
//        final Dialog dialog = new Dialog(context, R.style.DialogCustomTheme);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.dialog_mark_tick);
//        AnyTextView confirm = (AnyTextView) dialog.findViewById(R.id.tv_dialog_message);
//        confirm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                prefHelper.setConfirmAge(true);
//                gridFragments(storeType);
//            }
//        });
//        dialog.show();
//    }

//    private void dialogChangeCity(Context context) {
//        final Dialog dialog = new Dialog(context, R.style.DialogCustomTheme);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.dialog_city);
//        AnyTextView message = dialog.findViewById(R.id.tv_dialog_message);
//        AnyTextView btnAbuDhabi = dialog.findViewById(R.id.btn_left);
//        AnyTextView btnDubai = dialog.findViewById(R.id.btn_right);
//        message.setText(getResources().getString(R.string.select_city));
//        btnAbuDhabi.setText("Abu Dhabi");
//        btnDubai.setText("Dubai");
//        btnAbuDhabi.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                updateSelectedCity("2");
//                dialog.dismiss();
//            }
//        });
//        btnDubai.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                updateSelectedCity("1");
//                dialog.dismiss();
//            }
//        });
//        dialog.show();
//    }

    private void ChangeCityActionSheetDialog() {
        final String[] stringItems = {getResources().getString(R.string.city_abu_dhabi),
                getResources().getString(R.string.city_dubai)};
        final ActionSheetDialog dialog = new ActionSheetDialog(getDockActivity(), stringItems, null);
        dialog.title(getResources().getString(R.string.change_city))
                .titleTextSize_SP(14.5f)
                .cancelText(getResources().getString(android.R.string.cancel))
                .show();

        dialog.setOnOperItemClickL(new OnOperItemClickL() {
            @Override
            public void onOperItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) updateSelectedCity("2");
                else updateSelectedCity("1");
                dialog.dismiss();
            }
        });
    }

    private void Confirmation21Years(final String storeType) {
        final NormalDialog dialog = new NormalDialog(getDockActivity());
        dialog.content(getResources().getString(R.string.confirm_over_21_years_old))
                .style(NormalDialog.STYLE_TWO)
                .title(getResources().getString(android.R.string.dialog_alert_title))
                .titleTextSize(23)
                .contentGravity(Gravity.CENTER)
                .showAnim(new BounceEnter())
                .btnText(getResources().getString(R.string.yes), getResources().getString(R.string.no))
                .show();

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        setOver21(prefHelper.getUser().getId(), storeType);
                        dialog.dismiss();
                    }
                },
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                    }
                });
    }

    private void setOver21(final int userId, final String storeType) {
        loadingStarted();
        webService.setOver21Year(userId, 1).enqueue(new Callback<ResponseWrapper>() {
            @Override
            public void onResponse(Call<ResponseWrapper> call, Response<ResponseWrapper> response) {
                loadingFinished();
                if (HomeFragment.this.isResumed() && response.body() != null && response.body().getResponse() != null &&
                        response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    //i confirm 21 years old
                    LoginResult userObj = prefHelper.getUser();
                    userObj.setOver_21(1);
                    prefHelper.putUser(userObj);
                    gridFragments(storeType);
                } else if (HomeFragment.this.isResumed())
                    UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
            }

            @Override
            public void onFailure(Call<ResponseWrapper> call, Throwable t) {
                if (HomeFragment.this.isResumed())
                    UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                loadingFinished();
            }
        });
    }

//    private void dialogYesNo(final String storeType) {
//        DialogFactory.createMessageDialog2(getDockActivity(),
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
////                        getDockActivity().finish();
//                        prefHelper.setConfirmAge(true);
//                        gridFragments(storeType);
//                    }
//                }, getResources().getString(R.string.confirm_over_21_years_old), "").show();
//    }

    private void gridFragments(String storeType) {
        StoreListingFiterableFragment fragment = new StoreListingFiterableFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_STORE_TYPE, storeType);
        fragment.setArguments(bundle);
        getDockActivity().replaceDockableFragment(fragment, StoreListingFiterableFragment.class.getSimpleName());
    }

    @RestAPI
    private void getHomeData(String cityId) {
        webService.getHomeData(cityId).enqueue(new Callback<ResponseWrapper<HomeResult>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<HomeResult>> call, Response<ResponseWrapper<HomeResult>> response) {
                if (HomeFragment.this.isDetached() || HomeFragment.this.isRemoving()) {
                    //fragment is no longer available
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                    //home data
                    UpdateUI(response.body().getResult());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                } else {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                }
                loadingFinish(++apiCount);
            }

            @Override
            public void onFailure(Call<ResponseWrapper<HomeResult>> call, Throwable t) {
                loadingFinish(++apiCount);
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    private void UpdateUI(HomeResult result) {
        if (!HomeFragment.this.isVisible()) return;

        List<FeaturedStore> featuredList = result.getFeaturedStores();
        List<FeaturedStore> lastTwoStores = new ArrayList<>();

        //hide or show feature list scroll arrow
        if (featuredList.size() > 2) {
            showFeatureArrow();
        } else hideFeatureArrow();

        if (featuredList.size() > 5) {
            lineBlueColor.setVisibility(View.VISIBLE);
            lastTwoStores.add(featuredList.get(5));
            featuredList.remove(5);
            if (featuredList.size() > 5) {
                lastTwoStores.add(featuredList.get(5));
                featuredList.remove(5);
            }
        } else {
            lineBlueColor.setVisibility(View.GONE);
        }
        adapterHotDeals.addAll(featuredList);
        adapterOffers.addAll(lastTwoStores);
    }

    @RestAPI
    private void getBanner(String type) {
        String cityId = prefHelper.getUser().getSelectedCity();
        webService.getBanner(cityId, type)
                .enqueue(new Callback<ResponseWrapper<ArrayList<BannerResult>>>() {

                             @Override
                             public void onResponse(Call<ResponseWrapper<ArrayList<BannerResult>>> call,
                                                    Response<ResponseWrapper<ArrayList<BannerResult>>> response) {
                                 if (HomeFragment.this.isDetached() || HomeFragment.this.isRemoving()) {
                                     return;
                                 }
                                 if (response.body() == null || response.body().getResponse() == null) {
                                     UIHelper.showShortToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                                 } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                                     try {
                                         bannerList = response.body().getResult();
                                         if (bannerList != null && bannerList.size() > 0) {
                                             //for exclusive
                                             if (bannerList.get(0).getIs_exclusive() == 1) {
                                                 ImageLoaderHelper.loadImageWithPicasso(getDockActivity(), bannerList.get(0).getBannerImage(), img_BANNER, true);

                                                 img_BANNER.setOnClickListener(new View.OnClickListener() {
                                                     @Override
                                                     public void onClick(View view) {
                                                         if (UIHelper.doubleClickCheck())
                                                             Util.openCustomChromeTabs(getDockActivity(),
                                                                     bannerList.get(0).getLink());
                                                     }
                                                 });
                                             } else {
                                                 usingAlarmLogic(bannerList);
                                                 handler.post(r);
                                             }
                                         }
                                     } catch (Exception e) {
                                         e.printStackTrace();
                                     }
                                 } else {
                                     UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                                 }
                                 loadingFinish(++apiCount);
                             }

                             @Override
                             public void onFailure(Call<ResponseWrapper<ArrayList<BannerResult>>> call, Throwable t) {
                                 loadingFinish(++apiCount);
                                 UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                             }
                         }
                );
    }

    private void usingAlarmLogic(final List<BannerResult> bannerList) {
        final int[] i = {0};
        r = new Runnable() {
            @Override
            public void run() {
                if (bannerList.size() > i[0]) {
                    final int clickCount = i[0];
                    int setTime;
                    setTime = (TextUtils.isEmpty(bannerList.get(i[0]).getTime())) ? DEFAULT_BANNER_REFRESH_RATE :
                            (Util.getParsedInteger(bannerList.get(i[0]).getTime()) * 1000);

                    handler.postDelayed(this, setTime); //run the runnable in a minute again
                    i[0]++;

                    ImageLoaderHelper.loadImageWithPicasso(getDockActivity(),
                            bannerList.get(clickCount).getBannerImage(), img_BANNER, true);

                    if (img_BANNER != null)
                        img_BANNER.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (UIHelper.doubleClickCheck())
                                    Util.openCustomChromeTabs(getDockActivity(), bannerList.get(clickCount).getLink());
                            }
                        });

                } else {
                    i[0] = 0;

//                        UIHelper.showShortToastInCenter(getDockActivity(), "removeCallbacks " + i[0]);

                    handler.removeCallbacks(this);
                    handler.post(r);
                }
            }
        };
    }

//    private void usingCountDown(final List<BannerResult> bannerList) {
//        final int[] i = {0};
//        int myTime = Util.getParsedInteger(bannerList.get(i[0]).getTime()) * 1000;
//        new CountDownTimer(myTime, 1) {
//
//            @Override
//            public void onTick(long millisUntilFinished) {
//                UIHelper.showShortToastInCenter(getDockActivity(), millisUntilFinished + "");
//            }
//
//            @Override
//            public void onFinish() {
////                imgView.setImageDrawable(sdk.getContext().getResources().getDrawable(array[i]));
//                if (i[0] == bannerList.size()) i[0] = 0;
//                ImageLoaderHelper.loadImage(bannerList.get(i[0]).getBannerImage(), img_BANNER);
////                img_BANNER.setOnClickListener(new View.OnClickListener() {
////                    @Override
////                    public void onClick(View view) {
////                        UIHelper.showShortToastInCenter(getDockActivity(), "i = " + i[0]);
////
////                        int myListIteration = i[0];
////                        if (myListIteration != 0)
////                            myListIteration = i[0] - 1;
////                        Util.openCustomChromeTabs(getDockActivity(), bannerList.get(myListIteration).getLink());
////
////                    }
////                });
//                i[0]++;
//                start();
//            }
//
//        }.start();
//    }

//    private void usingAnimation(List<BannerResult> bannerList) {
//        AnimationDrawable animation = new AnimationDrawable();
//        for (int i = 0; i < bannerList.size(); i++) {
//            animation.addFrame(Drawable.createFromPath(bannerList.get(i).getBannerImage()),
//                    Util.getParsedInteger(bannerList.get(i).getTime()) * 1000);
//        }
////        animation.addFrame(getResources().getDrawable(R.drawable.image2), 500);
////        animation.addFrame(getResources().getDrawable(R.drawable.image3), 300);
//        animation.setOneShot(false);
//
////        ImageView imageAnim =  (ImageView) findViewById(R.id.img);
//        img_BANNER.setBackgroundDrawable(animation);
//
//        // start the animation!
//        animation.start();
//    }

    @Override
    public void onStop() {
//        TimerUtilHelper.killTimerThread();
        super.onStop();
    }

    @Override
    public void onHotDealsItemClicked(int position, int storeId) {
        if (!InternetHelper.CheckInternetConectivityandShowToast(getDockActivity())) {
            return;
        }
        //Detail Page
        DetailPageFragment fragment = new DetailPageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(storeId));
        bundle.putString(AppConstants.BUNDLE_STORE_TYPE, "");
        fragment.setArguments(bundle);
        getDockActivity().replaceDockableFragment(fragment, DetailPageFragment.class.getSimpleName());
    }

    @Override
    public void onOfferItemClicked(int position, int storeId) {
        if (!InternetHelper.CheckInternetConectivityandShowToast(getDockActivity())) {
            return;
        }
        //Detail Page
        DetailPageFragment fragment = new DetailPageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_STORE_ID, String.valueOf(storeId));
        bundle.putString(AppConstants.BUNDLE_STORE_TYPE, "");
        fragment.setArguments(bundle);
        getDockActivity().replaceDockableFragment(fragment, DetailPageFragment.class.getSimpleName());
    }

    @RestAPI
    private void getSavings(int user_id) {
        loadingStarted();
        webService.getSavings(user_id)
                .enqueue(new Callback<ResponseWrapper<ArrayList<SavingResult>>>() {
                    @Override
                    public void onResponse(Call<ResponseWrapper<ArrayList<SavingResult>>> call,
                                           Response<ResponseWrapper<ArrayList<SavingResult>>> response) {
                        if (!HomeFragment.this.isVisible()) {
                            Log.e(TAG, "onResponse: *** SAVED FROM CRASH ***");
                            //fragment is no longer available
                            return;
                        }
                        if (response == null || response.body() == null || response.body().getResponse() == null) {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                        } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
                            try {
                                float amount = getTotalAmount(response.body().getResult());
                                if (amount > 0) {
                                    tvSavedAmountText.setText(String.format(Locale.getDefault(),
                                            userAvailedAnyVoucher, amount
                                    ));
                                } else {
                                    tvSavedAmountText.setText(String.format(Locale.getDefault(),
                                            userNotAvailedAnyVoucher, amount
                                    ));
                                }
                            } catch (Exception ex) {
                            }
                        } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                            UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                        } else {
                            UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.failure_response));
                        }
                        loadingFinish(++apiCount);
                    }

                    @Override
                    public void onFailure(Call<ResponseWrapper<ArrayList<SavingResult>>> call, Throwable t) {
                        loadingFinish(++apiCount);
                        UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
                    }
                });
    }

    private float getTotalAmount(ArrayList<SavingResult> result) {
        float totalSaving = 0;
        for (int i = 0; i < result.size(); i++) {
            totalSaving += result.get(i).getDiscountAmount() == null ? 0 : Util.getParsedFloat(result.get(i).getDiscountAmount());
        }
        return totalSaving;
    }

    private void loadingFinish(int count) {
        if (count > 2)
            loadingFinished();
    }

    private void hideFeatureArrow() {
        viewRightIcon.setVisibility(View.GONE);
        imageSwipeHotDeals.setVisibility(View.GONE);
    }

    private void showFeatureArrow() {
        viewRightIcon.setVisibility(View.VISIBLE);
        imageSwipeHotDeals.setVisibility(View.VISIBLE);
    }

    /**
     * Permission Dispatcher work
     */
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    void showLocation() {
        if (GPSHelper.isGPSEnabled(getDockActivity())) {
            loadingStarted();
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    getLocation();
                }
            }, DELAY_FOR_LATE_GPS_RESPONSE);
        } else
            GPSHelper.showGPSDisabledSnackBarToUser(getDockActivity(), rootView, null);
    }

    @SuppressLint("MissingPermission")
    private void getLocation() {
        if (mFusedLocationClient != null) {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getDockActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                Log.e(TAG, "onSuccess: lat->" + location.getLatitude());
                                int cityId = getCityId(getCityName(location.getLatitude(), location.getLongitude()));
                                updateSelectedCity(String.valueOf(cityId));
                            } else {
//                                UIHelper.showShortToastInCenter(getDockActivity(), "Location is Null");
                                Log.e(TAG, "location = NULL");
                            }
                        }
                    });
        }
        loadingFinished();
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForLocation(final PermissionRequest request) {
        new AlertDialog.Builder(getDockActivity())
                .setMessage(R.string.permission_location_rationale)
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForLocation() {
        showSettingsSnackBar(getResources().getString(R.string.permission_location_denied));
    }

    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    void showNeverAskForLocation() {
        showSettingsSnackBar(getResources().getString(R.string.permission_location_neverask));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        HomeFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    private void showSettingsSnackBar(String msg) {
        Snackbar snackbar = Snackbar.make(rootView, msg, Snackbar.LENGTH_LONG);
        snackbar.setAction("Settings", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startInstalledAppDetailsActivity(getDockActivity());
            }
        });
        snackbar.show();
    }

    private void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    private String getCityName(double lat, double lng) {
        String cityName = "";
        Geocoder geocoder = new Geocoder(getDockActivity(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            cityName = addresses.get(0).getLocality();
            Log.e(TAG, "getCityName: " + "city: " + cityName);
        }
        return cityName;
    }

    private int getCityId(String cityName) {
        if (Util.isNotNullEmpty(cityName) && (cityName.equalsIgnoreCase("Al Ain") ||
                cityName.equalsIgnoreCase("Abu Dhabi"))) {
            return 2;
        } else return 1;
    }

    @RestAPI
    private void getMagazine() {
        String cityVal = prefHelper.getUser().getSelectedCity();
        if (cityVal == null) {
            cityVal = "2";//TODO Abu Dhabi Only
        }
        loadingStarted();
        webService.getMagazine(cityVal).enqueue(new Callback<ResponseWrapper<MagazineResult>>() {
            @Override
            public void onResponse(Call<ResponseWrapper<MagazineResult>> call, Response<ResponseWrapper<MagazineResult>> response) {
                loadingFinished();
                if (!HomeFragment.this.isResumed()) {
                    //Activity is no longer available 
                    return;
                }
                if (response == null || response.body() == null || response.body().getResponse() == null) {
                    UIHelper.showLongToastInCenter(getDockActivity(), getResources().getString(R.string.null_response));
                } else if (response.body().getResponse().equals(WebServiceConstants.SUCCESS)) {
//                    openUrlInWebView(response.body().getResult().getMagazineUrl());
                    Util.openCustomChromeTabs(getDockActivity(), response.body().getResult().getMagazineUrl());
                } else if (response.body().getResponse().equals(WebServiceConstants.FAILURE)) {
                    UIHelper.showLongToastInCenter(getDockActivity(), response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ResponseWrapper<MagazineResult>> call, Throwable t) {
                loadingFinished();
                UIHelper.showLongToastInCenter(getDockActivity(), t.getMessage());
            }
        });
    }

    private void openUrlInWebView(String url) {
        String errorMessage = getResources().getString(R.string.error_url_not_valid);
        if (Util.isNotNullEmpty(url) && URLUtil.isValidUrl(url) && Patterns.WEB_URL.matcher(url).matches())
            getDockActivity().replaceDockableFragment(PurchaseSubscriptionFragment.newInstance(url, "Latest Edition"));
        else UIHelper.showLongToastInCenter(getDockActivity(), errorMessage);
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }
}